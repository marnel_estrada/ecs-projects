# Unity Pure ECS Projects #

I place my practice projects for Unity ECS (pure) here. It's just a bunch of stuff.

### How to set up? ###

I use the latest of Unity 2018.3. Open the folder EcsPractice as the project root. Open any scenes under Game/Scenes and play them. Some of them works, some might not anymore.

### Contribution guidelines ###

Create a pull request and I'll see if it's good.

### Acknowledgements ###

Thanks to [Kenney](https://kenney.nl/assets) for the assets.

(https://bitbucket.org/tutorials/markdowndemo) -> Just keeping it here as a link