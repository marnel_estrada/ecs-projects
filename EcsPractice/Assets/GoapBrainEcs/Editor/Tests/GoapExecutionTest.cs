using Common;
using Common.Utils;

using CommonEcs;

using NUnit.Framework;

using Unity.Entities;

using UnityEngine;

namespace GoapBrainEcs {
    [TestFixture]
    [Category("GoapBrainEcs")]
    public class GoapExecutionTest : CommonEcsTest {
        [Test]
        public void SingleAtomActionTest() {
            const int frames = 20;
            SingleAtomAction test = new SingleAtomAction(this.World, this.EntityManager);
            test.Execute(frames);
        }

        [Test]
        public void SingleMultipleFrameAtomActionTest() {
            const int frames = SingleMultipleFrameAtomAction.MAX_VALUE * 2;
            SingleMultipleFrameAtomAction test = new SingleMultipleFrameAtomAction(this.World, this.EntityManager);
            test.Execute(frames);
        }

        [Test]
        public void MultipleAtomActionsTest() {
            const int frames = MultipleAtomActions.MAX_VALUE * 3;
            new MultipleAtomActions(this.World, this.EntityManager).Execute(frames);
        }

        [Test]
        public void MoveIntTranslationTest() {
            const int frames = 40;
            new MoveIntTranslationTest(this.World, this.EntityManager).Execute(frames);
        }

        [Test]
        public void MultipleActionsWithMultipleAtomActionsTest() {
            const int frames = 50;
            new MultipleActionsWithMultipleAtomActions(this.World, this.EntityManager).Execute(frames);
        }

        [Test]
        public void FailedAtomActionTest() {
            const int frames = MultipleAtomActions.MAX_VALUE * 2;
            new FailedAtomAction(this.World, this.EntityManager).Execute(frames);
        }

        private void RunSystems(SimpleList<ComponentSystemBase> systems, int frames) {
            for (int i = 0; i < frames; ++i) {
                for (int systemIndex = 0; systemIndex < systems.Count; ++systemIndex) {
                    systems[systemIndex].Update();
                }
            }
        }
    }
}