using GoapBrainEcs;

using Unity.Entities;

namespace CommonEcs {
    [UpdateAfter(typeof(ExecuteNextAtomActionSystem))]
    [UpdateBefore(typeof(ReplanAfterFinishSystem))]
    [UpdateInGroup(typeof(PresentationSystemGroup))]
    public class SetGoapStatusSystem : AtomActionComponentSystem<SetGoapStatus> {
        protected override GoapStatus Start(ref AtomAction atomAction, ref SetGoapStatus actionComponent) {
            return actionComponent.status;
        }
    }
}