using CommonEcs;

using Unity.Entities;

namespace GoapBrainEcs {
    [UpdateAfter(typeof(ExecuteNextOnFailAtomActionSystem))]
    [UpdateInGroup(typeof(PresentationSystemGroup))]
    public class CheckAtomOnFailActionExecution : TemplateComponentSystem {
        protected override EntityQuery ComposeQuery() {
            return GetEntityQuery(typeof(AtomOnFailAction));
        }

        protected override void BeforeChunkTraversal() {
        }

        protected override void BeforeProcessChunk(ArchetypeChunk chunk) {
        }

        protected override void Process(int index) {
        }
    }
}