using Unity.Entities;

namespace GoapBrainEcs {
    public struct AtomOnFailAction : IComponentData {
        public readonly Entity agentEntity; // We added it here for convenience on writing action systems
        public readonly Entity parentOnFailAtomActionExecution;

        public AtomOnFailAction(Entity agentEntity, Entity parentOnFailAtomActionExecution) {
            this.agentEntity = agentEntity;
            this.parentOnFailAtomActionExecution = parentOnFailAtomActionExecution;
        }
    }
}