Shader "Common/SingleUvTexture" {
    Properties {
        _Texture("Main Texture", 2D) = "white" {}
    }
    SubShader {
        Tags{ "Queue" = "Transparent" "IgnoreProjector" = "True" "RenderType" = "Transparent" }
		ZWrite Off Lighting Off Cull Off Fog{ Mode Off } Blend SrcAlpha OneMinusSrcAlpha
		LOD 110
        
        Pass
		{
			CGPROGRAM
			#pragma vertex vert_vct
			#pragma fragment frag_mult 
			#pragma fragmentoption ARB_precision_hint_fastest
			#pragma multi_compile_instancing
			#include "UnityCG.cginc"

			sampler2D _Texture;

			struct vin_vct {
				float4 vertex : POSITION;
				float4 color : COLOR;
				float2 texcoord0 : TEXCOORD0;
				UNITY_VERTEX_INPUT_INSTANCE_ID
			};

			struct v2f_vct {
				float4 vertex : SV_POSITION;
				fixed4 color : COLOR;
				float2 texcoord0 : TEXCOORD0;
				UNITY_VERTEX_INPUT_INSTANCE_ID
			};

			v2f_vct vert_vct(vin_vct v) {
				v2f_vct o;

				UNITY_SETUP_INSTANCE_ID(v);
                UNITY_TRANSFER_INSTANCE_ID(v, o);

				o.vertex = UnityObjectToClipPos(v.vertex);
				o.color = v.color;
				o.texcoord0 = v.texcoord0;
				return o;
			}

			fixed4 frag_mult(v2f_vct i) : SV_Target {
				UNITY_SETUP_INSTANCE_ID(i);

				return tex2D(_Texture, i.texcoord0) * i.color;
			}

			ENDCG
		}
    }
}