﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using UnityEngine;
using UnityEditor;

using Common;
using Common.Utils;

namespace Common {
    /// <summary>
    /// Editor class for DataClassPool objects. Must be derived before it can be used.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public abstract class DataClassPoolEditor<T> : UnityEditor.Editor where T : Identifiable, new() {

        private DataClassPool<T> dataPool;

        void OnEnable() {
            this.dataPool = (DataClassPool<T>)this.target;
            Assertion.AssertNotNull(this.dataPool);
        }
        
        protected DataClassPool<T> DataPool {
            get {
                return (DataClassPool<T>)this.target;
            }
        }

    }
}
