﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using UnityEngine;
using UnityEditor;

using Common;
using Common.Utils;

namespace Common {
    class DataClassSidebarView<T> where T : Identifiable, new() {

        private Dictionary<string, T> filteredMap = new Dictionary<string, T>();
        private List<string> filteredIds = new List<string>(); // Used to render list of action buttons

        private Vector2 scrollPos = new Vector2();

        private int selection = 0;

        /// <summary>
        /// Constructor
        /// </summary>
        public DataClassSidebarView() {
        }

        /// <summary>
        /// Renders the UI for the specified pool
        /// </summary>
        /// <param name="pool"></param>
        public void Render(DataClassPool<T> pool) {
            GUILayout.BeginVertical(GUILayout.Width(300));

            // Render existing
            GUILayout.Label("Items: ", EditorStyles.boldLabel);

            GUILayout.Space(5);

            // New item
            RenderNewItemUi(pool);

            GUILayout.Space(5);

            RenderItems(pool);

            GUILayout.EndVertical();
        }

        private string newItemId = "";

        private void RenderNewItemUi(DataClassPool<T> pool) {
            GUILayout.BeginHorizontal();
            GUILayout.Label("New: ", GUILayout.Width(40));
            this.newItemId = EditorGUILayout.TextField(this.newItemId);

            if(GUILayout.Button("Add", GUILayout.Width(40), GUILayout.Height(15))) {
                if(!string.IsNullOrEmpty(this.newItemId)) {
                    // See if it already exists
                    if(pool.Contains(newItemId.Trim())) {
                        // An item with the same ID exists
                        EditorUtility.DisplayDialog("Add Data Class", "Can't add. An item with the same ID already exists.", "OK");
                        return;
                    }

                    // Add the new item
                    T newItem = new T();
                    newItem.Id = this.newItemId.Trim();
                    pool.Add(newItem);

                    // Add to filtered and select it
                    AddToFiltered(newItem);
                    this.selection = this.filteredIds.Count - 1;
                    this.newItemId = "";

                    EditorUtility.SetDirty(pool);
                    DataClassPoolEditorWindow<T>.REPAINT.Dispatch();
                }
            }

            GUILayout.EndHorizontal();
        }

        private void AddToFiltered(T item) {
            this.filteredMap[item.Id] = item;
            this.filteredIds.Add(item.Id);
        }

        private string filterText = "";

        private void RenderItems(DataClassPool<T> pool) {
            // Filter text
            GUILayout.BeginHorizontal();
            GUILayout.Label("Filter:", GUILayout.Width(50));
            string newFilter = EditorGUILayout.TextField(this.filterText);
            if(!newFilter.Equals(this.filterText)) {
                // There's a new filter. Apply the filter
                this.filterText = newFilter;
                Filter(pool);
            }
            GUILayout.EndHorizontal();

            if (this.filteredIds.Count == 0) {
                // There are no filtered actions
                GUILayout.Label("(empty)");
                this.filterText = "";
                Filter(pool); // Try empty filter in the hopes of finding entries
                return;
            }

            this.scrollPos = GUILayout.BeginScrollView(this.scrollPos);
            this.selection = GUILayout.SelectionGrid(this.selection, this.filteredIds.ToArray(), 1);
            GUILayout.EndScrollView();
        }

        private void Filter(DataClassPool<T> pool) {
            this.filteredMap.Clear();
            this.filteredIds.Clear();

            for (int i = 0; i < pool.Count; ++i) {
                T item = pool.GetAt(i);

                if (string.IsNullOrEmpty(this.filterText)) {
                    // Filter text is empty
                    // Add every action data
                    AddToFiltered(item);
                } else if (item.Id.ToLower().Contains(this.filterText.ToLower())) {
                    AddToFiltered(item);
                }
            }

            // Sort by ID
            this.filteredIds.Sort();
        }

        public bool IsValidSelection {
            get {
                return 0 <= this.selection && this.selection < this.filteredMap.Count;
            }
        }

        public T SelectedItem {
            get {
                if (IsValidSelection) {
                    // Valid index
                    string selectedId = this.filteredIds[this.selection];
                    return this.filteredMap[selectedId];
                }

                return default(T);
            }
        }

        /// <summary>
        /// Routines on Repaint()
        /// </summary>
        /// <param name="pool"></param>
        public void OnRepaint(DataClassPool<T> pool) {
            // Apply filter again
            // Items might have been removed
            Filter(pool);
        }

    }
}
