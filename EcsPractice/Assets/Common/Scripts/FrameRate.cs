public class FrameRate {
	private float frameRate;
	private int numFrames;
	private float polledTime;
	
	/**
	 * Update routines.
	 */
	public void Update(float timeElapsed) {
		++this.numFrames;
		this.polledTime += timeElapsed;
		
		if(Comparison.TolerantGreaterThanOrEquals(this.polledTime, 1.0f)) {
			// update frame rate
			this.frameRate = (float) this.numFrames / this.polledTime;
			
			// reset states
			this.numFrames = 0;
			this.polledTime = 0;
		}
	}
	
	/**
	 * Returns the frame rate.
	 */
	public float GetFrameRate() {
		return this.frameRate;
	}
}
