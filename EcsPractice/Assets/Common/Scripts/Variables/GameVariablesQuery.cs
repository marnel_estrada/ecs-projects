﻿using UnityEngine;
using System.Collections;

using Common;
using Common.Query;

namespace Common {
    /**
	 * Wraps a GameVariable through queries
	 */
    public class GameVariablesQuery : MonoBehaviour {

        [SerializeField]
        private GameVariables gameVariables;

        // Query Ids
        public const string GET_GAME_VAR = "GetGameVar";
        public const string GET_INT_GAME_VAR = "GetIntGameVar";
        public const string GET_FLOAT_GAME_VAR = "GetFloatGameVar";
        public const string GET_BOOL_GAME_VAR = "GetBoolGameVar";

        // Parameters
        public const string KEY = "Key";

        void Awake() {
            Assertion.AssertNotNull(this.gameVariables);

            QuerySystem.RegisterResolver(GET_GAME_VAR, delegate (IQueryRequest request) {
                string key = (string)request.GetParameter(KEY);
                return this.gameVariables.Get(key);
            });

            QuerySystem.RegisterResolver(GET_INT_GAME_VAR, delegate (IQueryRequest request) {
                string key = (string)request.GetParameter(KEY);
                return this.gameVariables.GetInt(key);
            });

            QuerySystem.RegisterResolver(GET_FLOAT_GAME_VAR, delegate (IQueryRequest request) {
                string key = (string)request.GetParameter(KEY);
                return this.gameVariables.GetFloat(key);
            });

            QuerySystem.RegisterResolver(GET_BOOL_GAME_VAR, delegate (IQueryRequest request) {
                string key = (string)request.GetParameter(KEY);
                return this.gameVariables.GetBool(key);
            });
        }

        void OnDestroy() {
            QuerySystem.RemoveResolver(GET_GAME_VAR);
            QuerySystem.RemoveResolver(GET_INT_GAME_VAR);
            QuerySystem.RemoveResolver(GET_FLOAT_GAME_VAR);
            QuerySystem.RemoveResolver(GET_BOOL_GAME_VAR);
        }

        /// <summary>
        /// Queries a string game variable
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public static string Get(string key) {
            IQueryRequest request = QuerySystem.Start(GET_GAME_VAR);
            request.AddParameter(KEY, key);
            return QuerySystem.Complete<string>(request);
        }

        /**
		 * Queries a float game variable
		 */
        public static float GetFloat(string key) {
            IQueryRequest request = QuerySystem.Start(GET_FLOAT_GAME_VAR);
            request.AddParameter(KEY, key);
            return QuerySystem.Complete<float>(request);
        }

        /**
		 * Queries an int game variable
		 */
        public static int GetInt(string key) {
            IQueryRequest request = QuerySystem.Start(GET_INT_GAME_VAR);
            request.AddParameter(KEY, key);
            return QuerySystem.Complete<int>(request);
        }

        /// <summary>
        /// Queries a bool game variable
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public static bool GetBool(string key) {
            IQueryRequest request = QuerySystem.Start(GET_BOOL_GAME_VAR);
            request.AddParameter(KEY, key);
            return QuerySystem.Complete<bool>(request);
        }

    }
}
