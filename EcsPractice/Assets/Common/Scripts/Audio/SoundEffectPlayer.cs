﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using UnityEngine;

using Common.Time;
using Common.Fsm;
using Common.Fsm.Action;

namespace Common {
    [RequireComponent(typeof(AudioSource))]
    [RequireComponent(typeof(SwarmItem))]
    public class SoundEffectPlayer : MonoBehaviour {

        public enum PlayerType { DEFAULT, AMBIENT }

        [SerializeField]
        private string timeReferenceName;

        [SerializeField]
        private PlayerType playerType;

        private AudioSource source;
        private SwarmItem swarmItem;

        private Common.Fsm.Fsm fsm;

        private Transform selfTransform;

        void Awake() {
            this.source = GetComponent<AudioSource>();
            Assertion.AssertNotNull(this.source);
            
            this.swarmItem = GetComponent<SwarmItem>();
            Assertion.AssertNotNull(this.swarmItem);

            this.selfTransform = this.transform;

            PrepareFsm();
        }

        // initial state
        private const string PLAYING = "Playing";

        // events
        private const string FINISHED = "Finished";

        private void PrepareFsm() {
            this.fsm = new Common.Fsm.Fsm("SfxPlayer." + this.gameObject.name);

            // states
            FsmState playing = fsm.AddState(PLAYING);
            FsmState killed = fsm.AddState("Killed");

            // actions
            TimedWaitAction wait = new TimedWaitAction(this.timeReferenceName, FINISHED);

            playing.AddAction(new FsmDelegateAction(delegate (FsmState owner) {
                this.source.Play();

                    // initialize wait timer before it is started
                    wait.Init(this.source.clip.length + 0.1f); // a little offset to make sure that sound finishes playing
            }));

            playing.AddAction(wait);

            if (this.playerType != PlayerType.AMBIENT) {
                killed.AddAction(new FsmDelegateAction(delegate (FsmState owner) {
                    KillPlayer();
                }));
            }

            // transitions
            playing.AddTransition(FINISHED, killed);

            // fsm would be started at Play()
        }

        public void KillPlayer() {
            this.swarmItem.Kill();
        }

        /// <summary>
        /// Plays the specified clip at the origin
        /// </summary>
        /// <param name="clip"></param>
        public void Play(AudioClip clip) {
            Play(clip, VectorUtils.ZERO);
        }

        /**
         * Initializes the player with a clip and position. Plays the sound effect. Auto kills self.
         */
        public void Play(AudioClip clip, Vector3 position) {
            this.source.clip = clip;
            this.selfTransform.position = position;

            this.fsm.Start(PLAYING);
        }

        void Update() {
            this.fsm.Update();
        }

        public float Volume {
            get {
                return this.source.volume;
            }

            set {
                this.source.volume = value;
            }
        }

        public AudioSource Source {
            get {
                return source;
            }

            set {
                source = value;
            }
        }

        public PlayerType EffectPlayerType {
            get {
                return playerType;
            }

            set {
                playerType = value;
            }
        }

        public Vector3 PlayerPosition {
            get {
                return this.selfTransform.position;
            }
            set {
                this.selfTransform.position = value;
            }
        }
    }
    
}
