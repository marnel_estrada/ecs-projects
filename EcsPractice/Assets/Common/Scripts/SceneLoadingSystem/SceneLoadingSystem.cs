﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using UnityEngine;
using UnityEngine.SceneManagement;

using Common.Xml;

namespace Common {
    public class SceneLoadingSystem : MonoBehaviour {

        [SerializeField]
        private TextAsset configXml;

        [SerializeField]
        private string loadProfileToExecute;

        private Dictionary<string, LoadProfile> profileMap;
        private Dictionary<string, SceneSet> sceneSetMap;

        void Awake() {
            Assertion.AssertNotNull(configXml);

            this.profileMap = new Dictionary<string, LoadProfile>();
            this.sceneSetMap = new Dictionary<string, SceneSet>();
            Parse();

            if(!string.IsNullOrEmpty(this.loadProfileToExecute)) {
                Load(this.loadProfileToExecute);
            }
        }

        // elements
        private const string LOAD_PROFILE = "LoadProfile";
        private const string SCENE_SET = "SceneSet";

        private void Parse() {
            SimpleXmlReader reader = new SimpleXmlReader();
            SimpleXmlNode root = reader.Read(this.configXml.text).FindFirstNodeInChildren("SceneLoadingSystem");

            for(int i = 0; i < root.Children.Count; ++i) {
                SimpleXmlNode child = root.Children[i];

                switch(child.TagName) {
                    case LOAD_PROFILE:
                        ParseLoadProfile(child);
                        break;

                    case SCENE_SET:
                        ParseSceneSet(child);
                        break;
                }
            }
        }
         
        // attributes
        private const string ID = "id";

        private void ParseLoadProfile(SimpleXmlNode node) {
            string id = node.GetAttribute(ID);
            LoadProfile profile = new LoadProfile(id);

            // parse scene sets in the profile
            for(int i = 0; i < node.Children.Count; ++i) {
                SimpleXmlNode child = node.Children[i];

                if(SCENE_SET.Equals(child.TagName)) {
                    // add the id
                    profile.AddSceneSet(child.GetAttribute(ID));
                }
            }

            this.profileMap[id] = profile;
        }

        private const string SCENE = "Scene";
        private const string NAME = "name";

        private void ParseSceneSet(SimpleXmlNode node) {
            string id = node.GetAttribute(ID);
            SceneSet sceneSet = new SceneSet(id);

            // parse scenes in the set
            for(int i = 0; i < node.Children.Count; ++i) {
                SimpleXmlNode child = node.Children[i];
                if(SCENE.Equals(child.TagName)) {
                    sceneSet.Add(child.GetAttribute(NAME));
                }
            }

            this.sceneSetMap[id] = sceneSet;
        }

        /// <summary>
        /// Loads the specified load profile
        /// </summary>
        /// <param name="loadProfileId"></param>
        public void Load(string loadProfileId) {
            LoadProfile loadProfile = null;
            Assertion.Assert(this.profileMap.TryGetValue(loadProfileId, out loadProfile));

            for(int i = 0; i < loadProfile.Count; ++i) {
                LoadSceneSet(loadProfile.GetSceneSetAt(i));
            }
        }

        private void LoadSceneSet(string sceneSetId) {
            SceneSet sceneSet = null;
            Assertion.Assert(this.sceneSetMap.TryGetValue(sceneSetId, out sceneSet));

            for(int i = 0; i < sceneSet.Count; ++i) {
                LoadSceneAdditively(sceneSet.GetAt(i));
            }
        }

        /// <summary>
        /// Common algorithm for loading a scene additively
        /// </summary>
        /// <param name="sceneName"></param>
        public static void LoadSceneAdditively(string sceneName) {
            UnityEngine.SceneManagement.SceneManager.LoadScene(sceneName, LoadSceneMode.Additive);
        }

    }
}
