﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common {
    internal class LoadProfile {

        private IdentifiableSet<string> set;

        /// <summary>
        /// Constructor with specified ID
        /// </summary>
        /// <param name="id"></param>
        public LoadProfile(string id) {
            this.set = new IdentifiableSet<string>(id);
        }

        public string Id {
            get {
                return this.set.Id;
            }
        }

        /// <summary>
        /// Adds a scene set
        /// </summary>
        /// <param name="sceneSetId"></param>
        public void AddSceneSet(string sceneSetId) {
            this.set.Add(sceneSetId);
        }

        public int Count {
            get {
                return this.set.Count;
            }
        }

        /// <summary>
        /// Returns the scene set ID at the specified index
        /// </summary>
        /// <param name="index"></param>
        /// <returns></returns>
        public string GetSceneSetAt(int index) {
            return this.set.GetAt(index);
        }

    }
}
