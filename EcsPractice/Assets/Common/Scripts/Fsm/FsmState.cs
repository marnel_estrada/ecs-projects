using System;
using System.Collections.Generic;

using Common.Utils;

namespace Common.Fsm {
	public class FsmState {
		
		private readonly string name;
		private readonly Fsm owner;
		
		private readonly Dictionary<string, FsmState> transitionMap = new Dictionary<string, FsmState>();
		private readonly SimpleList<FsmAction> actionList = new SimpleList<FsmAction>(1);

        /**
		 * Constructor
		 */
        public FsmState(string name, Fsm owner) {
			this.name = name;
			this.owner = owner;
		}

		public string GetName() {
			return name;
		}

		public void AddTransition(string eventId, FsmState destinationState) {
			// can't have two transitions for the same event
			Assertion.Assert(!transitionMap.ContainsKey(eventId), string.Format("The state {0} already contains a transition for event {1}.", this.name, eventId));
			transitionMap[eventId] = destinationState;
		}

		public FsmState GetTransition(string eventId) {
			return transitionMap.Find(eventId);
		}

		public void AddAction(FsmAction action) {
			Assertion.Assert(!actionList.Contains(action), "The state already contains the specified action.");
            action.Owner = this; // We set the owner here instead of making the action know its owner
			actionList.Add(action);
		}
        
        public SimpleList<FsmAction> ActionList {
            get {
                return actionList;
            }
        }

        public void SendEvent(string eventId) {
			this.owner.SendEvent(eventId);
		}

	}
}

