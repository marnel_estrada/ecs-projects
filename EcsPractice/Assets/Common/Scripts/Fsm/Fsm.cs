using System;
using System.Collections.Generic;
using Common.Logger;
using Common.Utils;
using UnityEngine;

namespace Common.Fsm {
	public class Fsm {
		
		private readonly string name;
		
		private FsmState currentState;
		private readonly Dictionary<string, FsmState> stateMap = new Dictionary<string, FsmState>();
        
        private StateActionProcessor onExitProcessor;

        private bool delayTransitionToNextFrame = false;
        private FsmState delayedTransitionState = null;

        private bool ownerExists; // Used in ECS
		
		/**
		 * Constructor
		 */
		public Fsm(string name, bool delayTransitionToNextFrame = false) {
			this.name = name;
            this.delayTransitionToNextFrame = delayTransitionToNextFrame;
			currentState = null;

            this.onExitProcessor = delegate (FsmAction action) {
                FsmState currentStateOnInvoke = this.currentState;
                action.OnExit();
                if (this.currentState != currentStateOnInvoke || this.delayedTransitionState != null) {
                    // this means that the action's OnExit() causes the FSM to change state
                    // note that states should not change state on exit
                    throw new Exception("State cannot be changed on exit of the specified state.");
                }
            };
        }
		
		/**
		 * Returns the name of the FSM.
		 */
		public string Name {
			get {
				return name;
			}
		}

        public bool OwnerExists {
            get {
                return ownerExists;
            }

            set {
                ownerExists = value;
            }
        }

        /**
		 * Adds a state to the FSM.
		 */
        public FsmState AddState(string name) {
			// state names should be unique
			Assertion.Assert(!stateMap.ContainsKey(name), name);
			
			FsmState newState = new FsmState(name, this);
			stateMap[name] = newState;
			return newState;
		}
		
		private delegate void StateActionProcessor(FsmAction action);
		
		private void ProcessStateActions(FsmState state, StateActionProcessor actionProcessor) {
			FsmState stateOnInvoke = this.currentState;

            SimpleList<FsmAction> actionList = state.ActionList;
            FsmAction[] actions = actionList.Buffer;
            int count = actionList.Count;
            for(int i = 0; i < count; ++i) {
                actionProcessor(actions[i]);

                if (this.currentState != stateOnInvoke) {
                    // this means that the action processing caused a state change
                    // we don't continue with the rest of the actions
                    break;
                }
            }
		}
		
		/**
		 * Starts the FSM with the specified state name as the starting state.
		 */
		public void Start(string stateName) {
            this.delayedTransitionState = null; // Reset so there's no unexpected transitions

            FsmState state = stateMap.Find(stateName);
            Assertion.AssertNotNull(state);
			ChangeToState(state);
		}

        /// <summary>
        /// Stops the FSM
        /// </summary>
        public void Stop() {
            this.delayedTransitionState = null; // Reset so there's no unexpected transitions
            ChangeToState(null);
        }
		
		private void ChangeToState(FsmState state) {
			if(this.currentState != null) {
				// if there's an active current state, we exit that first
				ExitState(this.currentState);
			}
			
			this.currentState = state;
			EnterState(this.currentState);
		}
		
		private void EnterState(FsmState state) {
            if(state == null) {
                // Null may be specified to stop the FSM
                return;
            }

            // We inlined ProcessStateActions() here so it would be faster
            FsmState stateOnInvoke = this.currentState;

            SimpleList<FsmAction> actionList = state.ActionList;
            FsmAction[] actions = actionList.Buffer;
            int count = actionList.Count;
            for (int i = 0; i < count; ++i) {
                actions[i].OnEnter();

                // We also check for delayedTransitionState because it means that the FSM is already scheduled for changing
                // to another state
                if (this.currentState != stateOnInvoke || this.delayedTransitionState != null) {
                    // this means that the action processing caused a state change
                    // we don't continue with the rest of the actions
                    break;
                }
            }
        }
		
		private void ExitState(FsmState state) {
			ProcessStateActions(state, this.onExitProcessor);
		}
		
		/**
		 * Updates the current state.
		 */
		public void Update() {
			if(this.currentState == null) {
				return;
			}

            // Check if there was a state that was supposed to be transitioned to
            if(this.delayTransitionToNextFrame && this.delayedTransitionState != null) {
                // We cached the transition state here and set delayedTransitionState to null
                // because ChangeToState() invokes ExitState() which does not allow transition.
                // It considers this.delayedTransitionState != null as a transition so it fails assertion.
                FsmState transitionState = this.delayedTransitionState;
                this.delayedTransitionState = null;
                ChangeToState(transitionState);
            }

            // We inlined the code from ProcessStateActions() here so that Update() is a little bit faster
            FsmState statePriorToUpdate = this.currentState;

            SimpleList<FsmAction> actionList = this.currentState.ActionList;
            FsmAction[] actions = actionList.Buffer;
            int count = actionList.Count;
            for (int i = 0; i < count; ++i) {
                actions[i].OnUpdate();

                // We also check for delayedTransitionState because it means that the FSM is already scheduled for changing
                // to another state
                if (this.currentState != statePriorToUpdate || this.delayedTransitionState != null) {
                    // this means that the action processing caused a state change
                    // we don't continue with the rest of the actions
                    break;
                }
            }
		}
		
		/**
		 * Returns the current state.
		 */
		public FsmState GetCurrentState() {
			return this.currentState;
		}
		
		/**
		 * Sends an event which may cause state change.
		 */
		public void SendEvent(string eventId) {
			Assertion.Assert(!string.IsNullOrEmpty(eventId), "The specified eventId can't be empty.");
			
			if(currentState == null) {
#if UNITY_EDITOR
                QuickLogger.Log(string.Format("Fsm {0} does not have a current state. Check if it was started.", this.name));
#endif
                return;
			}

			FsmState transitionState = this.currentState.GetTransition(eventId);
			if(transitionState == null) {
#if UNITY_EDITOR
                // log only in Unity Editor since it lags the game even if done in build
                QuickLogger.Log(string.Format("The current state {0} has no transtion for event {1}.", this.currentState.GetName(), eventId));
#endif
            } else {
                if (this.delayTransitionToNextFrame) {
                    // Do transition to next frame
                    this.delayedTransitionState = transitionState;
                } else {
                    // No delay. Change transition right away.
                    ChangeToState(transitionState);
                }
			}
		}

	}
}
