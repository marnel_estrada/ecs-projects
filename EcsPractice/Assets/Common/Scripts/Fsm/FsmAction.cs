using System;

namespace Common.Fsm {
	/**
	 * Base class for FSM actions
	 */
	public abstract class FsmAction {
		
		private FsmState owner;

        /**
		 * Constructor
		 */
        public FsmAction() {
		}

        public FsmState Owner {
            get {
                return owner;
            }

            set {
                Assertion.Assert(this.owner == null); // Should only be set once
                owner = value;
            }
        }
        
		public virtual void OnEnter() {
			// may or may not be implemented by deriving class
		}

		public virtual void OnUpdate() {
			// may or may not be implemented by deriving class
		}

		public virtual void OnExit() {
			// may or may not be implemented by deriving class
		}

	}
}
