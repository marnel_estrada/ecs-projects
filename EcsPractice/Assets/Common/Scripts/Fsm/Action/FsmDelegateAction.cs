using System;

namespace Common.Fsm {
	/**
	 * A pre created action class that uses delegates.
	 */
	public class FsmDelegateAction : FsmAction {
		
		public delegate void FsmActionRoutine(FsmState owner);
		
		private FsmActionRoutine onEnterRoutine;
		private FsmActionRoutine onUpdateRoutine;
		private FsmActionRoutine onExitRoutine;
		
		/**
		 * Constructor with OnEnter routine.
		 */
		public FsmDelegateAction(FsmActionRoutine onEnterRoutine) : this(onEnterRoutine, null, null) {
		}
		
		/**
		 * Constructor with OnEnter, OnUpdate and OnExit routines.
		 */
		public FsmDelegateAction(FsmActionRoutine onEnterRoutine, FsmActionRoutine onUpdateRoutine, FsmActionRoutine onExitRoutine = null) {
			this.onEnterRoutine = onEnterRoutine;
			this.onUpdateRoutine = onUpdateRoutine;
			this.onExitRoutine = onExitRoutine;
		}
		
		public override void OnEnter() {
			if(onEnterRoutine != null) {
				onEnterRoutine(this.Owner);
			}
		}
		
		public override void OnUpdate() {
			if(onUpdateRoutine != null) {
				onUpdateRoutine(this.Owner);
			}
		}
		
		public override void OnExit() {
			if(onExitRoutine != null) {
				onExitRoutine(this.Owner);
			}
		}

	}
}

