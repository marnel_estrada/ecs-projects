using UnityEngine;
using System.Collections;

using Common.Logger;

/**
 * A component that can be attached to a GameObject so it could run Logger updates.
 */
public class LoggerComponent : MonoBehaviour {
	
	[SerializeField]
	private string loggerName;
	
	private Common.Logger.Logger logger;
	
	void Awake() {
		logger = Common.Logger.Logger.GetInstance();
		logger.SetName(loggerName);
	}
	
	// Update is called once per frame
	void Update() {
		logger.Update();
	}

}
