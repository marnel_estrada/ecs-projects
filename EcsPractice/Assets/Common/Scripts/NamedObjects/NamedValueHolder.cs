﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common {
    /// <summary>
    /// Just a stub interface so we could refer to a NamedValue as one interface
    /// </summary>
    public interface NamedValueHolder : Named, ValueHolder {
    }
}
