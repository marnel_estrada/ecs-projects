using System;

namespace Common {
	/**
	 * Common interface for named objects
	 */
	public interface Named {
		
		/**
		 * Returns a name
		 */
		string Name {get; set;}
		
	}
}

