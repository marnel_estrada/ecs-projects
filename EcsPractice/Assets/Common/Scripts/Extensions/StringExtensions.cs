﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common {
    /// <summary>
    /// Contains string extension methods
    /// </summary>
    public static class StringExtensions {

        /// <summary>
        /// Returns whether or not the string contains the specified search criteria without case sensitivity
        /// </summary>
        /// <param name="source"></param>
        /// <param name="search"></param>
        /// <returns></returns>
        public static bool CaseInsensitiveContains(this string source, string search) {
            return source.ToLower().Contains(search.ToLower());
        }

        /// <summary>
        /// Custom equals that require a string parameter to avoid mistake of passing any object
        /// </summary>
        /// <param name="source"></param>
        /// <param name="other"></param>
        /// <returns></returns>
        public static bool EqualsString(this string source, string other) {
            return source.Equals(other);
        }

        /// <summary>
        /// Formats the string with the specified parameters
        /// </summary>
        /// <param name="source"></param>
        /// <param name="parameters"></param>
        /// <returns></returns>
        public static string FormatWith(this string source, params object[] parameters) {
            return string.Format(source, parameters);
        }

    }
}
