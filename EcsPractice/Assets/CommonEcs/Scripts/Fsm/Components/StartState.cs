﻿using System;

using Unity.Entities;

namespace Common.Ecs.Fsm {
    /// <summary>
    /// A component tag to identify which state to start to
    /// </summary>
    struct StartState : IComponentData {
    }
}
