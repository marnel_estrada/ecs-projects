using Common;
using Common.Fsm;

using Unity.Entities;

using UnityEngine;

namespace CommonEcs {
    /// <summary>
    /// This is like MoveAction from Fsm.Action but uses ECS to update the transforms
    /// </summary>
    public class MoveByDuration : FsmAction {
        // The entity whose transform will be updated
        private GameObjectEntity goEntity;
        private Entity entity;

        private Transform transform;
        private Vector3 positionFrom; 
        private Vector3 positionTo;
        private Space space;
        private float duration;
        private string finishEvent;
        
        private readonly CountdownTimer timer;

        private readonly CollectedCommandsSystem collectedCommands;
        private readonly EntityManager entityManager;

        private bool active;

        public MoveByDuration(FsmState owner, string timeReferenceName) {
            this.timer = new CountdownTimer(1, timeReferenceName);
            this.collectedCommands = World.Active.GetOrCreateSystem<CollectedCommandsSystem>();
            this.entityManager = World.Active.EntityManager;
        }
        
        public void Init(Transform transform, Vector3 positionFrom, Vector3 positionTo, float duration, string finishEvent, Space space = Space.World) {
            this.transform = transform;
            this.goEntity = transform.GetRequiredComponent<GameObjectEntity>();
            
            this.positionFrom = positionFrom;
            this.positionTo = positionTo;
            this.space = space;

            this.duration = duration;
            this.active = false;
            
            this.finishEvent = finishEvent;
        }
        
        public override void OnEnter() {
            if(Comparison.TolerantEquals(this.duration, 0)) {
                Finish();
                return;
            }
			
            if(VectorUtils.Equals(this.positionFrom, this.positionTo)) {
                // positionFrom and positionTo are already the same
                Finish();
                return;
            }
            
            // Proceed to movement
            SetPosition(this.positionFrom);
            this.timer.Reset(this.duration);
            
            // We always query the entity because it may already be a new one when the object was
            // used from a recycled object (deactivated and activated again)
            this.entity = this.goEntity.Entity;
            
            EntityCommandBuffer buffer = this.collectedCommands.Buffer;
            buffer.SetComponent(this.entity, new Move() {
                positionFrom = this.positionFrom,
                positionTo = this.positionTo,
                space = this.space
            });

            buffer.SetComponent(this.entity, new DurationTimer() {
                durationTime = this.duration,
                polledTime = 0
            });
            
            // We add the Active component so it will be filtered by the system that will process the movement
            buffer.AddComponent(this.entity, new Active());
            this.active = true;
        }
        
        public override void OnUpdate() {
            this.timer.Update();
			
            if(this.timer.HasElapsed()) {
                Finish();
            }
			
            // No need to explicitly set the position here as the ECS move system will be the one to execute it
        }

        public override void OnExit() {
            // We remove the Active tag so it will no longer be processed by the MoveSystem
            RemoveActive();
        }

        private void RemoveActive() {
            if (this.active) {
                if (this.entityManager.Exists(this.entity)) {
                    EntityCommandBuffer buffer = this.collectedCommands.Buffer;
                    buffer.RemoveComponent<Active>(this.entity);
                }

                this.active = false;
            }
        }

        private void SetPosition(Vector3 position) {
            switch(this.space) {
                case Space.World:
                    this.transform.position = position;
                    break;

                case Space.Self:
                    this.transform.localPosition = position;
                    break;
            }
        }
        
        private void Finish() {
            // snap to destination
            SetPosition(this.positionTo);
            RemoveActive();
			
            if(!string.IsNullOrEmpty(this.finishEvent)) {
                this.Owner.SendEvent(this.finishEvent);
            }
        }
    }
}