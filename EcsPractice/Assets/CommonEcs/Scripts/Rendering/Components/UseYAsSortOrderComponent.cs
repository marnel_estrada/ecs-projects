﻿namespace CommonEcs {
    using Unity.Entities;

    /// <summary>
    /// Wrapper for UseYAsSortOrder ECS component
    /// </summary>
    public class UseYAsSortOrderComponent : ComponentDataProxy<UseYAsSortOrder> {
    }
}