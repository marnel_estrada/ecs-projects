﻿using System.Collections.Generic;

using UnityEngine;
using UnityEngine.Assertions;

namespace Chronos {
    public class TimelineEffectorManager : MonoBehaviour {

        private readonly List<TimelineEffector> effectors = new List<TimelineEffector>();

        /// <summary>
        /// Adds a TimelineEffector instance
        /// </summary>
        /// <param name="effector"></param>
        public void Add(TimelineEffector effector) {
            this.effectors.Add(effector);
        }

        /// <summary>
        /// Removes the specified effector
        /// </summary>
        /// <param name="effector"></param>
        public void Remove(TimelineEffector effector) {
            this.effectors.Remove(effector);
        }

        private void Update() {
            int count = this.effectors.Count;
            for(int i = 0; i < count; ++i) {
                this.effectors[i].ExecuteUpdate(); 
            }
        }

        private void FixedUpdate() {
            int count = this.effectors.Count;
            for (int i = 0; i < count; ++i) {
                this.effectors[i].ExecuteFixedUpdate();
            }
        }

        // The only instance
        private static TimelineEffectorManager INSTANCE = null;

        public static TimelineEffectorManager Instance {
            get {
                if(INSTANCE == null) {
                    GameObject go = new GameObject("TimelineEffectorManager");
                    DontDestroyOnLoad(go);
                    INSTANCE = go.AddComponent<TimelineEffectorManager>();
                    Assert.IsNotNull(INSTANCE);
                }

                return INSTANCE;
            }
        }

    }
}
