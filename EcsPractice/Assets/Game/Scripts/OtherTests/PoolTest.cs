using System.Runtime.InteropServices;

using CommonEcs;

using Unity.Collections;
using Unity.Entities;
using Unity.Jobs;

using UnityEngine;

namespace Game {
    [AlwaysUpdateSystem]
    public class PoolTest : JobComponentSystem {
        private readonly Pool<NativeList<int>> pool = new Pool<NativeList<int>>(delegate {
            return new NativeList<int>(10, Allocator.Persistent);
        });

        protected override void OnCreateManager() {
            this.Enabled = false;
        }

        protected override void OnDestroyManager() {
            this.pool.Dispose();
        }
    
        protected override JobHandle OnUpdate(JobHandle inputDeps) {
            if (Input.GetKeyDown(KeyCode.Space)) {
                PoolEntry<NativeList<int>> poolEntry = this.pool.Request();
                
                SampleJob job = new SampleJob() {
                    list = poolEntry.instance
                };
                JobHandle handle = job.Schedule(inputDeps);
    
                RecycleJob recycleJob = new RecycleJob() {
                    handle = GCHandle.Alloc(this.pool),
                    poolEntry = poolEntry
                };
                handle = recycleJob.Schedule(handle);
    
                return handle;
            }
            
            return inputDeps;
        }
    
        private struct SampleJob : IJob {
            public NativeList<int> list;
            
            public void Execute() {
                Debug.Log("SampleJob");
            }
        }
    
        private struct RecycleJob : IJob {
            public GCHandle handle;
            public PoolEntry<NativeList<int>> poolEntry;
    
            public void Execute() {
                Pool<NativeList<int>> pool = (Pool<NativeList<int>>) this.handle.Target;
                pool.Recycle(this.poolEntry);
                
                this.handle.Free();
            }
        }
    }
}