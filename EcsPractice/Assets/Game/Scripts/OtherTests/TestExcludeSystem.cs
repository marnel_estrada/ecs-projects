using Unity.Entities;
using Unity.Transforms;

namespace Game {
    public struct A : IComponentData {
    }

    public struct B : IComponentData {
    }

    public class TestExcludeSystem : ComponentSystem {
        private EntityQuery query;

        protected override void OnCreateManager() {
            this.query = GetEntityQuery(typeof(A), typeof(LocalToWorld), ComponentType.Exclude<C>());

            this.EntityManager.CreateEntity(typeof(A), typeof(LocalToWorld));
            this.EntityManager.CreateEntity(typeof(A), typeof(LocalToWorld), typeof(C));

            this.Enabled = false;
        }

        protected override void OnUpdate() {
        }
        
        public struct C : ISystemStateComponentData {
        }
    }
}