using Unity.Entities;
using Unity.Jobs;
using Unity.Mathematics;

namespace Game {
    [AlwaysUpdateSystem]
    public class ParallelIJobSystem : JobComponentSystem {
        protected override void OnCreate() {
            this.Enabled = false;
        }

        protected override JobHandle OnUpdate(JobHandle inputDeps) {
            for (int i = 0; i < 6; ++i) {
                Job job = new Job();
                JobHandle handle = job.Schedule(inputDeps);
                JobHandle.CombineDependencies(handle, inputDeps);
            }
            
            return inputDeps;
        }

        private struct Job : IJob {
            public float total;

            public void Execute() {
                Random random = new Random(12345);

                for (int i = 0; i < 10000; ++i) {
                    float randomValue = random.NextFloat(1, 20);
                    this.total += math.sqrt(randomValue);
                }
            }
        }
    }
}