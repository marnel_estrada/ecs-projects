﻿using Unity.Entities;
using Unity.Mathematics;

namespace Game {
    public struct Velocity : IComponentData {
        public float3 value;
    }
}