﻿using Unity.Collections;
using Unity.Entities;
using Unity.Transforms;

using UnityEngine;

namespace Game {
    [UpdateInGroup(typeof(PresentationSystemGroup))]
    public class MoveByVelocitySystem : ComponentSystem {
        private EntityQuery query;
    
        private ArchetypeChunkComponentType<Translation> translationType;
        private ArchetypeChunkComponentType<Velocity> velocityType;
    
        protected override void OnCreateManager() {
            // Prepare group
            this.query = GetEntityQuery(typeof(Translation), typeof(Velocity));
        }
    
        protected override void OnUpdate() {
            // Get each ArchetypeChunkComponentType
            this.translationType = GetArchetypeChunkComponentType<Translation>();
            this.velocityType = GetArchetypeChunkComponentType<Velocity>();
    
            // Get the chunks and process each
            NativeArray<ArchetypeChunk> chunks = this.query.CreateArchetypeChunkArray(Allocator.TempJob);
            for (int i = 0; i < chunks.Length; ++i) {
                Process(chunks[i]);
            }
            
            chunks.Dispose();
        }
    
        private void Process(ArchetypeChunk chunk) {
            // Use the ArchetypeChunkComponentType to get an array of each component
            NativeArray<Translation> positions = chunk.GetNativeArray(this.translationType);
            NativeArray<Velocity> velocities = chunk.GetNativeArray(this.velocityType);
    
            // Update the components here
            for (int i = 0; i < chunk.Count; ++i) {
                positions[i] = new Translation() {
                    Value = velocities[i].value * Time.deltaTime
                };
            }
        }
    }
}