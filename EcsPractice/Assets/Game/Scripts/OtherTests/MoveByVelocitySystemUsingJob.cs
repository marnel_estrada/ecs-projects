﻿using Unity.Collections;
using Unity.Entities;
using Unity.Jobs;
using Unity.Transforms;

using UnityEngine;

namespace Game {
    [UpdateInGroup(typeof(PresentationSystemGroup))]
    public class MoveByVelocitySystemUsingJob : JobComponentSystem {
        private EntityQuery query;
        
        protected override void OnCreateManager() {
            this.query = GetEntityQuery(typeof(Translation), typeof(Velocity));
        }
        
        protected override JobHandle OnUpdate(JobHandle inputDeps) {
            NativeArray<ArchetypeChunk> chunks = this.query.CreateArchetypeChunkArray(Allocator.TempJob);
            
            Job job = new Job() {
                deltaTime = Time.deltaTime,
                translationType = GetArchetypeChunkComponentType<Translation>(),
                velocityType = GetArchetypeChunkComponentType<Velocity>(),
                chunks = chunks
            };
            
            return job.Schedule(chunks.Length, 64, inputDeps);
        }
    
        private struct Job : IJobParallelFor {
            public float deltaTime;
            
            public ArchetypeChunkComponentType<Translation> translationType;
            public ArchetypeChunkComponentType<Velocity> velocityType;
    
            [ReadOnly]
            [DeallocateOnJobCompletion]
            public NativeArray<ArchetypeChunk> chunks;
            
            public void Execute(int index) {
                Process(this.chunks[index]);
            }
            
            private void Process(ArchetypeChunk chunk) {
                NativeArray<Translation> positions = chunk.GetNativeArray(this.translationType);
                NativeArray<Velocity> velocities = chunk.GetNativeArray(this.velocityType);
    
                for (int i = 0; i < chunk.Count; ++i) {
                    positions[i] = new Translation() {
                        Value = velocities[i].value * this.deltaTime
                    };
                }
            }
        }
    }
}