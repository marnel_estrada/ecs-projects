﻿namespace Game {
    using Unity.Entities;

    using UnityEngine;
    
    public class FilterBySharedComponentBootstrap : MonoBehaviour {

        [SerializeField]
        private int sharedComponentCount = 7;
        
        public struct SharedComponent : ISharedComponentData {
            public byte value;
        }

        public struct ComponentData : IComponentData {
            public byte componentValue;
        }

        private EntityManager entityManager;

        private void Awake() {
            this.entityManager = World.Active.EntityManager;
            Assertion.AssertNotNull(this.entityManager);

            for (int i = 0; i < this.sharedComponentCount; ++i) {
                SharedComponent shared = CreateShared((byte)(i + 1));
                CreateComponents(ref shared, (byte)(i + 1), 13500);
            }
        }

        private SharedComponent CreateShared(byte value) {
            Entity entity = this.entityManager.CreateEntity();
            SharedComponent shared = new SharedComponent() {
                value = value
            };
            
            this.entityManager.AddSharedComponentData(entity, shared);

            return shared;
        }

        private void CreateComponents(ref SharedComponent shared, byte value, int count) {
            for (int i = 0; i < count; ++i) {
                CreateEntityWithComponent(ref shared, value);
            }
        }

        private void CreateEntityWithComponent(ref SharedComponent shared, byte value) {
            Entity entity = this.entityManager.CreateEntity();
            this.entityManager.AddSharedComponentData(entity, shared);
            
            ComponentData data = new ComponentData() {
                componentValue = value
            };
            this.entityManager.AddComponentData(entity, data);
        }

    }
}
