﻿using UnityEngine;

using System.Collections.Generic;

using CommonEcs;

using Unity.Collections;
using Unity.Entities;

namespace Game {
    [AlwaysUpdateSystem]
    public class FilterBySharedComponentSystem : ComponentSystem {
        private EntityQuery query;
        private SharedComponentQuery<FilterBySharedComponentBootstrap.SharedComponent> sharedComponentQuery;

        [ReadOnly]
        private ArchetypeChunkComponentType<FilterBySharedComponentBootstrap.ComponentData> componentType;

        protected override void OnCreateManager() {
            this.query = GetEntityQuery(typeof(FilterBySharedComponentBootstrap.ComponentData),
                typeof(FilterBySharedComponentBootstrap.SharedComponent));
            this.sharedComponentQuery =
                new SharedComponentQuery<FilterBySharedComponentBootstrap.SharedComponent>(this, this.EntityManager);
        }

        protected override void OnUpdate() {
            this.sharedComponentQuery.Update();

            IReadOnlyList<FilterBySharedComponentBootstrap.SharedComponent> sharedComponents =
                this.sharedComponentQuery.SharedComponents;

            this.componentType = GetArchetypeChunkComponentType<FilterBySharedComponentBootstrap.ComponentData>();

            // We start at 1 because the item at zero is a default value
            for (int i = 1; i < sharedComponents.Count; ++i) {
                this.query.SetFilter(sharedComponents[i]);

                NativeArray<FilterBySharedComponentBootstrap.ComponentData> components = this.query.ToComponentDataArray<FilterBySharedComponentBootstrap.ComponentData>(Allocator.TempJob);

                // Debug.Log is slow!!!
                Debug.Log($"Component count of shared component {sharedComponents[i].value}: {components.Length}");
            }
        }
    }
}