﻿using System;

using Unity.Entities;

namespace Game {
    public struct UfoRotation : IComponentData {

        public float rotationSpeed;
        public float angle; // This is in euler

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="rotationSpeed"></param>
        public UfoRotation(float rotationSpeed) {
            this.rotationSpeed = rotationSpeed;
            this.angle = 0;
        }

    }
}
