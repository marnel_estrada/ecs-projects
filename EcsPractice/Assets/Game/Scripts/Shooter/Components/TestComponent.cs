﻿using System;
using System.Collections.Generic;
using Unity.Collections;
using Unity.Entities;

namespace Game {
    public enum SomeState {
        Alive = 0, Dead = 1
    }

    public struct TestComponent : IComponentData {
        public SomeState state;
    }
}
