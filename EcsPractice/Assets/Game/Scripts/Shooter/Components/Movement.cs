﻿using Unity.Entities;
using Unity.Mathematics;
using UnityEngine;
     

namespace Game {
    public struct Movement : IComponentData {
        public float velocity;
        public float2 direction;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="velocity"></param>
        public Movement(float velocity) {
            this.velocity = velocity;
            this.direction = new float2();
        }

        /// <summary>
        /// Constructor with both velocity and direction
        /// </summary>
        /// <param name="velocity"></param>
        /// <param name="direction"></param>
        public Movement(float velocity, float2 direction) {
            this.velocity = velocity;
            this.direction = direction;
        }
    }
}