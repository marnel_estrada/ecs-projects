﻿using System;
using Unity.Entities;

namespace Game {
    public struct Weapon : IComponentData {
        public float projectileVelocity;
        public float life; // Life of the projectile in seconds

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="projectileVelocity"></param>
        public Weapon(float projectileVelocity, float life) {
            this.projectileVelocity = projectileVelocity;
            this.life = life;
        }
    }
}
