﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Unity.Entities;

namespace Game {
    public struct AggressiveAi : IComponentData {

        // This is the interpolant to use when interpolating between player position and enemy position
        public float interpolationValue;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="interpolationValue"></param>
        public AggressiveAi(float interpolationValue) {
            this.interpolationValue = interpolationValue;
        }

    }
}
