﻿using System;
using Common;
using Unity.Entities;

namespace Game {
    // Just a tag for now
    public struct WeaponProjectile : IComponentData {
        public ByteBool destroyed;

        /// <summary>
        /// Constructor
        /// </summary>
        public WeaponProjectile(bool destroyed) {
            this.destroyed = new ByteBool(destroyed);
        }
    }
}
