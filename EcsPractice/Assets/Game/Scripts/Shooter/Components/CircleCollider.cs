﻿using System;
using Unity.Entities;

namespace Game {
    public struct CircleCollider : IComponentData {

        public float radius;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="radius"></param>
        public CircleCollider(float radius) {
            this.radius = radius;
        }

    }
}
