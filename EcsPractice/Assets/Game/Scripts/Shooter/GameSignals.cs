﻿using System;

using Common;
using Common.Signal;

namespace Game {
    public static class GameSignals {

        public static readonly Signal GAME_OVER = new Signal("GameOver");
        public static readonly Signal RESET_GAME = new Signal("ResetGame");
        public static readonly Signal CREATE_PLAYER = new Signal("CreatePlayer");

    }
}
