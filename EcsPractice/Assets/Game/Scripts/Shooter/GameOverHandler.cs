﻿using System;
using Common;
using Common.Signal;
using UnityEngine;

namespace Game {
    class GameOverHandler : MonoBehaviour {

        [SerializeField]
        private GameObject gameOverRoot;

        private void Awake() {
            Assertion.AssertNotNull(this.gameOverRoot);

            this.gameOverRoot.Deactivate(); // Hidden by default

            GameSignals.GAME_OVER.AddListener(DoGameOver);
        }

        private void OnDestroy() {
            GameSignals.GAME_OVER.RemoveListener(DoGameOver);
        }

        private void DoGameOver(ISignalParameters parameters) {
            this.gameOverRoot.Activate(); // Show
            Time.timeScale = 0; // Pause the game
        }

        /// <summary>
        /// Requests to reset the game
        /// Used as button handler
        /// </summary>
        public void RequestResetGame() {
            GameSignals.RESET_GAME.Dispatch();

            Time.timeScale = 1;
            this.gameOverRoot.Deactivate();
        }

    }
}
