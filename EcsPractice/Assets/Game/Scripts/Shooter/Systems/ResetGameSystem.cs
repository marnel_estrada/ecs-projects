﻿using Unity.Entities;

namespace Game {
    [UpdateInGroup(typeof(PresentationSystemGroup))]
    public class ResetGameSystem : ComponentSystem {
        private bool reset = false;

        /// <summary>
        /// Marks the system for reset so it will execute the actual reset routines
        /// </summary>
        public void MarkReset() {
            this.reset = true;
        }

        protected override void OnUpdate() {
            // Execute only if for reset
            if(this.reset) {
                this.EntityManager.DestroyEntity(this.EntityManager.GetAllEntities());

                GameSignals.CREATE_PLAYER.Dispatch();
                this.reset = false;
            }
        }
    }
}
