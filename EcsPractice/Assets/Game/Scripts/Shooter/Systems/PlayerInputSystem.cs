﻿using Common;
using Unity.Entities;
using Unity.Mathematics;
using Unity.Transforms;
using UnityEngine;

using Quaternion = UnityEngine.Quaternion;

namespace Game {
    [UpdateInGroup(typeof(PresentationSystemGroup))]
    class PlayerInputSystem : ComponentSystem {
        private EntityQuery query;
        private Camera uiCamera;

        protected override void OnCreate() {
            this.query = GetEntityQuery(typeof(PlayerInput), typeof(Translation), typeof(Rotation), 
                typeof(Movement), typeof(Weapon), typeof(DurationTimer));
        }

        protected override void OnUpdate() {
            if(this.uiCamera == null) {
                // We get the fixed camera
                this.uiCamera = UnityUtils.GetRequiredComponent<Camera>("UiCamera");
            }

            Vector3 mousePosition3 = this.uiCamera.ScreenToWorldPoint(Input.mousePosition);
            Vector2 mousePosition = new Vector2(mousePosition3.x, mousePosition3.y);
            
            this.Entities.With(this.query).ForEach(delegate(ref Movement movement, ref Rotation rotation, 
                ref DurationTimer cooldownTimer, ref Translation translation, ref Weapon weapon) {
                UpdateKey(ref movement);
                UpdateMouseInput(mousePosition, ref rotation, ref cooldownTimer, ref translation, ref weapon);
            });
        }

        private void UpdateKey(ref Movement movement) {
            float xAxis = 0;

            if(Input.GetKey(KeyCode.A)) {
                xAxis -= 1;
            }

            if(Input.GetKey(KeyCode.D)) {
                xAxis += 1;
            }

            float yAxis = 0;

            if(Input.GetKey(KeyCode.S)) {
                yAxis -= 1;
            }

            if(Input.GetKey(KeyCode.W)) {
                yAxis += 1;
            }

            // Update Movement.direction here depending on the direction of each axis
            movement.direction = new Vector2(xAxis, yAxis).normalized;
        }

        private static readonly Vector2 RIGHT = new Vector2(1, 0);

        private void UpdateMouseInput(Vector2 mousePosition, ref Rotation rotation, ref DurationTimer cooldownTimer,
            ref Translation translation, ref Weapon weapon) {
            // Set the orientation of the ship
            // cos(t) = (A dot B) / (|A| * |B|);
            // Note here that we only divide by mousePosition's magnitude because RIGHT's magnitude is 1
            float cosine = Vector2.Dot(mousePosition, RIGHT) / mousePosition.magnitude;
            float angle = Mathf.Acos(cosine);
            float eulerAngle = Mathf.Rad2Deg * angle;

            // Check if it's on the 3rd or 4th quadrant
            if(mousePosition.y < 0) {
                eulerAngle = 360 - eulerAngle;
            }
            
            rotation = new Rotation{ Value = Quaternion.AngleAxis(eulerAngle, Vector3.forward) };

            // Check for firing
            if(Input.GetMouseButton(0)) {
                // Held left click 
                // Check if can fire
                if(cooldownTimer.HasElapsed) {
                    SpawnLaser(ref translation, ref weapon, mousePosition, eulerAngle);

                    // Reset the cooldown
                    cooldownTimer.polledTime = cooldownTimer.polledTime - cooldownTimer.durationTime;
                }
            }
        }

        private static readonly CircleCollider LASER_COLLIDER = new CircleCollider(0.03515625f);

        private void SpawnLaser(ref Translation translation, ref Weapon weapon, Vector2 direction, float orientation) {
            Entity entity = this.PostUpdateCommands.CreateEntity(GameBootstrap.LASER_ARCHETYPE);

            float3 shipPosition = translation.Value;
            this.PostUpdateCommands.SetComponent(entity, new Translation { Value = new float3(shipPosition.x, shipPosition.y, 0) });
            this.PostUpdateCommands.SetComponent(entity, new Rotation { Value = Quaternion.AngleAxis(orientation, Vector3.forward) });

            this.PostUpdateCommands.SetComponent(entity, new Movement(weapon.projectileVelocity, direction.normalized));
            this.PostUpdateCommands.SetComponent(entity, new DurationTimer(weapon.life, 0));
            this.PostUpdateCommands.SetComponent(entity, new WeaponProjectile(false)); // Start as not destroyed
            this.PostUpdateCommands.SetComponent(entity, LASER_COLLIDER);

            this.PostUpdateCommands.AddSharedComponent(entity, GameBootstrap.LASER_VIEW);
        }

    }
}
