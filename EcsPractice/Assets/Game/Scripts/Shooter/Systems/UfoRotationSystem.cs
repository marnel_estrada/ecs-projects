﻿using Unity.Entities;
using Unity.Jobs;
using Unity.Transforms;
using UnityEngine;
using Unity.Burst;

namespace Game {
    [UpdateInGroup(typeof(PresentationSystemGroup))]
    public class UfoRotationSystem : JobComponentSystem {
        [BurstCompile]
        private struct RotateJob : IJobForEach<Rotation, UfoRotation> {
            public float deltaTime;

            public void Execute(ref Rotation rotation, ref UfoRotation ufoRotation) {
                ufoRotation.angle += ufoRotation.rotationSpeed * deltaTime;
                if (ufoRotation.angle > 360) {
                    ufoRotation.angle -= 360; // Clamp
                }

                rotation.Value = Quaternion.AngleAxis(ufoRotation.angle, Vector3.forward);
            }
        }

        protected override JobHandle OnUpdate(JobHandle inputDeps) {
            RotateJob job = new RotateJob {
                deltaTime = Time.deltaTime
            };

            return job.Schedule(this, inputDeps);
        }
    }
}
