﻿using Unity.Entities;
using UnityEngine;

using Unity.Transforms;
using Unity.Mathematics;

namespace Game {
    [UpdateInGroup(typeof(PresentationSystemGroup))]
    public class EnemySpawnerSystem : ComponentSystem {
        private const float SPAWN_ACCELERATION = 0.125f; // Increases spawn rate (acts like acceleration)
        private const float SPAWN_RADIUS = 2.0f;

        private float polledTime;
        private float spawnRate = 1.0f; // This is spawn per second. Start with 1 per second
        private float spawnInterval;

        private bool waitPolledTime;

        private EntityQuery playerQuery;

        protected override void OnCreate() {
            this.playerQuery = GetEntityQuery(ComponentType.ReadOnly<PlayerInput>(), 
                ComponentType.ReadOnly<Translation>());
        }

        /// <summary>
        /// Resets the states of the spawner
        /// </summary>
        public void Reset() {
            this.polledTime = 0;
            this.spawnRate = 1.0f;
        }

        private float3 playerPosition;

        protected override void OnUpdate() {
            // Cache playerPosition
            this.Entities.With(this.playerQuery).ForEach(delegate(ref Translation translation) {
                this.playerPosition = translation.Value;
            });
            
            // Update the spawn rate
            this.spawnRate += SPAWN_ACCELERATION * Time.deltaTime;

            if (this.waitPolledTime) {
                this.polledTime += Time.deltaTime;

                if(Comparison.TolerantGreaterThanOrEquals(this.polledTime, this.spawnInterval)) {
                    // Time to spawn
                    while (this.polledTime >= this.spawnInterval) {
                        Spawn();
                        this.polledTime -= this.spawnInterval;
                    }
                    this.waitPolledTime = false;
                }
            } else {
                this.spawnInterval = 1.0f / this.spawnRate; // The time to wait before spawning
                this.waitPolledTime = true; // Change state
            }
        }

        private static readonly CircleCollider ENEMY_COLLIDER = new CircleCollider(0.0592448f);

        private void Spawn() {
            Entity entity = this.PostUpdateCommands.CreateEntity(GameBootstrap.ENEMY_ARCHETYPE);

            float randomAngle = UnityEngine.Random.Range(0f, 360f) * Mathf.Deg2Rad;
            float3 enemyPosition = this.playerPosition;
            enemyPosition.x += Mathf.Cos(randomAngle) * SPAWN_RADIUS;
            enemyPosition.y += Mathf.Sin(randomAngle) * SPAWN_RADIUS;
            this.PostUpdateCommands.SetComponent(entity, new Translation {  Value = enemyPosition });

            this.PostUpdateCommands.SetComponent(entity, new AggressiveAi(UnityEngine.Random.Range(0.005f, 0.025f)));
            this.PostUpdateCommands.SetComponent(entity, new UfoRotation(UnityEngine.Random.Range(90.0f, 180.0f)));
            this.PostUpdateCommands.SetComponent(entity, ENEMY_COLLIDER);

            this.PostUpdateCommands.AddSharedComponent(entity, GameBootstrap.ENEMY_VIEW);
        }
    }
}
