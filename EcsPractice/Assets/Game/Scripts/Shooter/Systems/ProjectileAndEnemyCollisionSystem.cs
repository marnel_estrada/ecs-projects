using CommonEcs;

using Unity.Collections;
using Unity.Entities;
using Unity.Mathematics;
using Unity.Transforms;

namespace Game {
    [UpdateInGroup(typeof(PresentationSystemGroup))]
    public class ProjectileAndEnemyCollisionSystem : TemplateComponentSystem {
        private EntityQuery enemyQuery;

        private ArchetypeChunkEntityType entityType;
        private ArchetypeChunkComponentType<Translation> translationType;
        private ArchetypeChunkComponentType<CircleCollider> colliderType;
        private ArchetypeChunkComponentType<WeaponProjectile> projectileType;

        protected override void OnCreate() {
            base.OnCreate();

            this.enemyQuery = GetEntityQuery(ComponentType.ReadOnly<Translation>(), 
                ComponentType.ReadOnly<CircleCollider>(), ComponentType.ReadOnly<AggressiveAi>());
        }

        protected override EntityQuery ComposeQuery() {
            // Main query is projectile
            return GetEntityQuery(ComponentType.ReadOnly<Translation>(), 
                ComponentType.ReadOnly<CircleCollider>(), typeof(WeaponProjectile));
        }

        protected override void BeforeChunkTraversal() {
            this.entityType = GetArchetypeChunkEntityType();
            this.translationType = GetArchetypeChunkComponentType<Translation>();
            this.colliderType = GetArchetypeChunkComponentType<CircleCollider>();
            this.projectileType = GetArchetypeChunkComponentType<WeaponProjectile>();
        }

        private NativeArray<Entity> projectileEntities;
        private NativeArray<Translation> projectileTranslations;
        private NativeArray<CircleCollider> projectileColliders;
        private NativeArray<WeaponProjectile> projectiles;

        protected override void BeforeProcessChunk(ArchetypeChunk chunk) {
            this.projectileEntities = chunk.GetNativeArray(this.entityType);
            this.projectileTranslations = chunk.GetNativeArray(this.translationType);
            this.projectileColliders = chunk.GetNativeArray(this.colliderType);
            this.projectiles = chunk.GetNativeArray(this.projectileType);
        }

        protected override void Process(int index) {
            // For each projectile, we compare it with all enemies
            this.Entities.With(this.enemyQuery).ForEach(delegate(Entity entity, ref Translation enemyTranslation, 
                ref CircleCollider enemyCollider) {
                CheckEnemyAndProjectileCollision(index, entity, ref enemyTranslation, ref enemyCollider);
            });
        }
        
        private void CheckEnemyAndProjectileCollision(int projectileIndex, Entity enemyEntity, ref Translation enemyTranslation, 
            ref CircleCollider enemyCollider) {
            WeaponProjectile projectile = this.projectiles[projectileIndex];
            if(projectile.destroyed.Value) {
                // Projectile is already destroyed
                // It may have collided with an enemy ship already
                return;
            }

            Translation projectilePosition = this.projectileTranslations[projectileIndex];
            CircleCollider projectileCollider = this.projectileColliders[projectileIndex];

            if(AreColliding(enemyTranslation, enemyCollider, projectilePosition, projectileCollider)) {
                // Set destroyed to true so that projectile may not be used to check for collision again
                this.projectiles[projectileIndex] = new WeaponProjectile(true);

                // There's a collision. Destroy the laser and the enemy.
                this.PostUpdateCommands.DestroyEntity(enemyEntity);
                this.PostUpdateCommands.DestroyEntity(this.projectileEntities[projectileIndex]);
            }
        }
        
        private static bool AreColliding(Translation posA, CircleCollider circleA, Translation posB, CircleCollider circleB) {
            float squareDistance = math.lengthsq(posA.Value - posB.Value);
            float squareRadiusLength = (circleA.radius + circleB.radius) * (circleA.radius + circleB.radius);

            // They are colliding if distance between them is less than the sum of their radius
            return Comparison.TolerantLesserThanOrEquals(squareDistance, squareRadiusLength);
        }
    }
}