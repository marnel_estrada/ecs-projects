﻿// using Unity.Collections;
// using Unity.Entities;
// using Unity.Transforms;
// using UnityEngine;
//
// namespace Game {
//     [UpdateInGroup(typeof(PresentationSystemGroup))]
//     public class CollisionSystem : ComponentSystem {
//         private struct ProjectileData {
//             public readonly int Length;
//
//             public EntityArray Entities;
//
//             [ReadOnly]
//             public ComponentDataArray<Translation> Translations;
//
//             [ReadOnly]
//             public ComponentDataArray<CircleCollider> Colliders;
//             
//             public ComponentDataArray<WeaponProjectile> Projectiles;
//         }
//
//         [Inject]
//         private ProjectileData projectiles;
//
//         private struct EnemyData {
//             public readonly int Length;
//
//             public EntityArray Entities;
//
//             [ReadOnly]
//             public ComponentDataArray<Translation> Translations;
//
//             [ReadOnly]
//             public ComponentDataArray<CircleCollider> Colliders;
//
//             [ReadOnly]
//             public ComponentDataArray<AggressiveAi> Ai;
//         }
//
//         [Inject]
//         private EnemyData enemies;
//
//         private struct PlayerData {
//             public readonly int Length;
//
//             public EntityArray Entities;
//
//             [ReadOnly]
//             public ComponentDataArray<Translation> Translations;
//
//             [ReadOnly]
//             public ComponentDataArray<CircleCollider> Colliders;
//
//             [ReadOnly]
//             public ComponentDataArray<PlayerInput> Players;
//         }
//
//         [Inject]
//         private PlayerData players;
//
//         private EntityQuery projectileQuery;
//         private EntityQuery enemyQuery;
//         private EntityQuery playerQuery;
//
//         protected override void OnUpdate() {
//             // Check projectile against enemies
//             for (int projectileIndex = 0; projectileIndex < this.projectiles.Length; ++projectileIndex) {
//                 for (int enemyIndex = 0; enemyIndex < this.enemies.Length; ++enemyIndex) {
//                     CheckEnemyAndProjectileCollision(enemyIndex, projectileIndex);
//                 }
//             }
//             
//             this.Entities.With(this.projectileQuery).ForEach(
//                 delegate(ref WeaponProjectile projectile, ref Translation translation, ref CircleCollider collider) {
//                     
//                 });
//
//             // Check enemies and player
//             if (this.players.Length > 0) {
//                 for (int i = 0; i < this.enemies.Length; ++i) {
//                     CheckEnemyAndPlayerCollision(i, 0); // Check only with the first player
//                 }
//             }
//         }
//
//         private void CheckEnemyAndProjectileCollision(int enemyIndex, int projectileIndex) {
//             WeaponProjectile projectile = this.projectiles.Projectiles[projectileIndex];
//             if(projectile.destroyed.Value) {
//                 // Projectile is already destroyed
//                 // It may have collided with an enemy ship already
//                 return;
//             }
//
//             Translation enemyPosition = this.enemies.Translations[enemyIndex];
//             CircleCollider enemyCollider = this.enemies.Colliders[enemyIndex];
//
//             Translation projectilePosition = this.projectiles.Translations[projectileIndex];
//             CircleCollider projectileCollider = this.projectiles.Colliders[projectileIndex];
//
//             if(AreColliding(enemyPosition, enemyCollider, projectilePosition, projectileCollider)) {
//                 // Set destroyed to true so that projectile may not be used to check for collision again
//                 this.projectiles.Projectiles[projectileIndex] = new WeaponProjectile(true);
//
//                 // There's a collision. Destroy the laser and the enemy.
//                 this.PostUpdateCommands.DestroyEntity(this.enemies.Entities[enemyIndex]);
//                 this.PostUpdateCommands.DestroyEntity(this.projectiles.Entities[projectileIndex]);
//             }
//         }
//
//         private bool CheckEnemyAndPlayerCollision(int enemyIndex, int playerIndex) {
//             Translation enemyPosition = this.enemies.Translations[enemyIndex];
//             CircleCollider enemyCollider = this.enemies.Colliders[enemyIndex];
//
//             Translation playerPosition = this.players.Translations[playerIndex];
//             CircleCollider playerCollider = this.players.Colliders[playerIndex];
//
//             if(AreColliding(enemyPosition, enemyCollider, playerPosition, playerCollider)) {
//                 // Ane enemy collided with the player. Game is over :(
//                 this.PostUpdateCommands.DestroyEntity(this.players.Entities[playerIndex]);
//                 GameSignals.GAME_OVER.Dispatch();
//                 return true;
//             }
//
//             return false;
//         }
//
//         private static bool AreColliding(Translation posA, CircleCollider circleA, Translation posB, CircleCollider circleB) {
//             float squareDistance = Vector3.SqrMagnitude(posA.Value - posB.Value);
//             float squareRadiusLength = (circleA.radius + circleB.radius) * (circleA.radius + circleB.radius);
//
//             // They are colliding if distance between them is less than the sum of their radius
//             return Comparison.TolerantLesserThanOrEquals(squareDistance, squareRadiusLength);
//         }
//     }
// }
