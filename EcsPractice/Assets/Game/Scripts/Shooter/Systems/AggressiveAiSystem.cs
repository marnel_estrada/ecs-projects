﻿using Unity.Collections;
using Unity.Entities;
using Unity.Mathematics;
using Unity.Transforms;
using UnityEngine;

namespace Game {
    /// <summary>
    /// System that makes enemies chase the player
    /// </summary>
    [UpdateInGroup(typeof(PresentationSystemGroup))]
    public class AggressiveAiSystem : ComponentSystem {
        private EntityQuery playerQuery;
        private EntityQuery enemyQuery;

        protected override void OnCreate() {
            this.playerQuery = GetEntityQuery(ComponentType.ReadOnly<PlayerInput>(), ComponentType.ReadOnly<Translation>());
            this.enemyQuery = GetEntityQuery(typeof(Translation), ComponentType.ReadOnly<AggressiveAi>());
        }

        protected override void OnUpdate() {
            if(Comparison.IsZero(Time.deltaTime)) {
                // Game is paused
                return;
            }

            float3 playerPosition = GetPlayerPosition();
            
            this.Entities.With(this.enemyQuery).ForEach(delegate(ref Translation translation, ref AggressiveAi ai) {
                float3 enemyPosition = translation.Value;
                translation.Value = enemyPosition + ((playerPosition - enemyPosition) * ai.interpolationValue);
            });
        }

        private float3 GetPlayerPosition() {
            NativeArray<Translation> translationsArray = this.playerQuery.ToComponentDataArray<Translation>(Allocator.TempJob);
            Assertion.Assert(translationsArray.Length > 0);
            float3 result = translationsArray[0].Value;
            translationsArray.Dispose();

            return result;
        }
    }
}
