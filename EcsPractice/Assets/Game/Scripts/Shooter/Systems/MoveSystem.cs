﻿using Unity.Burst;
using Unity.Entities;
using Unity.Jobs;
using Unity.Transforms;
using UnityEngine;

namespace Game {
    [UpdateInGroup(typeof(PresentationSystemGroup))]
    public class MoveSystem : JobComponentSystem {
        [BurstCompile]
        private struct MoveJob : IJobForEach<Movement, Translation> {
            public float deltaTime;

            public void Execute(ref Movement movement, ref Translation position) {
                position.Value.x += movement.direction.x * movement.velocity * deltaTime;
                position.Value.y += movement.direction.y * movement.velocity * deltaTime;
            }
        }

        protected override JobHandle OnUpdate(JobHandle inputDeps) {
            MoveJob job = new MoveJob {
                deltaTime = Time.deltaTime
            };

            return job.Schedule(this, inputDeps);
        }
    }
}
