using CommonEcs;

using Unity.Collections;
using Unity.Entities;
using Unity.Mathematics;
using Unity.Transforms;

namespace Game {
    [UpdateInGroup(typeof(PresentationSystemGroup))]
    public class EnemyAndPlayerCollisionSystem : TemplateComponentSystem {
        private EntityQuery playerQuery;
        
        private ArchetypeChunkComponentType<Translation> translationType;
        private ArchetypeChunkComponentType<CircleCollider> colliderType;

        protected override void OnCreate() {
            base.OnCreate();
            this.playerQuery = GetEntityQuery(ComponentType.ReadOnly<Translation>(), 
                ComponentType.ReadOnly<CircleCollider>(), ComponentType.ReadOnly<PlayerInput>());
        }

        protected override EntityQuery ComposeQuery() {
            // Main query is for enemies
            return GetEntityQuery(ComponentType.ReadOnly<Translation>(), 
                ComponentType.ReadOnly<CircleCollider>(), ComponentType.ReadOnly<AggressiveAi>());
        }

        private Entity playerEntity;
        private Translation playerPosition;
        private CircleCollider playerCollider;

        protected override void BeforeChunkTraversal() {
            this.translationType = GetArchetypeChunkComponentType<Translation>();
            this.colliderType = GetArchetypeChunkComponentType<CircleCollider>();
            
            // Cache player values
            this.Entities.With(this.playerQuery).ForEach(delegate(Entity entity, ref Translation translation, 
                ref CircleCollider collider) {
                this.playerEntity = entity;
                this.playerPosition = translation;
                this.playerCollider = collider;
            });
        }
        
        private NativeArray<Translation> enemyTranslations;
        private NativeArray<CircleCollider> enemyColliders;

        protected override void BeforeProcessChunk(ArchetypeChunk chunk) {
            this.enemyTranslations = chunk.GetNativeArray(this.translationType);
            this.enemyColliders = chunk.GetNativeArray(this.colliderType);
        }

        protected override void Process(int index) {
            if (AreColliding(this.enemyTranslations[index], this.enemyColliders[index], this.playerPosition,
                this.playerCollider)) {
                // Ane enemy collided with the player. Game is over :(
                this.PostUpdateCommands.DestroyEntity(this.playerEntity);
                GameSignals.GAME_OVER.Dispatch();
            }
        }
        
        private static bool AreColliding(Translation posA, CircleCollider circleA, Translation posB, CircleCollider circleB) {
            float squareDistance = math.lengthsq(posA.Value - posB.Value);
            float squareRadiusLength = (circleA.radius + circleB.radius) * (circleA.radius + circleB.radius);

            // They are colliding if distance between them is less than the sum of their radius
            return Comparison.TolerantLesserThanOrEquals(squareDistance, squareRadiusLength);
        }
    }
}