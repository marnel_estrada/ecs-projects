﻿using Common;

using Unity.Entities;
using Unity.Mathematics;
using Unity.Transforms;
using UnityEngine;

namespace Game {
    [UpdateInGroup(typeof(PresentationSystemGroup))]
    public class CameraFollowSystem : ComponentSystem {
        private EntityQuery query;

        private Transform cameraTransform;

        protected override void OnCreate() {
            this.query = GetEntityQuery(ComponentType.ReadOnly<PlayerInput>(), ComponentType.ReadOnly<Translation>());
        }

        protected override void OnUpdate() {
            if(this.cameraTransform == null) {
                // Resolve camera
                this.cameraTransform = UnityUtils.GetRequiredComponent<Transform>("WorldCamera");
            }
            
            this.Entities.With(this.query).ForEach(delegate(ref Translation translation) {
                float3 position = translation.Value;
                position.z = this.cameraTransform.position.z; // Retain z
                this.cameraTransform.position = position;
            });
        }
    }
}
