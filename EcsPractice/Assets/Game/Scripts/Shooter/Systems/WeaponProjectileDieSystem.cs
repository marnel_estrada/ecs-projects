﻿using Common;

using Unity.Entities;

namespace Game {
    [UpdateAfter(typeof(DurationTimerSystem))]
    public class WeaponProjectileDieSystem : ComponentSystem {
        private EntityQuery query;

        protected override void OnCreate() {
            this.query = GetEntityQuery(ComponentType.ReadOnly<WeaponProjectile>(), 
                ComponentType.ReadOnly<DurationTimer>());
        }

        protected override void OnUpdate() {   
            this.Entities.With(this.query).ForEach(delegate(Entity entity, ref DurationTimer timer) {
                if(timer.HasElapsed) {
                    // The weapon projectile has now to die
                    this.PostUpdateCommands.DestroyEntity(entity);
                }
            });
        }
    }
}
