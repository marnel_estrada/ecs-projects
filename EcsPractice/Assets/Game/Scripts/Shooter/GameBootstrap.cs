﻿using System.Collections;
using System.Collections.Generic;
using Common;
using Common.Signal;
using Unity.Entities;
using Unity.Mathematics;
using Unity.Rendering;
using Unity.Transforms;
using UnityEngine;

namespace Game {
    public sealed class GameBootstrap {

        private static EntityManager ENTITY_MANAGER;

        // Archetypes are like prefabs
        private static EntityArchetype PLAYER_ARCHETYPE;
        public static EntityArchetype LASER_ARCHETYPE;
        public static EntityArchetype ENEMY_ARCHETYPE;

        // Views
        private static RenderMesh PLAYER_SHIP_VIEW;
        public static RenderMesh LASER_VIEW;
        public static RenderMesh ENEMY_VIEW;

        // Comment this out if you want to run this bootstrap
        //[RuntimeInitializeOnLoadMethod(RuntimeInitializeLoadType.AfterSceneLoad)]
        private static void Initialize() {
            ENTITY_MANAGER = World.Active.EntityManager;

            PLAYER_ARCHETYPE = ENTITY_MANAGER.CreateArchetype(
                // We use 3D position here so we can use Quaternion for rotation (TransformSystem uses Position and Rotation components)
                typeof(Translation),
                typeof(Rotation),
                typeof(PlayerInput), 
                typeof(Movement),
                typeof(Weapon),
                typeof(DurationTimer), // Used for cooldown
                typeof(CircleCollider),
                typeof(TestComponent)
            );

            LASER_ARCHETYPE = ENTITY_MANAGER.CreateArchetype(
                typeof(Translation),
                typeof(Rotation),
                typeof(Movement),
                typeof(WeaponProjectile),
                typeof(DurationTimer), // Used as the life of the projectile
                typeof(CircleCollider)
            );

            ENEMY_ARCHETYPE = ENTITY_MANAGER.CreateArchetype(
                typeof(Translation),
                typeof(Rotation),
                typeof(AggressiveAi),
                typeof(UfoRotation),
                typeof(CircleCollider)
            );

            // Prepare views
            PLAYER_SHIP_VIEW = GetLookFromPrototype("PlayerShipView");
            LASER_VIEW = GetLookFromPrototype("LaserView");
            ENEMY_VIEW = GetLookFromPrototype("EnemyView");

            GameSignals.RESET_GAME.AddListener(ResetGame);
            GameSignals.CREATE_PLAYER.AddListener(CreatePlayer);

            CreatePlayer();
        }

        private static readonly CircleCollider PLAYER_COLLIDER = new CircleCollider(0.06445315f);

        /// <summary>
        /// Creates the player
        /// </summary>
        public static void CreatePlayer() {
            Entity entity = ENTITY_MANAGER.CreateEntity(PLAYER_ARCHETYPE);

            // Prepare components
            ENTITY_MANAGER.SetComponentData(entity, new Translation { Value = new float3(0.0f, 0.0f, 0.0f) });
            ENTITY_MANAGER.SetComponentData(entity, new Rotation());
            ENTITY_MANAGER.SetComponentData(entity, new Movement(1.0f));
            ENTITY_MANAGER.SetComponentData(entity, new Weapon(3.0f, 5.0f));
            ENTITY_MANAGER.SetComponentData(entity, new CircleCollider(0.06445315f));

            // Used for weapon cooldown. Starts ready to be fired
            float fireCooldown = 1.0f / 8.0f;
            ENTITY_MANAGER.SetComponentData(entity, new DurationTimer(fireCooldown, fireCooldown));

            ENTITY_MANAGER.AddSharedComponentData(entity, PLAYER_SHIP_VIEW);
        }

        private static RenderMesh GetLookFromPrototype(string protoName) {
            GameObject go = GameObject.Find(protoName);
            Assertion.AssertNotNull(go, protoName);
            RenderMesh result = go.GetComponent<SpriteInstanceRendererComponent>().Value;
            Object.Destroy(go);
            return result;
        }

        private static void ResetGame(ISignalParameters parameters) {
            // Clear all
            ResetGameSystem resetGame = World.Active.GetOrCreateSystem<ResetGameSystem>();
            resetGame.MarkReset();

            EnemySpawnerSystem spawner = World.Active.GetOrCreateSystem<EnemySpawnerSystem>();
            spawner.Reset();
        }

        // Just delegates to CreatePlayer(). Needed for signal.
        private static void CreatePlayer(ISignalParameters parameters) {
            CreatePlayer();
        }

    }
}
