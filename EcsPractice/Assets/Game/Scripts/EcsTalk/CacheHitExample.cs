using UnityEngine;

namespace Example {
    public struct GridCell {
        public int x;
        public int y;

        public float width;
        public float height;

        public Vector3 center;
    }

    public class Grid {
        private GridCell[] cells;
        private Vector3 center; // center of the grid
        
        // Translates the grid
        public void Translate(Vector3 delta) {
            this.center += delta;

            // Potential cache hit when accessing every cell
            for (int i = 0; i < this.cells.Length; ++i) {
                GridCell cell = this.cells[i];
                cell.center = cell.center + delta;
                this.cells[i] = cell; // Set again to update the data in the array (struct gotcha)
            }
        }
    }
}