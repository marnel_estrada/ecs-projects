using System.Collections.Generic;

using UnityEngine;

namespace Example {
    public class AI {
        public void CollectWorldData() {
        }

        public void Update() {
            // ... Update routines
        }
    }
    
    public class AiManager : MonoBehaviour {
        private List<AI> aiList = new List<AI>();

        private void Update() {
            // Potential cache miss for accessing every element in the list
            for (int i = 0; i < this.aiList.Count; ++i) {
                this.aiList[i].CollectWorldData();
                this.aiList[i].Update();
            }
        }
    }
}
