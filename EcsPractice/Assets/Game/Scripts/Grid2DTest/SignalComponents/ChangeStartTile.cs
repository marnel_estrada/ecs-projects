using Unity.Entities;
using Unity.Mathematics;

namespace Game {
    public struct ChangeStartTile : IComponentData {
        public int2 previousCoordinate;
        public int2 newCoordinate;
    }
}