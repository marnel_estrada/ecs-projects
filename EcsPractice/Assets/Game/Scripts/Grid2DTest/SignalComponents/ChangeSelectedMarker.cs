using Unity.Entities;

namespace Game {
    public struct ChangeSelectedMarker : IComponentData {
        public TileMarker newSelectedMarker;

        public ChangeSelectedMarker(TileMarker newSelectedMarker) {
            this.newSelectedMarker = newSelectedMarker;
        }
    }
}