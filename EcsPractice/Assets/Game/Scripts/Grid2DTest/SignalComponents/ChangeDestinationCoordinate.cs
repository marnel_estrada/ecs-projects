using Unity.Entities;
using Unity.Mathematics;

using UnityEngine;

namespace Game {
    public struct ChangeDestinationCoordinate : IComponentData {
        public int2 newCoordinate;

        public ChangeDestinationCoordinate(int2 newCoordinate) {
            this.newCoordinate = newCoordinate;
        }
    }
}