using Unity.Entities;
using Unity.Mathematics;

using UnityEngine;

namespace Game {
    public struct PaintBlocker : IComponentData {
        public int2 coordinate;

        public PaintBlocker(int2 coordinate) {
            this.coordinate = coordinate;
        }
    }
}