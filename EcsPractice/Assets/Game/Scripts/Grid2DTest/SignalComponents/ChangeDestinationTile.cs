using Unity.Entities;
using Unity.Mathematics;

namespace Game {
    public struct ChangeDestinationTile : IComponentData {
        public int2 previousCoordinate;
        public int2 newCoordinate;
    }
}