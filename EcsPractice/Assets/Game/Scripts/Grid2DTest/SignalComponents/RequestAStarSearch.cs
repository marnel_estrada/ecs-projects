using Unity.Entities;
using Unity.Mathematics;

namespace Game {
    public struct RequestAStarSearch : IComponentData {
        public Entity owner;
        public int2 start;
        public int2 goal;

        public RequestAStarSearch(Entity owner, int2 start, int2 goal) {
            this.owner = owner;
            this.start = start;
            this.goal = goal;
        }
    }
}
