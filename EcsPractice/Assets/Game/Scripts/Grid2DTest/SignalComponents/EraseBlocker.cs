using Unity.Entities;
using Unity.Mathematics;

namespace Game {
    public struct EraseBlocker : IComponentData {
        public int2 coordinate;

        public EraseBlocker(int2 coordinate) {
            this.coordinate = coordinate;
        }
    }
}