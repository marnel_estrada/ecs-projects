using Unity.Entities;
using Unity.Mathematics;

using UnityEngine;

namespace Game {
    public struct ChangeStartCoordinate : IComponentData {
        public int2 newCoordinate;

        public ChangeStartCoordinate(int2 newCoordinate) {
            this.newCoordinate = newCoordinate;
        }
    }
}