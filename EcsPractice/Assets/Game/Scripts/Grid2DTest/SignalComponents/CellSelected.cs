using Unity.Entities;
using Unity.Mathematics;

namespace Game {
    public struct CellSelected : IComponentData {
        public int2 cellCoordinate;

        public CellSelected(int2 cellCoordinate) {
            this.cellCoordinate = cellCoordinate;
        }
    }
}