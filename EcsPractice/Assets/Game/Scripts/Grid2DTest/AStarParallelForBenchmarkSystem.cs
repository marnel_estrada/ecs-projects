using System;

using Common.Utils;

using CommonEcs;

using Unity.Entities;
using Unity.Mathematics;

using UnityEngine;

namespace Game {
    [AlwaysUpdateSystem]
    [UpdateAfter(typeof(Grid2dSystem))]
    [UpdateBefore(typeof(AStarParallelForSystem))]
    [UpdateInGroup(typeof(PresentationSystemGroup))]
    public class AStarParallelForBenchmarkSystem : ComponentSystem {
        private Grid2dSystem gridSystem;

        private EntityQuery query;
        private ArchetypeChunkEntityType entityType;

        private ComponentDataFromEntity<Tile> allTiles;

        private int requestCount;

        private EntityArchetype requestArchetype;
        
        private SimpleList<Action<int>> countObservers = new SimpleList<Action<int>>(1); 

        protected override void OnCreate() {
            this.gridSystem = this.World.GetOrCreateSystem<Grid2dSystem>();
            this.requestArchetype = this.EntityManager.CreateArchetype(typeof(Signal), typeof(RequestAStarParallelFor), 
                typeof(AStarSearchParameters), typeof(AStarPath), typeof(Int2BufferElement), typeof(Waiting));
        }

        protected override void OnUpdate() {
            this.allTiles = GetComponentDataFromEntity<Tile>(true);
            
            for (int i = 0; i < this.requestCount; ++i) {
                MakeRequest(this.PostUpdateCommands);
            }

            CheckInput();
        }

        private void MakeRequest(EntityCommandBuffer commandBuffer) {
            Entity entity = commandBuffer.CreateEntity(this.requestArchetype);
            
            // Add parameters
            Grid2D grid = this.gridSystem.Grid2D;
            int2 start = GetUnblockedCoordinate(grid);
            int2 goal = GetUnblockedCoordinate(grid);
            commandBuffer.SetComponent(entity, new AStarSearchParameters(start, goal));
        }
        
        private int2 GetUnblockedCoordinate(in Grid2D grid) {
            int2 coordinate;
            Tile tile;
            do {
                coordinate = RandomRange(grid.minCoordinate, grid.maxCoordinte);
                Maybe<Entity> cellEntity = this.gridSystem.GetCellEntityAtWorld(coordinate);
                tile = this.allTiles[cellEntity.Value];
            } while (tile.isBlocked.Value);

            return coordinate;
        }

        private static int2 RandomRange(int2 min, int2 max) {
            int x = UnityEngine.Random.Range(min.x, max.x);
            int y = UnityEngine.Random.Range(min.y, max.y);
            return new int2(x, y);
        }

        private const int REQUEST_INCREMENT = 10;

        private void CheckInput() {
            if (Input.GetKeyDown(KeyCode.Space)) {
                this.requestCount += REQUEST_INCREMENT;
                NotifyObservers();
            }

            if (Input.GetKeyDown(KeyCode.Backspace)) {
                this.requestCount -= REQUEST_INCREMENT;
                
                // Clamp
                if (this.requestCount < 0) {
                    this.requestCount = 0;
                }

                NotifyObservers();
            }
        }

        public void AddCountObserver(Action<int> observer) {
            this.countObservers.Add(observer);
        }

        private void NotifyObservers() {
            for (int i = 0; i < this.countObservers.Count; ++i) {
                this.countObservers[i](this.requestCount); // Invoke action
            }
        }
    }
}