using CommonEcs;

using Unity.Entities;

namespace Game {
    [UpdateBefore(typeof(DestroySignalsSystem))]
    [UpdateInGroup(typeof(PresentationSystemGroup))]
    public class CellSelectedSystem : GridInteractionSignalHandlerSystem<CellSelected> {
        private Grid2dSystem gridSystem;
        
        protected override void OnCreateManager() {
            base.OnCreateManager();
            this.gridSystem = World.Active.GetOrCreateSystem<Grid2dSystem>();
        }

        protected override GridInteraction Process(GridInteraction interaction, CellSelected signalComponent) {
            if (!this.gridSystem.IsInside(signalComponent.cellCoordinate)) {
                // Not inside the grid. Do nothing.
                return interaction;
            }

            switch (interaction.selectedMarker) {
                case TileMarker.BLOCKER:
                    Signal.Dispatch(this.PostUpdateCommands, new PaintBlocker(signalComponent.cellCoordinate));
                    break;
                
                case TileMarker.ERASE_BLOCKER:
                    Signal.Dispatch(this.PostUpdateCommands, new EraseBlocker(signalComponent.cellCoordinate));
                    break;
                
                case TileMarker.START:
                    Signal.Dispatch(this.PostUpdateCommands, new ChangeStartCoordinate(signalComponent.cellCoordinate));
                    break;
                
                case TileMarker.DESTINATION:
                    Signal.Dispatch(this.PostUpdateCommands, new ChangeDestinationCoordinate(signalComponent.cellCoordinate));
                    break;
            }

            return interaction;
        }
    }
}