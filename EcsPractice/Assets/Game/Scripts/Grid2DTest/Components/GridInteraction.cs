using Unity.Entities;
using Unity.Mathematics;

namespace Game {
    public struct GridInteraction : IComponentData {
        public TileMarker selectedMarker;

        public int2 startCoordinate;
        public int2 destinationCoordinate;
    }
}