using Unity.Entities;

namespace Game {
    /// <summary>
    /// Just a tag component that identifies an entity to paint its path
    /// </summary>
    public struct PaintPath : IComponentData {
    }
}