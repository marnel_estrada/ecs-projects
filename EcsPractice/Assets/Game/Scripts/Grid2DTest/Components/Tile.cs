﻿namespace Game {
    using Common;

    using Unity.Entities;
    
    public struct Tile : IComponentData {
        // Whether or not the tile is blocked or not
        public ByteBool isBlocked;
    }
}