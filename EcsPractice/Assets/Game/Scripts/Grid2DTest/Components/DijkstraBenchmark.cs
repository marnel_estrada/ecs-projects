using Unity.Entities;

namespace Game {
    /// <summary>
    /// Just a tag component to identify an entity that is used for benchmarking Dijkstra's Algorithm
    /// </summary>
    public struct DijkstraBenchmark : IComponentData {
    }
}