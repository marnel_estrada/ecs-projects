using System.Diagnostics.Contracts;

using CommonEcs;

using Unity.Mathematics;

namespace Game {
    public struct SimpleHeuristicCalculator : HeuristicCostCalculator {
        [Pure]
        public float ComputeCost(int2 start, int2 goal) {
            int xDistance = math.abs(start.x - goal.x);
            int yDistance = math.abs(start.y - goal.y);
            if (xDistance > yDistance) {
                return 14 * yDistance + (10 * (xDistance - yDistance));
            }
            
            return 14 * xDistance + (10 * (yDistance - xDistance));
        }
    }
}