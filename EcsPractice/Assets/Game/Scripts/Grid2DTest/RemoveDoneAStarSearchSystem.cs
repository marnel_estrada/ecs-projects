using CommonEcs;

using Unity.Collections;
using Unity.Entities;

namespace Game {
    [UpdateAfter(typeof(AStarSearchExecutionSystem))]
    [UpdateInGroup(typeof(PresentationSystemGroup))]
    public class RemoveDoneAStarSearchSystem : ComponentSystem {
        private EntityQuery query;

        private ArchetypeChunkComponentType<Waiting> waitingType;
        private ArchetypeChunkEntityType entityType;

        protected override void OnCreateManager() {
            this.query = GetEntityQuery(typeof(AStarPath), typeof(Waiting), 
                ComponentType.Exclude<PaintPath>());
        }

        protected override void OnUpdate() {
            this.waitingType = GetArchetypeChunkComponentType<Waiting>(true);
            this.entityType = GetArchetypeChunkEntityType();

            NativeArray<ArchetypeChunk> chunks = this.query.CreateArchetypeChunkArray(Allocator.TempJob);
            for (int i = 0; i < chunks.Length; ++i) {
                Process(chunks[i]);
            }
            
            chunks.Dispose();
        }

        private void Process(in ArchetypeChunk chunk) {
            NativeArray<Waiting> waitingArray = chunk.GetNativeArray(this.waitingType);
            NativeArray<Entity> entities = chunk.GetNativeArray(this.entityType);
            for (int i = 0; i < waitingArray.Length; ++i) {
                Waiting waiting = waitingArray[i];
                if (waiting.done) {
                    // Remove the waiting. It's already done.
                    this.PostUpdateCommands.RemoveComponent<Waiting>(entities[i]);
                }
            }
        }
    }
}