using CommonEcs;

using Unity.Collections;
using Unity.Entities;

using UnityEngine;

using Sprite = CommonEcs.Sprite;

namespace Game {
    [UpdateBefore(typeof(IdentifySpriteManagerChangedSystem))]
    [UpdateBefore(typeof(DestroySignalsSystem))]
    [UpdateInGroup(typeof(PresentationSystemGroup))]
    public class ClearPathSystem : GridInteractionSignalHandlerSystem<ClearPath> {
        private EntityQuery query;
        private ArchetypeChunkComponentType<Sprite> spriteType;
        private ArchetypeChunkComponentType<Cell2D> cellType;
        private ArchetypeChunkComponentType<Tile> tileType;
        private ArchetypeChunkEntityType entityType;
        
        protected override void OnCreateManager() {
            base.OnCreateManager();
            this.query = GetEntityQuery(typeof(Sprite), typeof(Cell2D), ComponentType.Exclude<Changed>());
        }
        
        protected override void OnUpdate() {
            this.spriteType = GetArchetypeChunkComponentType<Sprite>();
            this.cellType = GetArchetypeChunkComponentType<Cell2D>(true);
            this.tileType = GetArchetypeChunkComponentType<Tile>(true);
            this.entityType = GetArchetypeChunkEntityType();
            
            base.OnUpdate();
        }

        protected override GridInteraction Process(GridInteraction interaction, ClearPath signalComponent) {
            NativeArray<ArchetypeChunk> chunks = this.query.CreateArchetypeChunkArray(Allocator.TempJob);
            for (int i = 0; i < chunks.Length; ++i) {
                Process(chunks[i], interaction);
            }
            
            chunks.Dispose();
            
            return interaction;
        }

        private void Process(in ArchetypeChunk chunk, in GridInteraction interaction) {
            NativeArray<Sprite> sprites = chunk.GetNativeArray(this.spriteType);
            NativeArray<Cell2D> cells = chunk.GetNativeArray(this.cellType);
            NativeArray<Tile> tiles = chunk.GetNativeArray(this.tileType);
            NativeArray<Entity> entities = chunk.GetNativeArray(this.entityType);

            for (int i = 0; i < chunk.Count; ++i) {
                Tile tile = tiles[i];
                if (tile.isBlocked) {
                    // Do not clear blocked path
                    continue;
                }
                
                Cell2D cell = cells[i];
                if (cell.worldCoordinate.Equals(interaction.startCoordinate)) {
                    // Do not erase start cell
                    continue;
                }

                if (cell.worldCoordinate.Equals(interaction.destinationCoordinate)) {
                    // Do not erase destination cell
                    continue;
                }
                
                // Clear the color
                Sprite sprite = sprites[i];
                sprite.SetColor(Color.white);
                sprites[i] = sprite; // Modify
                
                this.PostUpdateCommands.AddComponent(entities[i], new Changed());
            }
        }
    }
}