using Common;

using CommonEcs;

using Unity.Entities;
using Unity.Mathematics;

using UnityEngine;

namespace Game {
    [AlwaysUpdateSystem]
    [UpdateBefore(typeof(DestroySignalsSystem))]
    [UpdateBefore(typeof(CellSelectedSystem))]
    [UpdateInGroup(typeof(PresentationSystemGroup))]
    public class Grid2dInputSystem : ComponentSystem {
        private bool resolvedObjects;
        private Grid grid;
        private Camera worldCamera;

        protected override void OnUpdate() {
            if (!this.resolvedObjects) {
                ResolveObjects();
            }

            if (this.grid == null) {
                // It's probably another scene or game
                return;
            }
            
            if (Input.GetMouseButtonDown(0)) {
                Vector3 mouseWorld = this.worldCamera.ScreenToWorldPoint(Input.mousePosition);
                mouseWorld.z = 0;
                Vector3Int selectedCell = this.grid.WorldToCell(mouseWorld);
                Signal.Dispatch(this.PostUpdateCommands, new CellSelected(new int2(selectedCell.x, selectedCell.y)));
            }
        }

        private void ResolveObjects() {
            this.grid = UnityUtils.GetComponent<Grid>("Grid");
            this.worldCamera = UnityUtils.GetComponent<Camera>("WorldCamera");
            
            this.resolvedObjects = true;
        }
    }
}