using CommonEcs;

using Unity.Collections;
using Unity.Entities;
using Unity.Mathematics;

using UnityEngine;

using Sprite = CommonEcs.Sprite;

namespace Game {
    [UpdateAfter(typeof(AStarSearchExecutionSystem))]
    [UpdateBefore(typeof(IdentifySpriteManagerChangedSystem))]
    [UpdateInGroup(typeof(PresentationSystemGroup))]
    public class PaintPathSystem : ComponentSystem {
        private EntityQuery query;

        private ArchetypeChunkComponentType<AStarPath> pathType;
        private ArchetypeChunkComponentType<Waiting> waitingType;
        private ArchetypeChunkBufferType<Int2BufferElement> waypointsType;
        private ArchetypeChunkEntityType entityType;

        private Grid2dSystem gridSystem;
        private ComponentDataFromEntity<Sprite> allSprites; 

        protected override void OnCreateManager() {
            this.query = GetEntityQuery(typeof(AStarPath), typeof(Waiting), typeof(PaintPath), ComponentType.Create<Int2BufferElement>());
            this.gridSystem = World.Active.GetOrCreateSystem<Grid2dSystem>();
        }

        protected override void OnUpdate() {
            this.pathType = GetArchetypeChunkComponentType<AStarPath>(true);
            this.waitingType = GetArchetypeChunkComponentType<Waiting>(true);
            this.waypointsType = GetArchetypeChunkBufferType<Int2BufferElement>(true);
            this.entityType = GetArchetypeChunkEntityType();
            this.allSprites = GetComponentDataFromEntity<Sprite>();

            NativeArray<ArchetypeChunk> chunks = this.query.CreateArchetypeChunkArray(Allocator.TempJob);
            for (int i = 0; i < chunks.Length; ++i) {
                Process(chunks[i]);
            }
            
            chunks.Dispose();
        }

        private void Process(in ArchetypeChunk chunk) {
            NativeArray<Entity> entities = chunk.GetNativeArray(this.entityType);
            NativeArray<Waiting> waitingArray = chunk.GetNativeArray(this.waitingType);
            NativeArray<AStarPath> paths = chunk.GetNativeArray(this.pathType);
            BufferAccessor<Int2BufferElement> waypointsAccessor = chunk.GetBufferAccessor(this.waypointsType);
            
            for (int i = 0; i < chunk.Count; ++i) {
                Waiting waiting = waitingArray[i];
                if (waiting.done) {
                    // Waiting is done. Time to process the result.
                    Process(paths[i], waypointsAccessor[i]);
                    this.PostUpdateCommands.RemoveComponent<Waiting>(entities[i]);
                }
            }
        }

        private void Process(in AStarPath path, in DynamicBuffer<Int2BufferElement> waypoints) {
            // Note here that we don't include the first since it's already the destination position
            // However if the path was unreachable, we include the first path as it is not the
            // destination.
            int startIndex = path.reachable ? 1 : 0;
            for (int i = startIndex; i < waypoints.Length; ++i) {
                PaintCell(waypoints[i].value);
            }
        }

        private void PaintCell(int2 coordinate) {
            Maybe<Entity> result = this.gridSystem.GetCellEntityAtWorld(coordinate);
            Entity cellEntity = result.Value;
            Sprite sprite = this.allSprites[cellEntity];
            sprite.SetColor(Color.green);
            this.allSprites[cellEntity] = sprite; // Modify
            
            // We add this so that changes to the sprite will be identified
            this.PostUpdateCommands.AddComponent(cellEntity, new Changed());
        }
    }
}