using System;
using System.IO;

using Common.Xml;

using CommonEcs;

using Unity.Entities;
using Unity.Mathematics;

using UnityEngine;

namespace Game {
    [UpdateInGroup(typeof(PresentationSystemGroup))]
    public class LoadGridSystem : SignalHandlerComponentSystem<LoadGrid> {
        private Grid2dSystem gridSystem;
        private ComponentDataFromEntity<Tile> allTiles;

        protected override void OnCreateManager() {
            base.OnCreateManager();
            this.gridSystem = World.Active.GetOrCreateSystem<Grid2dSystem>();
        }

        protected override void OnDispatch(Entity entity, LoadGrid component) {
            string path = Path.Combine(Application.streamingAssetsPath, "Grid.xml");
            Grid2D grid = this.gridSystem.Grid2D;

            this.allTiles = GetComponentDataFromEntity<Tile>(true);
            
            SimpleXmlReader reader = new SimpleXmlReader();
            SimpleXmlNode root = reader.Read(File.ReadAllText(path)).FindFirstNodeInChildren("Grid");
            
            // Verify columnCount and rowCount
            Assertion.Assert(grid.columnCount == root.GetAttributeAsInt("columns"));
            Assertion.Assert(grid.rowCount == root.GetAttributeAsInt("rows"));
            
            // Load blocker data
            byte[] blockerData = Convert.FromBase64String(root.GetAttribute("blockerData"));
            for (int i = 0; i < blockerData.Length; ++i) {
                if (blockerData[i] == 0) {
                    // No blocker at this index
                    continue;
                }
                
                int x = grid.GetX(i);
                int y = grid.GetY(i);
                int2 worldCoordinate = grid.ToWorldCoordinate(new int2(x, y));
                Signal.Dispatch(this.PostUpdateCommands, new PaintBlocker(worldCoordinate));
            }
        }
    }
}
