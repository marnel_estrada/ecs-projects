using Unity.Entities;

namespace Game {
    [UpdateInGroup(typeof(PresentationSystemGroup))]
    public class ChangeSelectedMarkerSystem : GridInteractionSignalHandlerSystem<ChangeSelectedMarker> {
        protected override GridInteraction Process(GridInteraction interaction, ChangeSelectedMarker signalComponent) {
            interaction.selectedMarker = signalComponent.newSelectedMarker;
            return interaction;
        }
    }
}