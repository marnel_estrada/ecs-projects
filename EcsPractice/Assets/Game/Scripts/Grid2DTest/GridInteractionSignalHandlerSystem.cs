using CommonEcs;

using Unity.Collections;
using Unity.Entities;

namespace Game {
    [UpdateInGroup(typeof(PresentationSystemGroup))]
    public abstract class GridInteractionSignalHandlerSystem<T> : SignalHandlerComponentSystem<T> where T : struct, IComponentData {
        private EntityQuery query;
        private ArchetypeChunkComponentType<GridInteraction> interactionType;

        protected override void OnCreateManager() {
            base.OnCreateManager();
            this.query = GetEntityQuery(typeof(GridInteraction));
        }

        protected override void OnDispatch(Entity entity, T component) {
            this.interactionType = GetArchetypeChunkComponentType<GridInteraction>();
            NativeArray<ArchetypeChunk> chunks = this.query.CreateArchetypeChunkArray(Allocator.TempJob);

            for (int i = 0; i < chunks.Length; ++i) {
                ArchetypeChunk chunk = chunks[i];
                ProcessChunk(chunk, component);
            }
            
            chunks.Dispose();
        }
        
        private void ProcessChunk(in ArchetypeChunk chunk, T signalComponent) {
            NativeArray<GridInteraction> array = chunk.GetNativeArray(this.interactionType);
            for (int i = 0; i < array.Length; ++i) {
                GridInteraction interaction = array[i];
                array[i] = Process(interaction, signalComponent); // Modify the data
            }
        }

        protected abstract GridInteraction Process(GridInteraction interaction, T signalComponent);
    }
}