﻿using System.Collections;

using Common.Utils;

using CommonEcs;

using Unity.Entities;
using Unity.Mathematics;
using Unity.Transforms;

using UnityEngine;
using UnityEngine.UI;

using Sprite = CommonEcs.Sprite;

namespace Game {
    public class Grid2dTestBootstrap : MonoBehaviour {
        [SerializeField]
        private Vector2Int gridDimension = new Vector2Int(2, 2);

        [SerializeField]
        private Grid grid;

        [SerializeField]
        private Material gridMaterial;

        [SerializeField]
        private Toggle initiallySelectedToggle;

        [SerializeField]
        private Text benchmarkCountLabel;

        [SerializeField]
        private bool loadGrid;
        
        [SerializeField]
        private int aStarCount = 1;

        [SerializeField]
        private int dijkstraCount;

        private EntityManager entityManager;

        private Entity spriteLayerEntity;

        private Entity interactionEntity;

        // Only one for now
        private Entity agentEntity;
        
        private readonly SimpleList<Entity> benchmarkEntities = new SimpleList<Entity>(10); 

        private void Start() {
            Assertion.AssertNotNull(this.grid);
            Assertion.AssertNotNull(this.gridMaterial);

            // Should have even dimensions
            Assertion.Assert(IsEven(this.gridDimension.x) && IsEven(this.gridDimension.y));

            this.entityManager = World.Active.EntityManager;

            this.spriteLayerEntity = CreateSpriteLayer();

            // Create an entity with Grid2DCreator
            Entity gridEntity = this.entityManager.CreateEntity();
            Vector3 cellSize = this.grid.cellSize;
            Grid2D grid = new Grid2D(this.gridDimension.x, this.gridDimension.y, cellSize.x, cellSize.y);
            this.entityManager.AddComponentData(gridEntity, grid);

            PrepareCells(gridEntity);
            PrepareInteraction(grid);
            PrepareAgents();

            if (this.loadGrid) {
                StartCoroutine(LoadGrid());
            }

            // Auto select the starting selected toggle
            Assertion.AssertNotNull(this.initiallySelectedToggle);
            this.initiallySelectedToggle.isOn = true;
        }

        private void PrepareCells(Entity gridEntity) {
            // Note here that we collect them on a temporary array first because we can't add on a DynamicBuffer
            // while creating new entities at the same time
            int halfColumns = this.gridDimension.x >> 1;
            int halfRows = this.gridDimension.y >> 1;
            Entity[] cellEntities = new Entity[this.gridDimension.x * this.gridDimension.y];
            int2 minCoordinate = new int2(-halfColumns, -halfRows);

            for (int y = 0; y < this.gridDimension.y; ++y) {
                for (int x = 0; x < this.gridDimension.x; ++x) {
                    int index = (y * this.gridDimension.x) + x;
                    int2 gridCoordinate = new int2(x, y);
                    int2 worldCoordinate = minCoordinate + gridCoordinate;
                    cellEntities[index] = CreateCellEntity(gridEntity, gridCoordinate, worldCoordinate, index);
                }
            }

            DynamicBuffer<EntityBufferElement> cellEntitiesBuffer =
                this.entityManager.AddBuffer<EntityBufferElement>(gridEntity);
            for (int i = 0; i < cellEntities.Length; ++i) {
                cellEntitiesBuffer.Add(new EntityBufferElement(cellEntities[i]));
            }
        }
 
        private Entity CreateSpriteLayer() {
            Entity layerEntity = this.entityManager.CreateEntity();
            SpriteLayer spriteLayer = new SpriteLayer(layerEntity, this.gridMaterial, 100);
            spriteLayer.alwaysUpdateMesh = false;
            spriteLayer.useMeshRenderer = false;
            spriteLayer.layer = this.gameObject.layer;
            this.entityManager.AddSharedComponentData(layerEntity, spriteLayer);

            return layerEntity;
        }

        // Creates a cell at the specified coordinate
        private Entity CreateCellEntity(Entity gridEntity, int2 gridCoordinate, int2 worldCoordinate, int index) {
            Entity cellEntity = this.entityManager.CreateEntity();

            Vector3 cellCenter = this.grid.GetCellCenterWorld(new Vector3Int(worldCoordinate.x, worldCoordinate.y, 0));
            Cell2D cell = new Cell2D(gridEntity, gridCoordinate, worldCoordinate, index, cellCenter);
            this.entityManager.AddComponentData(cellEntity, cell);

            // Add the tile
            this.entityManager.AddComponentData(cellEntity, new Tile());
            
            // Prepare the sprite
            Sprite sprite = new Sprite();
            Vector3 cellSize = this.grid.cellSize;
            sprite.Init(Entity.Null, cellSize.x, cellSize.y, new float2(0.5f, 0.5f));
            sprite.SetUv(new float2(0, 0), new float2(1, 1));
            this.entityManager.AddComponentData(cellEntity, sprite);
            
            AddToSpriteLayer addToSpriteLayer = new AddToSpriteLayer(this.spriteLayerEntity);
            this.entityManager.AddComponentData(cellEntity, addToSpriteLayer);
            
            this.entityManager.AddComponentData(cellEntity, new UseYAsSortOrder());
            
            this.entityManager.AddComponentData(cellEntity, new Translation() {
                Value = cellCenter
            });
            
            this.entityManager.AddComponentData(cellEntity, new LocalToWorld());
            
            this.entityManager.AddComponentData(cellEntity, new Static());

            return cellEntity;
        }

        private void PrepareInteraction(Grid2D grid) {
            GridInteraction interaction = new GridInteraction();
            interaction.selectedMarker = TileMarker.BLOCKER;
            interaction.startCoordinate = new int2(int.MaxValue, int.MaxValue);
            interaction.destinationCoordinate = new int2(int.MinValue, int.MinValue);
            
            this.interactionEntity = this.entityManager.CreateEntity();
            this.entityManager.AddComponentData(this.interactionEntity, interaction);
        }

        private IEnumerator LoadGrid() {
            // We set one frame later to give time for systems to prepare
            yield return null;
            
            Signal.Dispatch(this.entityManager, new LoadGrid());
        }

        private static bool IsEven(int i) {
            return (i & 1) == 0;
        }

        private void PrepareAgents() {
            this.agentEntity = this.entityManager.CreateEntity();
            this.entityManager.AddComponentData(this.agentEntity, new PaintPath());
            PrepareAgent(this.agentEntity);

            AddBenchmarkAgents<AStarBenchmark>(this.aStarCount);
            AddBenchmarkAgents<DijkstraBenchmark>(this.dijkstraCount);
        }

        private void AddBenchmarkAgents<T>(int count) where T : struct, IComponentData {
            for (int i = 0; i < count; ++i) {
                AddBenchmarkAgent<T>();
            }
        }

        private void AddBenchmarkAgent<T>() where T : struct, IComponentData {
            Entity benchmarkEntity = this.entityManager.CreateEntity();
            this.entityManager.AddComponentData(benchmarkEntity, new T());
            PrepareAgent(benchmarkEntity);
            this.benchmarkEntities.Add(benchmarkEntity);
            UpdateBenchmarkCountLabel();
        }

        // Used as Button handler
        public void AddBenchmarkTarget() {
            AddBenchmarkAgent<AStarBenchmark>();
        }

        private void UpdateBenchmarkCountLabel() {
            this.benchmarkCountLabel.text = this.benchmarkEntities.Count.ToString();
        }

        // We set as public as it is also used as button handler
        public void RemoveBenchmarkAgent() {
            if (this.benchmarkEntities.Count == 0) {
                // Nothing to remove
                return;
            }

            int lastIndex = this.benchmarkEntities.Count - 1;
            this.entityManager.DestroyEntity(this.benchmarkEntities[lastIndex]);
            this.benchmarkEntities.RemoveAt(lastIndex);
            UpdateBenchmarkCountLabel();
        }

        private void PrepareAgent(Entity owner) {
            this.entityManager.AddComponentData(owner, new AStarPath());
            this.entityManager.AddBuffer<Int2BufferElement>(owner);
        }

        // Used as button handler
        public void RequestAStar() {
            // Clear the previous path first
            Signal.Dispatch(this.entityManager, new ClearPath());
            
            // We dispatch the request one frame later so we won't have two Changed components when
            // painting cells
            StartCoroutine(DispatchAStarRequest());
        }

        private IEnumerator DispatchAStarRequest() {
            yield return null;
            
            this.entityManager.AddComponentData(this.agentEntity, new Waiting());
            
            GridInteraction interaction = this.entityManager.GetComponentData<GridInteraction>(this.interactionEntity);
            Signal.Dispatch(this.entityManager, new RequestAStarSearch(this.agentEntity, interaction.startCoordinate, interaction.destinationCoordinate));
        }

        // Used as button handler
        public void SaveGrid() {
            Signal.Dispatch(this.entityManager, new SaveGrid());
        }

        public void RequestDijkstra() {
            // Clear the previous path first
            Signal.Dispatch(this.entityManager, new ClearPath());
            
            // We dispatch the request one frame later so we won't have two Changed components when
            // painting cells
            StartCoroutine(DispatchDijkstraRequest());
        }

        private IEnumerator DispatchDijkstraRequest() {
            yield return null;
            
            this.entityManager.AddComponentData(this.agentEntity, new Waiting());
            GridInteraction interaction = this.entityManager.GetComponentData<GridInteraction>(this.interactionEntity);
            Signal.Dispatch(this.entityManager, new RequestDijkstraSearch(this.agentEntity, interaction.startCoordinate, interaction.destinationCoordinate));
        }
    }
}