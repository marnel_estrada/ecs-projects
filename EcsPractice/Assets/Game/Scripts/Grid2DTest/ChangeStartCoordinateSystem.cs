using CommonEcs;

using Unity.Entities;
using Unity.Mathematics;

namespace Game {
    [UpdateAfter(typeof(CollectedCommandsSystem))]
    [UpdateBefore(typeof(DestroySignalsSystem))]
    [UpdateAfter(typeof(CellSelectedSystem))]
    [UpdateInGroup(typeof(PresentationSystemGroup))]
    public class ChangeStartCoordinateSystem : GridInteractionSignalHandlerSystem<ChangeStartCoordinate> {
        protected override GridInteraction Process(GridInteraction interaction, ChangeStartCoordinate signalComponent) {
            if (interaction.destinationCoordinate.Equals(signalComponent.newCoordinate)) {
                // Can't be the same as destination coordinate
                return interaction;
            }

            if (interaction.startCoordinate.Equals(signalComponent.newCoordinate)) {
                // New coordinate is the same with the current one. No need to change.
                return interaction;
            }
            
            int2 previousValue = interaction.startCoordinate;
            interaction.startCoordinate = signalComponent.newCoordinate;
            
            // Dispatch this to change the tile color
            ChangeStartTile changeStartTile = new ChangeStartTile() {
                previousCoordinate = previousValue,
                newCoordinate = signalComponent.newCoordinate
            };
            Signal.Dispatch(this.PostUpdateCommands, changeStartTile);

            return interaction;
        }
    }
}