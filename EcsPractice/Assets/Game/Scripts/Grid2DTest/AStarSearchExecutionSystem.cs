using System.Runtime.InteropServices;

using CommonEcs;

using Unity.Collections;
using Unity.Entities;
using Unity.Jobs;
using Unity.Mathematics;

namespace Game {
    [UpdateInGroup(typeof(PresentationSystemGroup))]
    public class AStarSearchExecutionSystem : JobComponentSystem {
        private EntityQuery query;
        private ArchetypeChunkComponentType<RequestAStarSearch> requestType;

        private ComponentDataFromEntity<AStarPath> allPaths;
        private BufferFromEntity<Int2BufferElement> allPathLists;
        
        private ComponentDataFromEntity<Tile> allTiles;
        private ComponentDataFromEntity<Waiting> allWaiting;

        private Grid2dSystem gridSystem;

        private NativeArray<int2> neighborOffsets;
        
        private readonly Pool<AStarSearchContainers> containersPool = new Pool<AStarSearchContainers>(delegate {
            return new AStarSearchContainers(new NativeList<AStarNode>(10, Allocator.Persistent),
                new NativeList<HeapNode>(10, Allocator.Persistent), 
                new NativeHashMap<int2, AStarNode>(10, Allocator.Persistent),
                new NativeHashMap<int2, byte>(10, Allocator.Persistent));
        });

        protected override void OnCreateManager() {
            this.query = GetEntityQuery(typeof(Signal), typeof(RequestAStarSearch));
            this.gridSystem = World.Active.GetOrCreateSystem<Grid2dSystem>();
            
            // Prepare neighbor offsets
            this.neighborOffsets = new NativeArray<int2>(8, Allocator.Persistent);
            this.neighborOffsets[0] = new int2(0, 1); // Top
            this.neighborOffsets[1] = new int2(0, -1); // Bottom
            this.neighborOffsets[2] = new int2(1, 0); // Right
            this.neighborOffsets[3] = new int2(-1, 0); // Left
            this.neighborOffsets[4] = new int2(1, 1); // Top-Right
            this.neighborOffsets[5] = new int2(-1, 1); // Top-Left
            this.neighborOffsets[6] = new int2(1, -1); // Bottom-Right
            this.neighborOffsets[7] = new int2(-1, -1); // Bottom-Left
        }

        protected override void OnDestroyManager() {
            this.neighborOffsets.Dispose();
            this.containersPool.Dispose();
        }

        protected override JobHandle OnUpdate(JobHandle inputDeps) {
            this.requestType = GetArchetypeChunkComponentType<RequestAStarSearch>(true);
            this.allPaths = GetComponentDataFromEntity<AStarPath>();
            this.allPathLists = GetBufferFromEntity<Int2BufferElement>();
            this.allTiles = GetComponentDataFromEntity<Tile>(true);
            this.allWaiting = GetComponentDataFromEntity<Waiting>();
            NativeArray<ArchetypeChunk> chunks = this.query.CreateArchetypeChunkArray(Allocator.TempJob);
            
            SimpleReachability reachability = new SimpleReachability() {
                gridWrapper = this.gridSystem.GridWrapper,
                allTiles = this.allTiles
            };

            JobHandle handle = inputDeps;
            for (int i = 0; i < chunks.Length; ++i) {
                handle = Process(chunks[i], reachability, handle);
            }
            
            chunks.Dispose();
            
            return handle;
        }

        private JobHandle Process(in ArchetypeChunk chunk, in SimpleReachability reachability, JobHandle inputDeps) {
            JobHandle handle = inputDeps;
            NativeArray<RequestAStarSearch> requests = chunk.GetNativeArray(this.requestType);
            for (int i = 0; i < requests.Length; ++i) {
                RequestAStarSearch request = requests[i];
                
                PoolEntry<AStarSearchContainers> containers = this.containersPool.Request();
                
                // Prepare the OpenSet
                GrowingHeap heap = new GrowingHeap(containers.instance.heapNodes);
                OpenSet openSet = new OpenSet(heap, containers.instance.nodeMap);

                AStarSearch<SimpleHeuristicCalculator, SimpleReachability> searchJob = new AStarSearch<SimpleHeuristicCalculator, SimpleReachability>() {
                    owner = request.owner,
                    startPosition = request.start,
                    goalPosition = request.goal,
                    reachability = reachability,
                    neighborOffsets = this.neighborOffsets,
                    gridWrapper = this.gridSystem.GridWrapper,
                    allPaths = this.allPaths,
                    allPathLists = this.allPathLists,
                    allWaiting = this.allWaiting,
                    allNodes = containers.instance.allNodes,
                    openSet = openSet,
                    closeSet = containers.instance.closeSet
                };

                handle = searchJob.Schedule(handle);
                
                RecycleJob recycle = new RecycleJob() {
                    handle = GCHandle.Alloc(this.containersPool),
                    entry = containers
                };
                handle = recycle.Schedule(handle);
            }

            return handle;
        }

        private struct RecycleJob : IJob {
            public GCHandle handle;
            public PoolEntry<AStarSearchContainers> entry;
            
            public void Execute() {
                Pool<AStarSearchContainers> pool = (Pool<AStarSearchContainers>) this.handle.Target;
                pool.Recycle(this.entry);
                this.handle.Free();
            }
        }
    }
}