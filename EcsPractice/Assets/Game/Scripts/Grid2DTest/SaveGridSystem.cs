using System;
using System.Collections.Generic;
using System.IO;
using System.Xml;

using CommonEcs;

using Unity.Entities;

using UnityEngine;

namespace Game {
    [UpdateInGroup(typeof(PresentationSystemGroup))]
    public class SaveGridSystem : SignalHandlerComponentSystem<SaveGrid> {
        private Grid2dSystem gridSystem;
        private readonly XmlWriterSettings xmlSettings = new XmlWriterSettings();
        private readonly List<byte> blockerData = new List<byte>();

        private ComponentDataFromEntity<Tile> allTiles;
        
        protected override void OnCreateManager() {
            base.OnCreateManager();
            this.gridSystem = World.Active.GetOrCreateSystem<Grid2dSystem>();
            this.xmlSettings.Indent = true;
        }

        protected override void OnDispatch(Entity entity, SaveGrid component) {
            string path = Path.Combine(Application.streamingAssetsPath, "Grid.xml");
            Grid2D grid = this.gridSystem.Grid2D;

            this.allTiles = GetComponentDataFromEntity<Tile>(true);

            using (XmlWriter writer = XmlWriter.Create(path, this.xmlSettings)) {
                writer.WriteStartDocument();
                writer.WriteStartElement("Grid");
                writer.WriteAttributeString("columns", grid.columnCount.ToString());
                writer.WriteAttributeString("rows", grid.rowCount.ToString());
                
                // Store blocker data
                StoreBlockerData(grid);
                
                // Write blocker data as base 64
                string base64 = Convert.ToBase64String(this.blockerData.ToArray());
                writer.WriteAttributeString("blockerData", base64);
                
                writer.WriteEndElement(); // Grid
                writer.WriteEndDocument();
                writer.Flush();
            }
            
            Debug.Log($"Saved Grid at {path}");
        }

        private void StoreBlockerData(in Grid2D grid) {
            this.blockerData.Clear();
            
            for (int y = 0; y < grid.rowCount; ++y) {
                for (int x = 0; x < grid.columnCount; ++x) {
                    Maybe<Entity> result = this.gridSystem.GetCellEntity(x, y);
                    Assertion.Assert(result.HasValue);
                    Tile tile = this.allTiles[result.Value];
                    this.blockerData.Add(tile.isBlocked.Value ? (byte)1 : (byte)0);
                }
            }
        }
    }
}