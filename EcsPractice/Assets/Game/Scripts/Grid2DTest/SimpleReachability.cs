using CommonEcs;

using Unity.Collections;
using Unity.Entities;
using Unity.Mathematics;

namespace Game {
    public struct SimpleReachability : Reachability {
        public GridWrapper gridWrapper;
        
        [ReadOnly]
        public ComponentDataFromEntity<Tile> allTiles;

        public bool IsReachable(int2 goal) {
            Maybe<Entity> cellEntity = this.gridWrapper.GetCellEntityAtWorld(goal);
            if (cellEntity.HasValue) {
                Tile tile = this.allTiles[cellEntity.Value];
                return !tile.isBlocked;
            }

            return false;
        }

        public bool IsReachable(int2 from, int2 to) {
            return IsReachable(to);
        }

        public float GetWeight(int2 from, int2 to) {
            int xDifference = math.abs(to.x - from.x);
            int yDifference = math.abs(to.y - from.y);
            
            // Diagonal movement should be costlier than horizontal or vertical movement
            return xDifference > 0 && yDifference > 0 ? 14 : 10;
        }
    }
}