using CommonEcs;

using Unity.Entities;

using UnityEngine;

using Sprite = CommonEcs.Sprite;

namespace Game {
    [UpdateAfter(typeof(CellSelectedSystem))]
    [UpdateBefore(typeof(DestroySignalsSystem))]
    [UpdateInGroup(typeof(PresentationSystemGroup))]
    public class PaintBlockerSystem : GridInteractionSignalHandlerSystem<PaintBlocker> {
        private Grid2dSystem gridSystem;
        private ComponentDataFromEntity<Tile> allTiles;
        private ComponentDataFromEntity<Sprite> allSprites;

        protected override void OnCreateManager() {
            base.OnCreateManager();
            this.gridSystem = World.Active.GetOrCreateSystem<Grid2dSystem>();
        }

        protected override GridInteraction Process(GridInteraction interaction, PaintBlocker signalComponent) {
            if (interaction.startCoordinate.Equals(signalComponent.coordinate)) {
                // Can't be the same as start coordinate
                return interaction;
            }
            
            if (interaction.destinationCoordinate.Equals(signalComponent.coordinate)) {
                // Can't be the same as destination coordinate
                return interaction;
            }

            this.allTiles = GetComponentDataFromEntity<Tile>();
            Maybe<Entity> result = this.gridSystem.GetCellEntityAtWorld(signalComponent.coordinate);
            Assertion.Assert(result.HasValue); // Should have a value

            Entity cellEntity = result.Value;
            Tile tile = this.allTiles[cellEntity];
            tile.isBlocked = true;
            this.allTiles[cellEntity] = tile; // Modify

            this.allSprites = GetComponentDataFromEntity<Sprite>();
            Sprite sprite = this.allSprites[cellEntity];
            sprite.SetColor(Color.black);
            this.allSprites[cellEntity] = sprite; // Modify
            
            // Add this component to actually change the sprite
            this.PostUpdateCommands.AddComponent(cellEntity, new Changed());

            return interaction;
        }
    }
}