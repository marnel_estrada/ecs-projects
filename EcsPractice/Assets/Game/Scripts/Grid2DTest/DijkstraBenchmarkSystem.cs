using CommonEcs;

using Unity.Collections;
using Unity.Entities;
using Unity.Mathematics;

namespace Game {
    [AlwaysUpdateSystem]
    [UpdateBefore(typeof(DijkstraSearchExecutionSystem))]
    [UpdateAfter(typeof(Grid2dSystem))]
    [UpdateInGroup(typeof(PresentationSystemGroup))]
    public class DijkstraBenchmarkSystem : ComponentSystem {
        private Grid2dSystem gridSystem;

        private EntityQuery query;
        private ArchetypeChunkEntityType entityType;

        private ComponentDataFromEntity<Tile> allTiles;

        protected override void OnCreateManager() {
            this.gridSystem = this.World.GetOrCreateSystem<Grid2dSystem>();
            this.query = GetEntityQuery( typeof(DijkstraBenchmark), typeof(AStarPath), 
                ComponentType.Exclude<Waiting>(), ComponentType.Exclude<PaintPath>());
        }

        protected override void OnUpdate() {
            this.entityType = GetArchetypeChunkEntityType();
            this.allTiles = GetComponentDataFromEntity<Tile>();
            
            NativeArray<ArchetypeChunk> chunks = this.query.CreateArchetypeChunkArray(Allocator.TempJob);
            for (int i = 0; i < chunks.Length; ++i) {
                Process(chunks[i]);
            }
            
            chunks.Dispose();
        }
        
        private void Process(in ArchetypeChunk chunk) {
            NativeArray<Entity> entities = chunk.GetNativeArray(this.entityType);
            for (int i = 0; i < entities.Length; ++i) {
                MakeRequest(entities[i]);
            }
        }

        private void MakeRequest(Entity agent) {
            // Find random start and random goal
            Grid2D grid = this.gridSystem.Grid2D;
            int2 start = GetUnblockedCoordinate(grid);
            int2 goal = GetUnblockedCoordinate(grid);
            
            Signal.Dispatch(this.PostUpdateCommands, new RequestDijkstraSearch(agent, start, goal));
            this.PostUpdateCommands.AddComponent(agent, new Waiting());
        }

        private int2 GetUnblockedCoordinate(in Grid2D grid) {
            int2 coordinate;
            Tile tile;
            do {
                coordinate = RandomRange(grid.minCoordinate, grid.maxCoordinte);
                Maybe<Entity> cellEntity = this.gridSystem.GetCellEntityAtWorld(coordinate);
                tile = this.allTiles[cellEntity.Value];
            } while (tile.isBlocked.Value);

            return coordinate;
        }

        private static int2 RandomRange(int2 min, int2 max) {
            int x = UnityEngine.Random.Range(min.x, max.x);
            int y = UnityEngine.Random.Range(min.y, max.y);
            return new int2(x, y);
        }
    }
}