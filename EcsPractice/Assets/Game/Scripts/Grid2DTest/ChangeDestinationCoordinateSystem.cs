using CommonEcs;

using Unity.Entities;
using Unity.Mathematics;

namespace Game {
    [UpdateAfter(typeof(CollectedCommandsSystem))]
    [UpdateBefore(typeof(DestroySignalsSystem))]
    [UpdateAfter(typeof(CellSelectedSystem))]
    [UpdateInGroup(typeof(PresentationSystemGroup))]
    public class ChangeDestinationCoordinateSystem : GridInteractionSignalHandlerSystem<ChangeDestinationCoordinate> {
        protected override GridInteraction Process(GridInteraction interaction, ChangeDestinationCoordinate signalComponent) {
            if (interaction.startCoordinate.Equals(signalComponent.newCoordinate)) {
                // Can't be the same as start coordinate
                return interaction;
            }
            
            if (interaction.destinationCoordinate.Equals(signalComponent.newCoordinate)) {
                // New coordinate is the same with the current one. No need to change.
                return interaction;
            }
            
            int2 previousValue = interaction.destinationCoordinate;
            interaction.destinationCoordinate = signalComponent.newCoordinate;
            
            // Dispatch this to change the tile color
            ChangeDestinationTile changeDestinationTile = new ChangeDestinationTile {
                previousCoordinate = previousValue,
                newCoordinate = signalComponent.newCoordinate
            };
            Signal.Dispatch(this.PostUpdateCommands, changeDestinationTile);

            return interaction;
        }
    }
}