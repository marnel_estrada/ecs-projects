using CommonEcs;

using Unity.Entities;
using Unity.Mathematics;

using UnityEngine;

using Sprite = CommonEcs.Sprite;

namespace Game {
    [UpdateBefore(typeof(IdentifySpriteManagerChangedSystem))]
    [UpdateBefore(typeof(DestroySignalsSystem))]
    [UpdateAfter(typeof(ChangeStartCoordinateSystem))]
    [UpdateInGroup(typeof(PresentationSystemGroup))]
    public class ChangeStartTileSystem : SignalHandlerComponentSystem<ChangeStartTile> {
        private ArchetypeChunkComponentType<Grid2D> gridType;

        private Grid2dSystem gridSystem;
        private ComponentDataFromEntity<Sprite> allSprites;
        
        protected override void OnCreateManager() {
            base.OnCreateManager();
            this.gridSystem = World.Active.GetOrCreateSystem<Grid2dSystem>();
        }

        protected override void OnDispatch(Entity entity, ChangeStartTile component) {
            this.allSprites = GetComponentDataFromEntity<Sprite>();
            SetColor(component.previousCoordinate, Color.white);
            SetColor(component.newCoordinate, Color.blue);
        }

        private void SetColor(int2 cellPosition, Color color) {
            Maybe<Entity> result = this.gridSystem.GetCellEntityAtWorld(cellPosition);
            if (!result.HasValue) {
                // Cell was not resolved
                return;
            }

            Entity cellEntity = result.Value;
            Sprite sprite = this.allSprites[cellEntity];
            sprite.SetColor(color);
            this.allSprites[cellEntity] = sprite; // Modify the value
            
            // We add this component so it will be filtered by IdentifyChanged* system
            this.PostUpdateCommands.AddComponent(cellEntity, new Changed());
        }
    }
}