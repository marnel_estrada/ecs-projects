using CommonEcs;

using Unity.Collections;
using Unity.Entities;
using Unity.Jobs;
using Unity.Mathematics;

namespace Game {
    [UpdateAfter(typeof(Grid2dSystem))]
    [UpdateInGroup(typeof(PresentationSystemGroup))]
    public class AStarParallelForSystem : JobComponentSystem {
        private EntityQuery query;

        private ArchetypeChunkEntityType entityType;

        private ComponentDataFromEntity<Tile> allTiles;

        private ComponentDataFromEntity<AStarSearchParameters> allParameters;
        private ComponentDataFromEntity<AStarPath> allPaths;
        private ComponentDataFromEntity<Waiting> allWaiting;
        private BufferFromEntity<Int2BufferElement> allPathLists;

        private Grid2dSystem gridSystem;

        private NativeArray<int2> neighborOffsets;

        protected override void OnCreateManager() {
            this.query = GetEntityQuery(typeof(Signal), typeof(RequestAStarParallelFor), 
                typeof(AStarSearchParameters), typeof(AStarPath), typeof(Int2BufferElement), typeof(Waiting));
            this.gridSystem = World.Active.GetOrCreateSystem<Grid2dSystem>();

            // Prepare neighbor offsets
            this.neighborOffsets = new NativeArray<int2>(8, Allocator.Persistent);
            this.neighborOffsets[1] = new int2(0, -1); // Bottom
            this.neighborOffsets[6] = new int2(1, -1); // Bottom-Right
            this.neighborOffsets[7] = new int2(-1, -1); // Bottom-Left
            this.neighborOffsets[0] = new int2(0, 1); // Top
            this.neighborOffsets[2] = new int2(1, 0); // Right
            this.neighborOffsets[3] = new int2(-1, 0); // Left
            this.neighborOffsets[4] = new int2(1, 1); // Top-Right
            this.neighborOffsets[5] = new int2(-1, 1); // Top-Left
        }

        protected override void OnDestroyManager() {
            this.neighborOffsets.Dispose();
        }

        protected override JobHandle OnUpdate(JobHandle inputDeps) {
            this.entityType = GetArchetypeChunkEntityType();

            this.allTiles = GetComponentDataFromEntity<Tile>(true);

            this.allParameters = GetComponentDataFromEntity<AStarSearchParameters>(true);
            this.allPaths = GetComponentDataFromEntity<AStarPath>();
            this.allWaiting = GetComponentDataFromEntity<Waiting>();
            this.allPathLists = GetBufferFromEntity<Int2BufferElement>();

            NativeArray<ArchetypeChunk> chunks = this.query.CreateArchetypeChunkArray(Allocator.TempJob);

            SimpleReachability reachability = new SimpleReachability() {
                gridWrapper = this.gridSystem.GridWrapper,
                allTiles = this.allTiles
            };

            JobHandle handle = inputDeps;
            for (int i = 0; i < chunks.Length; ++i) {
                handle = Process(chunks[i], reachability, handle);
            }

            chunks.Dispose();

            return handle;
        }

        private const int REQUEST_PER_BATCH = 2;

        private JobHandle Process(in ArchetypeChunk chunk, in SimpleReachability reachability,
            JobHandle inputDeps) {
            AStarSearchParallelFor<SimpleHeuristicCalculator, SimpleReachability> searchJob =
                new AStarSearchParallelFor<SimpleHeuristicCalculator, SimpleReachability>() {
                    entities = chunk.GetNativeArray(this.entityType),
                    allParameters = this.allParameters,
                    allPaths = this.allPaths,
                    allWaiting = this.allWaiting,
                    allPathLists = this.allPathLists,
                    reachability = reachability,
                    neighborOffsets = this.neighborOffsets,
                    gridWrapper = this.gridSystem.GridWrapper
                };
        
            // We used a low innerloopBatchCount here so that the execution will try to distribute to different threads
            // for every 2 requests (I just realized that's what it does)
            // Note also that we only process up to the execute count
            return searchJob.Schedule(chunk.Count, REQUEST_PER_BATCH, inputDeps);
        }
    }
}