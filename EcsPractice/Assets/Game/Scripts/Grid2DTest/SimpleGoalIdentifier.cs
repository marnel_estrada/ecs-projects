using CommonEcs;

using Unity.Mathematics;

namespace Game {
    public struct SimpleGoalIdentifier : GoalIdentifier {
        public readonly int2 goal;

        public SimpleGoalIdentifier(int2 goal) {
            this.goal = goal;
        }
        
        public bool IsGoal(int2 position) {
            return position.Equals(this.goal);
        }
    }
}