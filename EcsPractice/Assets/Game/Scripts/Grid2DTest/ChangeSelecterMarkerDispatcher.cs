using CommonEcs;

using Unity.Entities;

using UnityEngine;

namespace Game {
    [UpdateInGroup(typeof(PresentationSystemGroup))]
    public class ChangeSelecterMarkerDispatcher : MonoBehaviour {
        [SerializeField]
        private TileMarker tileMarker = TileMarker.BLOCKER;

        private EntityManager entityManager;

        private void Awake() {
            this.entityManager = World.Active.EntityManager;
        }

        // Used as Toggle UI handler
        public bool ToggleValue {
            set {
                if (this.entityManager == null) {
                    // Not yet prepared
                    return;
                }
                
                // Dispatch the signal
                Signal.Dispatch(this.entityManager, new ChangeSelectedMarker(this.tileMarker));
            }
        }
    }
}