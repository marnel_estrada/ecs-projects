namespace Game {
    public enum TileMarker : byte {
        BLOCKER = 1, 
        START = 2,
        DESTINATION = 3,
        ERASE_BLOCKER = 4
    }
}