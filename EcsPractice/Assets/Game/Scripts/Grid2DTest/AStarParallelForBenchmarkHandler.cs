using Unity.Entities;

using UnityEngine;
using UnityEngine.UI;

namespace Game {
    public class AStarParallelForBenchmarkHandler : MonoBehaviour {
        [SerializeField]
        private Text countLabel;

        private AStarParallelForBenchmarkSystem benchmarkSystem;

        private void Awake() {
            Assertion.AssertNotNull(this.countLabel);
            
            this.benchmarkSystem = World.Active.GetOrCreateSystem<AStarParallelForBenchmarkSystem>();
            this.benchmarkSystem.AddCountObserver(delegate(int count) {
                this.countLabel.text = count.ToString();
            });
        }
    }
}