﻿using CommonEcs;

using Unity.Entities;
using Unity.Jobs;

using UnityEngine;

namespace Game {
    [UpdateAfter(typeof(CollectedCommandsSystem))]
    [UpdateBefore(typeof(EndPresentationEntityCommandBufferSystem))]
    [UpdateInGroup(typeof(PresentationSystemGroup))]
    public class CollectedTestSystem : JobComponentSystem {
        private CollectedTestSystemBarrier barrier;

        protected override void OnCreate() {
            this.barrier = this.World.GetOrCreateSystem<CollectedTestSystemBarrier>();
        }

        protected override JobHandle OnUpdate(JobHandle inputDeps) {
            Job job = new Job() {
                buffer = this.barrier.CreateCommandBuffer().ToConcurrent()
            };

            JobHandle jobHandle = job.Schedule(this, inputDeps);
            this.barrier.AddJobHandleForProducer(jobHandle);

            return jobHandle;
        }
        
        private struct Job : IJobForEachWithEntity<CollectedTest> {
            public EntityCommandBuffer.Concurrent buffer;
            
            public void Execute(Entity entity, int index, ref CollectedTest data) {
                // Just destroy the entity
                this.buffer.DestroyEntity(index, entity);
                Debug.Log("Destroy in CollectedTestSystem");
            }
        }

        private class CollectedTestSystemBarrier : EntityCommandBufferSystem {
        }
    }
}