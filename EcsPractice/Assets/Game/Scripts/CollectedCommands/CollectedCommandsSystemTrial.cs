﻿using CommonEcs;

using Unity.Entities;

using UnityEngine;

namespace Game {
    public class CollectedCommandsSystemTrial : MonoBehaviour {
        private CollectedCommandsSystem collectedCommands;

        private void Awake() {
            this.collectedCommands = World.Active.GetOrCreateSystem<CollectedCommandsSystem>();
        }

        private void Update() {
            EntityCommandBuffer buffer = this.collectedCommands.Buffer;
            Entity entity = buffer.CreateEntity();
            buffer.AddComponent(entity, new CollectedTest());
        }
    }
}