﻿namespace Game {
    using Unity.Mathematics;

    using UnityEngine;

    public class RandomWaypointMover : MonoBehaviour {

        [SerializeField]
        private int waypointCount = 2;

        private Vector3[] waypoints;

        private int startIndex = 0;
        private int targetIndex = 0;

        private Transform selfTransform;

        private CountdownTimer timer;

        private void Awake() {
            this.selfTransform = this.transform;

            Assertion.Assert(this.waypointCount > 1); // Should be more than 1
            this.waypoints = new Vector3[this.waypointCount];
            for (int i = 0; i < this.waypointCount; ++i) {
                this.waypoints[i] = new float3(UnityEngine.Random.insideUnitCircle * 1.3f, 0);
            }

            // Start with the first position
            this.selfTransform.position = this.waypoints[0];
            this.startIndex = 0;
            this.targetIndex = 1;

            this.timer = new CountdownTimer(2.0f);
        }

        private void Update() {
            this.timer.Update();

            if (this.timer.HasElapsed()) {
                // Snap
                this.selfTransform.position = this.waypoints[this.targetIndex];

                // Move to next waypoint
                this.startIndex = (this.startIndex + 1) % this.waypointCount;
                this.targetIndex = (this.targetIndex + 1) % this.waypointCount;
                
                this.timer.Reset();

                return;
            }

            // Lerp
            this.selfTransform.position = Vector3.Lerp(this.waypoints[this.startIndex],
                this.waypoints[this.targetIndex], this.timer.GetRatio());
        }

    }
}
