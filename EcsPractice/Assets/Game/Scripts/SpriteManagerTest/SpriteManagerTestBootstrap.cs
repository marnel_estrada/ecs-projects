﻿using Common.Utils;

namespace Game {
    using System;
    using System.Collections.Generic;

    using Common;

    using CommonEcs;

    using Unity.Entities;
    using Unity.Mathematics;
    using Unity.Transforms;

    using UnityEngine;

    using Random = UnityEngine.Random;
    using Sprite = CommonEcs.Sprite;
    using Static = Unity.Transforms.Static;

    public class SpriteManagerTestBootstrap : MonoBehaviour {

        private enum TestType {
            TestSharedSpriteManager,
            TestMultipleSprites,
            TestTransform,
            TestExpand,
            TestRemoval,
            TestColor,
            TestTwoUvs,
            TestStaticTwoUvs,
            TestStaticTwoUvsWithAlter,
            TestTransformAccessArrayWithPivot,
            TestLayerOrder,
            TestSingleFace,
            TestSpriteLayer,
            TestSpriteLayerWithFewAlteredSprites,
            TestStaticTwoUvsWithFewAlters,
            TestSpriteInLayerHandle
        }

        [SerializeField]
        private TestType testType = TestType.TestTwoUvs;

        [SerializeField]
        private Material material;

        [SerializeField]
        private SpriteSimpleRendererComponent cellView;

        [SerializeField]
        private GameObject spriteBuilderPrefab;

        [SerializeField]
        private bool alwaysUpdateMesh;
        
        [SerializeField]
        private int spriteCount = 10;

        [SerializeField]
        private int staticCount = 1000;
        
        [SerializeField]
        private int nonStaticCount = 100;

        [SerializeField]
        private int transformAccessTestCount = 10;

        [SerializeField]
        private int testLayerOrderCount = 20;

        [SerializeField]
        private string sortingLayer;

        [SerializeField]
        private bool spriteLayerAlwaysUpdateMesh;

        [SerializeField]
        private bool useMeshRenderer;
        
        [SerializeField]
        private GameObject alteredPrefab;

        [SerializeField]
        private int alteredCount = 20;

        private EntityManager entityManager;

        private Entity spriteManager;

        private void Awake() {
            Assertion.AssertNotNull(this.material);
            Assertion.AssertNotNull(this.cellView);
            Assertion.AssertNotNull(this.spriteBuilderPrefab);

            this.entityManager = World.Active.EntityManager;
            Assertion.AssertNotNull(this.entityManager);

            // We prepare the mesh so we know the width and height
            this.cellView.PrepareMesh();
            
            this.spriteManager = CreateSpriteManager(100);

            Dictionary<TestType, Action> testMap = new Dictionary<TestType, Action>() {
                { TestType.TestSharedSpriteManager, TestSharedSpriteManager },
                { TestType.TestMultipleSprites, TestMultipleSprites },
                { TestType.TestTransform, TestTransform },
                { TestType.TestExpand, TestExpand },
                { TestType.TestRemoval, TestRemoval },
                { TestType.TestColor, TestColor },
                { TestType.TestTwoUvs, TestTwoUvs },
                { TestType.TestStaticTwoUvs, TestStaticTwoUvs },
                { TestType.TestStaticTwoUvsWithAlter, TestStaticTwoUvsWithAlter },
                { TestType.TestTransformAccessArrayWithPivot, TestTransformAccessArrayWithPivot },
                { TestType.TestLayerOrder, TestLayerOrder },
                { TestType.TestSingleFace, TestSingleFace },
                { TestType.TestSpriteLayer, TestSpriteLayer },
                { TestType.TestSpriteLayerWithFewAlteredSprites, TestSpriteLayerWithFewAlteredSprites },
                { TestType.TestStaticTwoUvsWithFewAlters, TestStaticTwoUvsWithFewAlters },
                { TestType.TestSpriteInLayerHandle, TestSpriteInLayerHandle }
            };
            
            // Execute the current selected test
            testMap[this.testType]();
        }

        private void TestMultipleSprites() {
            for (int i = 0; i < this.spriteCount; ++i) {
                Vector2 random = UnityEngine.Random.insideUnitCircle * 1.3f;
                AddSprite(this.spriteManager, new float3(random, 0));
            }
        }

        private void TestTransform() {
            // Add non static
            for (int i = 0; i < this.nonStaticCount; ++i) {
                Vector2 random = UnityEngine.Random.insideUnitCircle * 1.3f;
                Entity entity = AddSprite(this.spriteManager, new float3(random, 0), false); // Add a non static sprite

                // Add some dummy movement
                this.entityManager.AddComponentData(entity, new DurationTimer(2.0f, 0));
                this.entityManager.AddComponentData(entity, new MoveBackAndForth(1));
                
                // Add rotation so we can see that rotation works
                // We just reuse the one from shooter game
                this.entityManager.AddComponentData(entity, new Rotation());
                this.entityManager.AddComponentData(entity, new UfoRotation(UnityEngine.Random.Range(90.0f, 180.0f)));

                // Prepare the waypoints
                this.entityManager.AddComponent(entity, ComponentType.Create<Waypoint>());
                DynamicBuffer<Waypoint> waypoints = this.entityManager.GetBuffer<Waypoint>(entity);
                waypoints.Add(new Waypoint(){ position = new float3(random, 0) }); // Use the initial position
                waypoints.Add(new Waypoint(){ position = new float3(UnityEngine.Random.insideUnitCircle * 1.3f, 0) });
            }
        }

        private bool isTestRemoval = false;
        private readonly SimpleList<Entity> spriteEntities = new SimpleList<Entity>(10);

        private void TestRemoval() {
            this.isTestRemoval = true;

            // Add non static
            int removalSpriteCount = 1;
            for (int i = 0; i < removalSpriteCount; ++i) {
                AddSprite();
            }
        }

        private void AddSprite() {
            Vector2 random = UnityEngine.Random.insideUnitCircle * 1.3f;
            Entity spriteEntity =
                AddSprite(this.spriteManager, new float3(random, 0), false); // Add a non static sprite

            // Add some dummy movement
            this.entityManager.AddComponentData(spriteEntity, new DurationTimer(2.0f, 0));
            this.entityManager.AddComponentData(spriteEntity, new MoveBackAndForth(1));

            // Prepare the waypoints
            this.entityManager.AddComponent(spriteEntity, ComponentType.Create<Waypoint>());
            DynamicBuffer<Waypoint> waypoints = this.entityManager.GetBuffer<Waypoint>(spriteEntity);
            waypoints.Add(new Waypoint(){ position = new float3(random, 0) }); // Use the initial position
            waypoints.Add(new Waypoint(){ position = new float3(UnityEngine.Random.insideUnitCircle * 1.3f, 0) });

            this.spriteEntities.Add(spriteEntity);
        }

        private void Update() {
            DoTestRemovalUpdate();
            this.handleTest?.Update();
        }

        private void DoTestRemovalUpdate() {
            if (!this.isTestRemoval) {
                // Not testing removal
                return;
            }

            if (Input.GetKeyDown(KeyCode.R)) {
                if (this.spriteEntities.Count <= 0) {
                    // No more sprites to remove
                    return;
                }

                // Remova a random sprite
                // Note here that we remove by adding a ForRemoval tag
                int index = UnityEngine.Random.Range(0, this.spriteEntities.Count);
                this.entityManager.AddComponentData(this.spriteEntities[index], new ForRemoval());
                this.spriteEntities.RemoveAt(index);
            }

            if (Input.GetKeyDown(KeyCode.A)) {
                AddSprite();
            }
        }

        private void TestColor() {
            // Add non static
            int count = this.nonStaticCount;
            for (int i = 0; i < count; ++i) {
                Vector2 random = UnityEngine.Random.insideUnitCircle * 1.3f;
                Color color = new Color(UnityEngine.Random.Range(0.0f, 1.0f), UnityEngine.Random.Range(0.0f, 1.0f),
                    UnityEngine.Random.Range(0.0f, 1.0f), UnityEngine.Random.Range(0.0f, 1.0f));
                Entity entity =
                    AddSprite(this.spriteManager, new float3(random, 0), color, false); // Add a non static sprite

                // Add some dummy movement
                this.entityManager.AddComponentData(entity, new DurationTimer(2.0f, 0));
                this.entityManager.AddComponentData(entity, new MoveBackAndForth(1));

                // Prepare the waypoints
                this.entityManager.AddComponent(entity, ComponentType.Create<Waypoint>());
                DynamicBuffer<Waypoint> waypoints = this.entityManager.GetBuffer<Waypoint>(entity);
                waypoints.Add(new Waypoint(){ position = new float3(random, 0) }); // Use the initial position
                waypoints.Add(new Waypoint(){ position = new float3(UnityEngine.Random.insideUnitCircle * 1.3f, 0) });
            }
        }

        private void TestExpand() {
            int allocationCount = 1000;

            // Add non static
            int spriteCount = allocationCount * 10;
            for (int i = 0; i < spriteCount; ++i) {
                Vector2 random = UnityEngine.Random.insideUnitCircle * 1.3f;
                Entity entity = AddSprite(this.spriteManager, new float3(random, 0), false); // Add a non static sprite

                // Add some dummy movement
                this.entityManager.AddComponentData(entity, new DurationTimer(2.0f, 0));
                this.entityManager.AddComponentData(entity, new MoveBackAndForth(1));

                // Prepare the waypoints
                this.entityManager.AddComponent(entity, ComponentType.Create<Waypoint>());
                DynamicBuffer<Waypoint> waypoints = this.entityManager.GetBuffer<Waypoint>(entity);
                waypoints.Add(new Waypoint(){ position = new float3(random, 0) }); // Use the initial position
                waypoints.Add(new Waypoint(){ position = new float3(UnityEngine.Random.insideUnitCircle * 1.3f, 0) });
            }
        }

        private void TestSharedSpriteManager() {
            AddSprite(this.spriteManager, new float3(-0.5f, -0.5f, 0));
            AddSprite(this.spriteManager, new float3(-0.5f, 0.5f, 0));
            AddSprite(this.spriteManager, new float3(0.5f, -0.5f, 0));
            AddSprite(this.spriteManager, new float3(0.5f, 0.5f, 0));
        }
        
        private void TestTransformAccessArrayWithPivot() {
            for (int i = 0; i < this.transformAccessTestCount; ++i) {
                GenerateGameObjectSprite();
            }
        }

        private GameObject GenerateGameObjectSprite() {
            return GenerateGameObjectSprite(this.spriteBuilderPrefab);
        }

        private GameObject GenerateGameObjectSprite(GameObject prefab) {
            GameObject go = Instantiate(prefab);
            Vector3 position = Random.insideUnitCircle * 1.3f;
            go.transform.position = position;

            // Assign the sprite manager
            SpriteWrapper wrapper = go.GetComponent<SpriteWrapper>();
            wrapper.SpriteManagerEntity = this.spriteManager;

            int faceX = UnityEngine.Random.Range(0, FACE_COUNT);
            wrapper.SetUv(GetRandomFaceUv(faceX), new float2(HEAD_UV_LENGTH, HEAD_UV_LENGTH));
            wrapper.SetUv2(GetRandomHairUv(faceX), new float2(HEAD_UV_LENGTH, HEAD_UV_LENGTH));

            wrapper.Width = SPRITE_DIMENSION;
            wrapper.Height = SPRITE_DIMENSION;

            // Note that UseYAsSortOrder does not run on static sprites
            // We bake the render order upon creation
            wrapper.RenderOrder = -position.y;

            return go;
        }

        private void TestLayerOrder() {
            {
                const int layer = 1;
                int faceX = 0;
                int faceY = 0;
                int hairY = 4;
                GenerateFaceSprites(this.testLayerOrderCount, layer, faceX, faceY, hairY);
            }
            
            {
                const int layer = 2;
                int faceX = 1;
                int faceY = 2;
                int hairY = 6;
                GenerateFaceSprites(this.testLayerOrderCount, layer, faceX, faceY, hairY);
            }
        }

        private void TestSingleFace() {
            // Get the face indexes from FaceHairData
            FaceHairData data = this.GetRequiredComponent<FaceHairData>();
            GenerateFaceSprites(this.transformAccessTestCount, 0, data.faceX, data.faceY, data.hairY);
        }

        private void GenerateFaceSprites(int count, int layer, int faceX, int faceY, int hairY) {
            for (int i = 0; i < count; ++i) {
                GameObject go = Instantiate(this.spriteBuilderPrefab);
                Vector3 position = Random.insideUnitCircle * 1.3f;
                go.transform.position = position;
                
                // Assign the sprite manager
                SpriteWrapper wrapper = go.GetComponent<SpriteWrapper>();
                wrapper.SpriteManagerEntity = this.spriteManager;
                
                wrapper.SetUv(ComputeFaceHairUv(faceX, faceY), new float2(HEAD_UV_LENGTH, HEAD_UV_LENGTH));
                wrapper.SetUv2(ComputeFaceHairUv(faceX, hairY), new float2(HEAD_UV_LENGTH, HEAD_UV_LENGTH));

                wrapper.Width = SPRITE_DIMENSION;
                wrapper.Height = SPRITE_DIMENSION;

                wrapper.LayerOrder = layer;
            }
        }
        
        private void TestTwoUvs() {
            // Add non static
            for (int i = 0; i < this.nonStaticCount; ++i) {
                Vector2 random = UnityEngine.Random.insideUnitCircle * 1.3f;
                Entity entity = AddFaceSprite(this.spriteManager, new float3(random, 0), ColorUtils.WHITE, false); // Add a non static sprite

                // Add some dummy movement
                this.entityManager.AddComponentData(entity, new DurationTimer(2.0f, 0));
                this.entityManager.AddComponentData(entity, new MoveBackAndForth(1));

                // Prepare the waypoints
                this.entityManager.AddComponent(entity, ComponentType.Create<Waypoint>());
                DynamicBuffer<Waypoint> waypoints = this.entityManager.GetBuffer<Waypoint>(entity);
                
                // Use the initial position
                waypoints.Add(new Waypoint() {
                    position = new float3(random, 0)
                }); 
                
                waypoints.Add(new Waypoint() {
                    position = new float3(UnityEngine.Random.insideUnitCircle * 1.3f, 0)
                });
            }
        }
        
        private void TestStaticTwoUvs() {
            // Add static
            for (int i = 0; i < this.staticCount; ++i) {
                Vector2 random = UnityEngine.Random.insideUnitCircle * 1.3f;
                AddFaceSprite(this.spriteManager, new float3(random, 0), ColorUtils.WHITE); // Add a static sprite
            }
        }

        private void TestStaticTwoUvsWithFewAlters() {
            // Add static
            int nonAlteredCount = this.staticCount - this.alteredCount;
            for (int i = 0; i < nonAlteredCount; ++i) {
                Vector2 random = UnityEngine.Random.insideUnitCircle * 1.3f;
                AddFaceSprite(this.spriteManager, new float3(random, 0), ColorUtils.WHITE); // Add a static sprite
            }
            
            
            // Add static but has alters
            // These entities will be processed by TimedSpriteAlterSystem
            for (int i = 0; i < this.alteredCount; ++i) {
                Vector2 random = UnityEngine.Random.insideUnitCircle * 1.3f;
                Entity entity = AddFaceSprite(this.spriteManager, new float3(random, 0), ColorUtils.WHITE); // Add a static sprite
                this.entityManager.AddComponentData(entity, new SpriteAlter());
                
                // Rate of alter
                // Between 7 fps to 15fps
                float timeInterval = UnityEngine.Random.Range(1.0f / 7.5f,  1.0f / 15.0f);
                this.entityManager.AddComponentData(entity, new DurationTimer(timeInterval));
            }
        }

        private void TestStaticTwoUvsWithAlter() {
            // Add non static
            for (int i = 0; i < this.staticCount; ++i) {
                Vector2 random = UnityEngine.Random.insideUnitCircle * 1.3f;
                Entity spriteEntity = AddFaceSprite(this.spriteManager, new float3(random, 0), ColorUtils.WHITE, true); // Add a static sprite
                this.entityManager.AddComponentData(spriteEntity, new SpriteAlter());
            }
        }

        private void TestSpriteLayer() {
            // Create a layer
            Entity spriteLayerEntity = CreateSpriteLayer();

            // Add sprites that adds to the layer
            // Note here that adding MonoBehaviour at runtime is not resolved or caught in ECS (beware)
            for (int i = 0; i < this.transformAccessTestCount; ++i) {
                GameObject go = GenerateGameObjectSprite();
                AddToSpriteLayerWrapper addToLayerWrapper = go.GetRequiredComponent<AddToSpriteLayerWrapper>();
                addToLayerWrapper.componentData.layerEntity = spriteLayerEntity;
            }
        }

        private void TestSpriteLayerWithFewAlteredSprites() {
            // Create a layer
            Entity spriteLayerEntity = CreateSpriteLayer();

            // Add sprites that adds to the layer
            // Note here that adding MonoBehaviour at runtime is not resolved or caught in ECS (beware)
            int nonAlterCount = this.transformAccessTestCount - this.alteredCount;
            for (int i = 0; i < nonAlterCount; ++i) {
                GameObject go = GenerateGameObjectSprite();
                AddToSpriteLayerWrapper addToLayerWrapper = go.GetRequiredComponent<AddToSpriteLayerWrapper>();
                addToLayerWrapper.componentData.layerEntity = spriteLayerEntity;
            }

            for (int i = 0; i < this.alteredCount; ++i) {
                GameObject go = GenerateGameObjectSprite(this.alteredPrefab);
                AddToSpriteLayerWrapper addToLayerWrapper = go.GetRequiredComponent<AddToSpriteLayerWrapper>();
                addToLayerWrapper.componentData.layerEntity = spriteLayerEntity;
            }
        }

        private SpriteInLayerHandleTest handleTest;
        
        private void TestSpriteInLayerHandle() {
            // Create a layer
            Entity spriteLayerEntity = CreateSpriteLayer();
            this.handleTest = new SpriteInLayerHandleTest(this.entityManager, spriteLayerEntity, 
                this.staticCount, this.alteredCount);
        }

        private Entity CreateSpriteLayer() {
            Entity spriteLayerEntity = this.entityManager.CreateEntity();
            SpriteLayer spriteLayer = new SpriteLayer(spriteLayerEntity, this.material, 100);
            spriteLayer.alwaysUpdateMesh = this.spriteLayerAlwaysUpdateMesh;
            spriteLayer.useMeshRenderer = this.useMeshRenderer;
            spriteLayer.layer = this.gameObject.layer;
            spriteLayer.SetSortingLayerId(SortingLayer.NameToID(this.sortingLayer));
            this.entityManager.AddSharedComponentData(spriteLayerEntity, spriteLayer);

            return spriteLayerEntity;
        }

        private Entity CreateSpriteManager(int allocationCount) {
            Entity managerEntity = this.entityManager.CreateEntity();

            // Prepare a SpriteManager
            SpriteManager spriteManager = new SpriteManager();
            spriteManager.Init(allocationCount);
            spriteManager.Name = this.gameObject.name;
            spriteManager.Owner = managerEntity;
            spriteManager.SetMaterial(this.material);
            spriteManager.Layer = this.gameObject.layer;
            spriteManager.SortingLayer = SortingLayer.GetLayerValueFromName(this.sortingLayer);
            spriteManager.AlwaysUpdateMesh = this.alwaysUpdateMesh;
            this.entityManager.AddSharedComponentData(managerEntity, spriteManager);

            return managerEntity;
        }

        private Entity AddSprite(Entity spriteManager, float3 position, bool isStatic = true) {
            return AddSprite(spriteManager, position, Color.white, isStatic);
        }

        // Another version of AddSprite that has color
        private Entity AddSprite(Entity spriteManager, float3 position, Color color, bool isStatic = true) {
            Entity spriteEntity = this.entityManager.CreateEntity();
            Sprite sprite = new Sprite();

            float lowerLeftX = UnityEngine.Random.Range(0, 2) * 0.5f;
            float lowerLeftY = UnityEngine.Random.Range(0, 2) * 0.5f;

            // We used half width and height because the tileset is 2x2
            sprite.Init(spriteManager, this.cellView.Width * 0.5f, this.cellView.Height * 0.5f, new float2(0.5f, 0.5f));
            sprite.SetUv(new float2(lowerLeftX, lowerLeftY), new float2(0.5f, 0.5f));
            sprite.SetColor(color);
            this.entityManager.AddComponentData(spriteEntity, sprite);

            this.entityManager.AddComponentData(spriteEntity, new Translation() {
                Value = position
            });
            
            this.entityManager.AddComponentData(spriteEntity, new UseYAsSortOrder());

            if (isStatic) {
                this.entityManager.AddComponentData(spriteEntity, new Static());
            }

            return spriteEntity;
        }

        private const float HEAD_UV_LENGTH = 64.0f / 2048.0f;
        private const float UNITS_PER_PIXEL = 1.0f / 768.0f;
        public const float SPRITE_DIMENSION = 64 * UNITS_PER_PIXEL;

        public static readonly float2 HEAD_UV_DIMENSION = new float2(HEAD_UV_LENGTH, HEAD_UV_LENGTH);
        
        private Entity AddFaceSprite(Entity spriteManager, float3 position, Color color, bool isStatic = true) {
            Entity entity = this.entityManager.CreateEntity();
            Sprite sprite = new Sprite();

            // We used half width and height because the tileset is 2x2
            sprite.Init(spriteManager, SPRITE_DIMENSION, SPRITE_DIMENSION, new float2(0.5f, 0.0f));
            int faceX = GenerateFaceX();
            sprite.SetUv(GetRandomFaceUv(faceX), HEAD_UV_DIMENSION);
            sprite.SetUv2(GetRandomHairUv(faceX), HEAD_UV_DIMENSION);
            sprite.SetColor(color);
            sprite.RenderOrder = -position.y;
            this.entityManager.AddComponentData(entity, sprite);

            this.entityManager.AddComponentData(entity, new Translation() {
                Value = position
            });
            
            this.entityManager.AddComponentData(entity, new LocalToWorld());
            
            this.entityManager.AddComponentData(entity, new UseYAsSortOrder());

            if (isStatic) {
                this.entityManager.AddComponentData(entity, new Static());
            }

            return entity;
        }

        private static readonly int[] FACE_ROWS = new int[] {
            0, 2
        };

        private const int FACE_COUNT = 16;

        private static readonly System.Random RANDOM = new System.Random();

        /// <summary>
        /// Common algorithm for generating the face x index
        /// </summary>
        /// <returns></returns>
        public static int GenerateFaceX() {
            return RANDOM.Next(0, FACE_COUNT);
        }
        
        /// <summary>
        /// Common algorithm for getting a random face
        /// </summary>
        /// <param name="faceX"></param>
        /// <returns></returns>
        public static float2 GetRandomFaceUv(int faceX) {
            int y = FACE_ROWS[RANDOM.Next(0, FACE_ROWS.Length)];
            return ComputeFaceHairUv(faceX, y);
        }
        
        private static readonly int[] HAIR_ROWS = new int[] {
            4, 6
        };

        /// <summary>
        /// Common algorithm for getting a random hair
        /// </summary>
        /// <param name="faceX"></param>
        /// <returns></returns>
        public static float2 GetRandomHairUv(int faceX) {
            int y = HAIR_ROWS[RANDOM.Next(0, HAIR_ROWS.Length)];
            return ComputeFaceHairUv(faceX, y);
        }

        private static float2 ComputeFaceHairUv(int x, int y) {
            return new float2(x * HEAD_UV_LENGTH, y * HEAD_UV_LENGTH);
        }

    }
}
