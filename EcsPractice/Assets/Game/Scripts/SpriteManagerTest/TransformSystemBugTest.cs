﻿using Unity.Entities;
using Unity.Transforms;

using UnityEngine;

namespace Game {
    public class TransformSystemBugTest : MonoBehaviour {
        private EntityManager entityManager;

        private void Awake() {
            this.entityManager = World.Active.EntityManager;
        }

        private void Update() {
            if (Input.GetKey(KeyCode.Space)) {
                Entity entity = this.entityManager.CreateEntity();
                this.entityManager.AddComponentData(entity, new Translation());
                this.entityManager.AddComponentData(entity, new Static());
                Debug.Log("Added dummy");
            }
        }
    }
}