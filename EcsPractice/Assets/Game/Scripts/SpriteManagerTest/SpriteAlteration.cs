﻿namespace Game {
    using Common;

    using CommonEcs;

    using Unity.Entities;

    using UnityEngine;
    
    using Sprite = CommonEcs.Sprite;

    public class SpriteAlteration : MonoBehaviour {

        // private Entity entity;
        //
        // private EntityManager entityManager;
        [SerializeField]
        private SpriteWrapper wrapper;

        private CountdownTimer timer;

        private void Awake() {
            Assertion.AssertNotNull(this.wrapper);

            ResetTimer();
        }

        private void Update() {
            this.timer.Update();

            if (this.timer.HasElapsed()) {
                // Time to update
                UpdateSprite();
                ResetTimer();
            }
        }

        private void ResetTimer() {
            if (this.timer == null) {
                this.timer = new CountdownTimer(UnityEngine.Random.Range(1.0f, 4.0f));
            } else {
                this.timer.Reset(UnityEngine.Random.Range(1.0f, 4.0f));
            }
        }

        private void UpdateSprite() {
            this.wrapper.SyncSprite();
            int faceX = SpriteManagerTestBootstrap.GenerateFaceX();
            this.wrapper.SetUv(SpriteManagerTestBootstrap.GetRandomFaceUv(faceX), SpriteManagerTestBootstrap.HEAD_UV_DIMENSION);
            this.wrapper.SetUv2(SpriteManagerTestBootstrap.GetRandomHairUv(faceX), SpriteManagerTestBootstrap.HEAD_UV_DIMENSION);
            this.wrapper.Color = ColorUtils.Random();
            this.wrapper.Commit();
        }

    }
}
