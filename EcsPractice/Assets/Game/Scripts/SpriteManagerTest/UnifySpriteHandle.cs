﻿namespace Game {
    using Unify;

    using UnityEngine;

    using Sprite = Unify.Sprite;

    public class UnifySpriteHandle : MonoBehaviour {

        private Sprite sprite;
        private Transform cachedTransform;

        private const float UV_LENGTH = 64.0f / 2048.0f;
        private static readonly Vector2 UV_DIMENSION = new Vector2(UV_LENGTH, UV_LENGTH);
        
        private const float UNITS_PER_PIXEL = 1.0f / 768.0f;
        private const float SPRITE_DIMENSION = 64 * UNITS_PER_PIXEL;
        
        private const int FACE_COUNT = 16;
        
        private static readonly int[] FACE_ROWS = new int[] {
            0, 2
        };
        
        private static readonly int[] HAIR_ROWS = new int[] {
            4, 6
        };

        private void Awake() {
            this.cachedTransform = this.transform;
        }
        
        /// <summary>
        /// Initializer
        /// Create the random face sprite
        /// </summary>
        /// <param name="spriteManager"></param>
        public void Init(SpriteManager spriteManager) {
            int faceX = UnityEngine.Random.Range(0, FACE_COUNT);
            this.sprite = spriteManager.AddSprite(this.gameObject, SPRITE_DIMENSION, SPRITE_DIMENSION,
                GetRandomFaceUv(faceX), UV_DIMENSION, GetRandomHairUv(faceX), UV_DIMENSION, VectorUtils.ZERO, false);
        }
        
        private static Vector2 GetRandomFaceUv(int faceX) {
            int y = FACE_ROWS[UnityEngine.Random.Range(0, FACE_ROWS.Length)];

            return new Vector2(faceX * UV_LENGTH, y * UV_LENGTH);
        }
        
        private static Vector2 GetRandomHairUv(int faceX) {
            int y = HAIR_ROWS[UnityEngine.Random.Range(0, FACE_ROWS.Length)];

            return new Vector2(faceX * UV_LENGTH, y * UV_LENGTH);
        }

        private void Update() {
            // Higher Y will be rendered first
            this.sprite.SetDrawLayer(-this.cachedTransform.position.y);
        }

    }
}