﻿namespace Game {
    using UnityEngine;
    
    public class FaceHairData : MonoBehaviour {
        public int faceX = 0;
        public int faceY = 0;
        public int hairY = 4;
    }
}