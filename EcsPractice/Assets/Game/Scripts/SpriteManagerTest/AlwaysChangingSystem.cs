﻿using CommonEcs;

using Unity.Collections;
using Unity.Entities;

namespace Game {
    [UpdateBefore(typeof(IdentifySpriteManagerChangedSystem))]
    [UpdateInGroup(typeof(PresentationSystemGroup))]
    public class AlwaysChangingSystem : ComponentSystem {
        private EntityQuery query;
        private ArchetypeChunkEntityType entityType;
        
        protected override void OnCreateManager() {
            this.query = GetEntityQuery(typeof(AlwaysChanging));
        }

        protected override void OnUpdate() {
            this.entityType = GetArchetypeChunkEntityType();
            NativeArray<ArchetypeChunk> chunks = this.query.CreateArchetypeChunkArray(Allocator.TempJob);

            for (int i = 0; i < chunks.Length; ++i) {
                ArchetypeChunk chunk = chunks[i];
                Process(ref chunk);
            }
            
            chunks.Dispose();
        }

        private void Process(ref ArchetypeChunk chunk) {
            NativeArray<Entity> entities = chunk.GetNativeArray(this.entityType);
            for (int i = 0; i < entities.Length; ++i) {
                if (this.EntityManager.HasComponent<Changed>(entities[i])) {
                    // Already has the changed component    
                    continue;
                }
                
                this.PostUpdateCommands.AddComponent(entities[i], new Changed());
            }
        }       
    }
}