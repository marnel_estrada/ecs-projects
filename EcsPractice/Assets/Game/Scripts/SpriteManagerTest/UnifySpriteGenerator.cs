﻿namespace Game {
    using Common;

    using Unify;

    using UnityEngine;
    
    public class UnifySpriteGenerator : MonoBehaviour {

        [SerializeField]
        private int count;

        [SerializeField]
        private GameObject prefab; // The sprite prefab

        [SerializeField]
        private SpriteManager spriteManager;

        private void Awake() {
            Assertion.AssertNotNull(this.prefab);
            Assertion.AssertNotNull(this.spriteManager);
            
            for (int i = 0; i < this.count; ++i) {
                // Generate a sprite
                GameObject go = Instantiate(this.prefab);
                UnifySpriteHandle handle = go.GetRequiredComponent<UnifySpriteHandle>();
                handle.Init(this.spriteManager);
            }
        }

        private void LateUpdate() {
            this.spriteManager.SortDrawingOrder();
        }

    }
}