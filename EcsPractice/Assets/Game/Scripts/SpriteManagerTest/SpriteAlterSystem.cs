﻿using CommonEcs;

using Unity.Collections;
using Unity.Entities;

using UnityEngine;

using Sprite = CommonEcs.Sprite;

namespace Game {
    [UpdateBefore(typeof(IdentifySpriteManagerChangedSystem))]
    [UpdateInGroup(typeof(PresentationSystemGroup))]
    public class SpriteAlterSystem : ComponentSystem {
        private EntityQuery query;

        private ArchetypeChunkEntityType entityType;
        private ArchetypeChunkComponentType<Sprite> spriteType;

        protected override void OnCreate() {
            this.query = GetEntityQuery(typeof(Sprite), ComponentType.ReadOnly<SpriteAlter>());
        }

        protected override void OnUpdate() {
            if (Input.GetKeyDown(KeyCode.Space)) {
                this.entityType = GetArchetypeChunkEntityType();
                this.spriteType = GetArchetypeChunkComponentType<Sprite>();
                
                NativeArray<ArchetypeChunk> chunks = this.query.CreateArchetypeChunkArray(Allocator.TempJob);
                
                // Pick random chunk
                int index = UnityEngine.Random.Range(0, chunks.Length);
                Process(chunks[index]);
                
                chunks.Dispose();
            }
        }

        private void Process(ArchetypeChunk chunk) {
            NativeArray<Entity> entities = chunk.GetNativeArray(this.entityType);
            NativeArray<Sprite> sprites = chunk.GetNativeArray(this.spriteType);
            
            // Pick a random sprite to alter
            int index = Random.Range(0, sprites.Length);
            Sprite sprite = sprites[index];
                
            int faceX = SpriteManagerTestBootstrap.GenerateFaceX();
            sprite.SetUv(SpriteManagerTestBootstrap.GetRandomFaceUv(faceX), SpriteManagerTestBootstrap.HEAD_UV_DIMENSION);
            sprite.SetUv2(SpriteManagerTestBootstrap.GetRandomHairUv(faceX), SpriteManagerTestBootstrap.HEAD_UV_DIMENSION);
            sprites[index] = sprite; // Modify
                
            // Add this component so it will be filtered by IdentifySpriteManagerChangedValuesSystem
            this.PostUpdateCommands.AddComponent(entities[index], new Changed());
        }
    }
}
