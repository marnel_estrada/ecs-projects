﻿using System.Collections.Generic;

using Common;

using CommonEcs;

using Unity.Entities;
using Unity.Mathematics;

using UnityEngine;

using Sprite = CommonEcs.Sprite;

namespace Game {
    public class SpriteInLayerHandleTest {

        private readonly EntityManager entityManager;
        private readonly Entity spriteLayerEntity;
        
        private readonly List<SpriteInLayerHandle> handles = new List<SpriteInLayerHandle>();
        
        public SpriteInLayerHandleTest(EntityManager entityManager, Entity spriteLayerEntity, int staticCount, int alteredCount) {
            this.entityManager = entityManager;
            this.spriteLayerEntity = spriteLayerEntity;
            
            // Add static
            int nonAlteredCount = staticCount - alteredCount;
            for (int i = 0; i < nonAlteredCount; ++i) {
                AddSprite(true);
            }
            
            // Add static but has alters
            // These entities will be processed by TimedSpriteAlterSystem
            for (int i = 0; i < alteredCount; ++i) {
                Vector2 random = UnityEngine.Random.insideUnitCircle * 1.3f;
                SpriteInLayerHandle handle = PrepareHandle(new float3(random, 0), true);
                this.entityManager.AddComponentData(handle.SpriteEntity, new SpriteAlter());
                
                // Rate of alter
                // Between 7 fps to 15fps
                float timeInterval = UnityEngine.Random.Range(1.0f / 7.5f,  1.0f / 15.0f);
                this.entityManager.AddComponentData(handle.SpriteEntity, new DurationTimer(timeInterval));
                
                this.handles.Add(handle);
            }
        }

        private void AddSprite(bool isStatic) {
            Vector2 random = UnityEngine.Random.insideUnitCircle * 1.3f;
            this.handles.Add(PrepareHandle(new float3(random, 0), isStatic));
        }

        private SpriteInLayerHandle PrepareHandle(float3 position, bool isStatic) {
            Sprite sprite = new Sprite();

            // We set null for the entity manager since it will be specified
            // when it will be added to the sprite layer
            sprite.Init(Entity.Null, SpriteManagerTestBootstrap.SPRITE_DIMENSION, SpriteManagerTestBootstrap.SPRITE_DIMENSION, new float2(0.5f, 0.0f));
            int faceX = SpriteManagerTestBootstrap.GenerateFaceX();
            sprite.SetUv(SpriteManagerTestBootstrap.GetRandomFaceUv(faceX), SpriteManagerTestBootstrap.HEAD_UV_DIMENSION);
            sprite.SetUv2(SpriteManagerTestBootstrap.GetRandomHairUv(faceX), SpriteManagerTestBootstrap.HEAD_UV_DIMENSION);
            sprite.RenderOrder = -position.y;

            SpriteInLayerHandle handle = new SpriteInLayerHandle(this.entityManager, this.spriteLayerEntity);
            handle.Create(ref sprite, position, isStatic);

            return handle;
        }

        public void Update() {
            if (Input.GetKey(KeyCode.R)) {
                RemoveLast();
            }
            
            if (Input.GetKeyDown(KeyCode.A)) {
                AddSprite(true);
            }
        }

        private void RemoveLast() {
            if (this.handles.Count <= 0) {
                // No more sprites to remove
                return;
            }
            
            int last = this.handles.Count - 1;
            this.handles[last].Destroy();
            this.handles.RemoveAt(last);
        }
    }
}
