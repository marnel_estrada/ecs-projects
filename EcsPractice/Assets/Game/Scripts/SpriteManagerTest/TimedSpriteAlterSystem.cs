﻿using Common;

using CommonEcs;

using Unity.Collections;
using Unity.Entities;
using Unity.Jobs;

using Sprite = CommonEcs.Sprite;

namespace Game {
    [UpdateAfter(typeof(DurationTimerSystem))]
    [UpdateBefore(typeof(IdentifySpriteManagerChangedSystem))]
    [UpdateInGroup(typeof(PresentationSystemGroup))]
    public class TimedSpriteAlterSystem : JobComponentSystem {
        // A barrier so we can get a command buffer
        [UpdateAfter(typeof(TimedSpriteAlterSystem))]
        [UpdateBefore(typeof(IdentifySpriteManagerChangedSystem))]
        [UpdateInGroup(typeof(PresentationSystemGroup))]
        private class TimedSpriteAlterSystemBarrier : EntityCommandBufferSystem {
        }

        private EntityQuery query;
        private ArchetypeChunkComponentType<DurationTimer> timerType;
        private ArchetypeChunkComponentType<Sprite> spriteType;
        private ArchetypeChunkEntityType entityType;

        private TimedSpriteAlterSystemBarrier barrier;
        
        protected override void OnCreateManager() {
            this.query = GetEntityQuery(typeof(Sprite), typeof(SpriteAlter), typeof(DurationTimer));
            this.barrier = this.World.GetOrCreateSystem<TimedSpriteAlterSystemBarrier>();
        }

        protected override JobHandle OnUpdate(JobHandle inputDeps) {
            this.timerType = GetArchetypeChunkComponentType<DurationTimer>();
            this.spriteType = GetArchetypeChunkComponentType<Sprite>();
            this.entityType = GetArchetypeChunkEntityType();
            NativeArray<ArchetypeChunk> chunks = this.query.CreateArchetypeChunkArray(Allocator.TempJob);
            
             Job job = new Job() {
                 timerType = this.timerType,
                 spriteType = this.spriteType,
                 entityType = this.entityType,
                 chunks = chunks,
                 commandBuffer = this.barrier.CreateCommandBuffer()
             };

             JobHandle jobHandle = job.Schedule(chunks.Length, 64, inputDeps);
             this.barrier.AddJobHandleForProducer(jobHandle);

             return jobHandle;
        }
        
        private struct Job : IJobParallelFor {
            public ArchetypeChunkComponentType<DurationTimer> timerType;

            public ArchetypeChunkComponentType<Sprite> spriteType;

            [ReadOnly]
            public ArchetypeChunkEntityType entityType;

            [ReadOnly]
            [DeallocateOnJobCompletion]
            public NativeArray<ArchetypeChunk> chunks;

            [ReadOnly]
            public EntityCommandBuffer commandBuffer;
            
            public void Execute(int index) {
                Process(this.chunks[index]);
            }

            private void Process(ArchetypeChunk chunk) {
                NativeArray<DurationTimer> timers = chunk.GetNativeArray(this.timerType);
                NativeArray<Sprite> sprites = chunk.GetNativeArray(this.spriteType);
                NativeArray<Entity> entities = chunk.GetNativeArray(this.entityType);

                for (int i = 0; i < chunk.Count; ++i) {
                    DurationTimer timer = timers[i];
                    if (!timer.HasElapsed) {
                        // Not yet time to alter the sprite
                        continue;
                    }

                    // Alter the sprite UV
                    Sprite sprite = sprites[i];
                    int faceX = SpriteManagerTestBootstrap.GenerateFaceX();
                    sprite.SetUv(SpriteManagerTestBootstrap.GetRandomFaceUv(faceX), SpriteManagerTestBootstrap.HEAD_UV_DIMENSION);
                    sprite.SetUv2(SpriteManagerTestBootstrap.GetRandomHairUv(faceX), SpriteManagerTestBootstrap.HEAD_UV_DIMENSION);
                    sprites[i] = sprite; // Modify
                    
                    // Reset the timer
                    timer.Reset();
                    timers[i] = timer;
                    
                    this.commandBuffer.AddComponent(entities[i], new Changed());
                }
            }
        }       
    }
}