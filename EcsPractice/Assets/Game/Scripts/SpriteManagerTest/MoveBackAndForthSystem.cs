﻿using Common;

using CommonEcs;

using Unity.Collections;
using Unity.Entities;
using Unity.Jobs;
using Unity.Mathematics;
using Unity.Transforms;

namespace Game {
    [UpdateAfter(typeof(DurationTimerSystem))]
    [UpdateBefore(typeof(UseYAsSortOrderSystem))]
    [UpdateBefore(typeof(MoveBackAndForthSystemBarrier))]
    [UpdateInGroup(typeof(PresentationSystemGroup))]
    public class MoveBackAndForthSystem : JobComponentSystem {
        private MoveBackAndForthSystemBarrier barrier;

        private EntityQuery query;

        protected override void OnCreate() {
            this.barrier = this.World.GetOrCreateSystem<MoveBackAndForthSystemBarrier>();
            this.query = GetEntityQuery(typeof(MoveBackAndForth), typeof(Translation), 
                typeof(DurationTimer), ComponentType.ReadOnly<Waypoint>());
        }

        protected override JobHandle OnUpdate(JobHandle inputDeps) {
            Job job = new Job() {
                commandBuffer = this.barrier.CreateCommandBuffer().ToConcurrent(),
                entityType = GetArchetypeChunkEntityType(),
                backForthType = GetArchetypeChunkComponentType<MoveBackAndForth>(),
                translationType = GetArchetypeChunkComponentType<Translation>(),
                timerType = GetArchetypeChunkComponentType<DurationTimer>(),
                waypointType = GetArchetypeChunkBufferType<Waypoint>()
            };

            return JobChunkExtensions.Schedule(job, this.query, inputDeps);
        }
        
        //[BurstCompile]
        // private struct OldJob : IJobParallelFor {
        //     public Data data;
        //
        //     public EntityCommandBuffer.Concurrent commandBuffer;
        //
        //     public void Execute(int index) {
        //         MoveBackAndForth move = this.data.Move[index];
        //         Translation position = this.data.Translation[index];
        //         DurationTimer timer = this.data.Timer[index];
        //         DynamicBuffer<Waypoint> waypoints = this.data.Waypoint[index];
        //
        //         if (timer.HasElapsed) {
        //             // This means that it's time to move to the next waypoint
        //
        //             // Snap to target position
        //             position.Value = waypoints[move.targetIndex].position;
        //             this.data.Translation[index] = position; // Modify
        //
        //             move.startIndex = (move.startIndex + 1) % waypoints.Length;
        //             move.targetIndex = (move.targetIndex + 1) % waypoints.Length;
        //             this.data.Move[index] = move; // Modify
        //
        //             timer.Reset();
        //             this.data.Timer[index] = timer; // Modify
        //
        //             return;
        //         }
        //
        //         // Interpolate
        //         position.Value = math.lerp(waypoints[move.startIndex].position, waypoints[move.targetIndex].position,
        //             timer.Ratio);
        //         this.data.Translation[index] = position; // Modify
        //         
        //         this.commandBuffer.AddComponent(index, this.data.entities[index], new Changed());
        //     }
        // }

        private struct Job : IJobChunk {
            public EntityCommandBuffer.Concurrent commandBuffer;

            public ArchetypeChunkEntityType entityType;
            public ArchetypeChunkComponentType<MoveBackAndForth> backForthType;
            public ArchetypeChunkComponentType<Translation> translationType;
            public ArchetypeChunkComponentType<DurationTimer> timerType;
            public ArchetypeChunkBufferType<Waypoint> waypointType;

            private NativeArray<Entity> entities;
            private NativeArray<MoveBackAndForth> moves;
            private NativeArray<Translation> translations;
            private NativeArray<DurationTimer> timers;
            private BufferAccessor<Waypoint> waypointBuffers;

            public void Execute(ArchetypeChunk chunk, int chunkIndex, int firstEntityIndex) {
                this.entities = chunk.GetNativeArray(this.entityType);
                this.moves = chunk.GetNativeArray(this.backForthType);
                this.translations = chunk.GetNativeArray(this.translationType);
                this.timers = chunk.GetNativeArray(this.timerType);
                this.waypointBuffers = chunk.GetBufferAccessor(this.waypointType);

                for (int i = 0; i < chunk.Count; ++i) {
                    Process(i);
                }
            }

            private void Process(int index) {
                MoveBackAndForth move = this.moves[index];
                Translation position = this.translations[index];
                DurationTimer timer = this.timers[index];
                DynamicBuffer<Waypoint> waypoints = this.waypointBuffers[index];

                if (timer.HasElapsed) {
                    // This means that it's time to move to the next waypoint

                    // Snap to target position
                    position.Value = waypoints[move.targetIndex].position;
                    this.translations[index] = position; // Modify

                    move.startIndex = (move.startIndex + 1) % waypoints.Length;
                    move.targetIndex = (move.targetIndex + 1) % waypoints.Length;
                    this.moves[index] = move; // Modify

                    timer.Reset();
                    this.timers[index] = timer; // Modify

                    return;
                }

                // Interpolate
                position.Value = math.lerp(waypoints[move.startIndex].position, waypoints[move.targetIndex].position,
                    timer.Ratio);
                this.translations[index] = position; // Modify
                
                this.commandBuffer.AddComponent(index, this.entities[index], new Changed());
            }
        }

        [UpdateBefore(typeof(IdentifySpriteManagerChangedSystem))]
        [UpdateInGroup(typeof(PresentationSystemGroup))]
        private class MoveBackAndForthSystemBarrier : EntityCommandBufferSystem {
        }
    }
}