﻿namespace Game {
    using Unity.Entities;
    
    public struct MoveBackAndForth : IComponentData {
        // Index of the starting waypoint
        public int startIndex;
        
        // Index of the target position
        public int targetIndex;

        /// <summary>
        /// Constructor with specified target index
        /// </summary>
        /// <param name="targetIndex"></param>
        public MoveBackAndForth(int targetIndex) {
            this.startIndex = 0;
            this.targetIndex = targetIndex;
        }
    }
}
