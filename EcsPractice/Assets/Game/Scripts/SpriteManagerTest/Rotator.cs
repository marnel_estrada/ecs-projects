﻿namespace Game {
    using UnityEngine;
    
    public class Rotator : MonoBehaviour {

        private float rotationSpeed;
        private Transform selfTransform;

        private void Awake() {
            this.rotationSpeed = Random.Range(45, 180);
            this.selfTransform = this.transform;
        }

        private void Update() {
            this.selfTransform.Rotate(Vector3.forward, this.rotationSpeed * Time.deltaTime);
        }

    }
}
