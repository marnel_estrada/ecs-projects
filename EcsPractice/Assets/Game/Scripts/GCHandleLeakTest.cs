using System;
using System.Runtime.InteropServices;

using UnityEngine;

namespace Game {
    public class GCHandleLeakTest : MonoBehaviour {
        private class LeakData {
            public int dummy;
        }

        private void Leak() {
            LeakData data = new LeakData();
        }

        private void Update() {
            GCHandle handle = GCHandle.Alloc((Action)Leak);
            ((Action) handle.Target)(); // Invoke the action
            handle.Free();
        }
    }
}