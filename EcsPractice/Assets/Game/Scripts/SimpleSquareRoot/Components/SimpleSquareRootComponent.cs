using Unity.Entities;

namespace Game {
    public struct SimpleSquareRootComponent : IComponentData {
        public float value;
        public float squareRoot;
    }
}