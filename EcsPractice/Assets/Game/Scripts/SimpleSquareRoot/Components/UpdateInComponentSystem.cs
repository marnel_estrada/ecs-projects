using Unity.Entities;

namespace Game {
    /// <summary>
    /// This is just a tag component that differentiates to update the entity as component system
    /// </summary>
    public struct UpdateInComponentSystem : IComponentData {
    }
}