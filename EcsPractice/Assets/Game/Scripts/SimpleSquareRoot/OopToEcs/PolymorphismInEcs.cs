using Unity.Entities;

namespace Game.PolymorphismEcs {
    // Components
    public struct Animal : IComponentData {
    }

    public struct Pig : IComponentData {
    }

    public struct Cat : IComponentData {
    }
    
    // Entity preparation
    public class Bootstrap {
        public void Init() {
            EntityManager entityManager = World.Active.EntityManager;
            
            // Pig entity
            Entity pigEntity = entityManager.CreateEntity();
            entityManager.AddComponentData(pigEntity, new Animal());
            entityManager.AddComponentData(pigEntity, new Pig());

            // Cat entity
            Entity catEntity = entityManager.CreateEntity();
            entityManager.AddComponentData(catEntity, new Animal());
            entityManager.AddComponentData(catEntity, new Cat());
        }
    }
    
    // Systems
    public class AnimalMoveSystem : ComponentSystem {
        // Filter entities with Animal component
        
        protected override void OnUpdate() {
            // Move code
        }
    }

    public class PigActionSystem : ComponentSystem {
        // Filter entities with Pig component

        protected override void OnUpdate() {
            // Execute pig actions
        }
    }
    
    public class CatActionSystem : ComponentSystem {
        // Filter entities with Cat component
        
        protected override void OnUpdate() {
            // Execute cat actions
        }
    }
}