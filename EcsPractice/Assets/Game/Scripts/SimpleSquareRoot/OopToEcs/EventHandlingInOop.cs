using System.Collections.Generic;

using UnityEngine;

namespace Game {
    public class Event {
        // Various event data
        private Vector3 position;
        private int eventType;
        // ... what have you
    }

    public interface EventListener {
        void EventTriggered(Event eventData);
    }

    public class EventCenter {
        private readonly List<EventListener> listeners = new List<EventListener>();

        public void Add(EventListener listener) {
            this.listeners.Add(listener);
        }

        public void Publish(Event eventData) {
            for (int i = 0; i < this.listeners.Count; ++i) {
                this.listeners[i].EventTriggered(eventData);
            }
        }
    }
}