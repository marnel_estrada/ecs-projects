using System.Collections.Generic;

using Unity.Entities;
using Unity.Jobs;

namespace Game.Ecs {
    // Components
    public struct Unit : IComponentData {
        public int attackDamage;
    }

    public struct Health : IComponentData {
        public int hp;
    }

    public struct TargetItem : IComponentData {
        public Entity owner;
        public Entity target;
    }

    public class GameController {
        private EntityManager entityManager;
        private Entity player;

        private List<Entity> enemies;

        public void Init() {
            // Prepare
            this.player = this.entityManager.CreateEntity();
            this.entityManager.AddComponentData(this.player, new Unit());
            this.entityManager.AddComponentData(this.player, new Health());
            
            // Enemy entities are prepared the same way
            // Each enemy also has Unit and Health components
        }
        
        public void Update() {
            // Somewhere along the game
            AssignTargets();
        }

        private void AssignTargets() {
            int targetIndex = ResolveTarget();
            
            // To add targets. Create entities with Target component
            Entity targetEntity = this.entityManager.CreateEntity();
            this.entityManager.AddComponentData(targetEntity, new TargetItem() {
                owner = this.player,
                target = this.enemies[targetIndex]
            });
        }

        private int ResolveTarget() {
            // Just a dummy
            return 0;
        }
    }

    public class DealDamageSystem : JobComponentSystem {
        private ComponentDataFromEntity<Unit> allUnits;
        private ComponentDataFromEntity<Health> allHealth;

        protected override JobHandle OnUpdate(JobHandle inputDeps) {
            Job job = new Job() {
                allUnits = this.allUnits,
                allHealth = this.allHealth
            };

            return job.Schedule(this, inputDeps);
        }

        private struct Job : IJobForEach<TargetItem> {
            public ComponentDataFromEntity<Unit> allUnits;
            public ComponentDataFromEntity<Health> allHealth;

            public void Execute(ref TargetItem targetItem) {
                Unit ownerUnit = this.allUnits[targetItem.owner];
                Health targetHealth = this.allHealth[targetItem.target];

                targetHealth.hp -= ownerUnit.attackDamage;
                this.allHealth[targetItem.target] = targetHealth; // Modify the data
            }
        }
    }
}
