using System.Collections.Generic;

namespace Game.Oop {
    /// <summary>
    /// Unit with targets
    /// </summary>
    public class Unit {
        private List<Unit> targets;
        private int hp;
        private int attackDamage;

        public void AddTarget(Unit target) {
            this.targets.Add(target);
        }

        public void ApplyDamageToTargets() {
            for (int i = 0; i < this.targets.Count; ++i) {
                this.targets[i].Deal(this.attackDamage);
            }
        }

        public void Deal(int damage) {
            this.hp -= damage;
        }
    }
}
