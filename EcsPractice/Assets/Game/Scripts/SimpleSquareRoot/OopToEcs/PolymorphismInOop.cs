namespace Game.PolymorphismOop {
    public abstract class Animal {
        public void Move() {
            // This a method common to all animals
        }

        // Every animal has specific action
        public abstract void DoAction();
    }

    public class Pig : Animal {
        public override void DoAction() {
            Eat();
            Sleep();
        }

        private void Eat() {
        }

        private void Sleep() {
        }
    }

    public class Cat : Animal {

        public override void DoAction() {
            Meow();
            Purr();
        }

        private void Meow() {
        }

        private void Purr() {
        }
    }
}