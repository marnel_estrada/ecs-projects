using System.Collections.Generic;

using Common;

using Unity.Entities;

using UnityEngine;
using UnityEngine.UI;

namespace Game {
    public class SimpleSquareRootEcsBaseBootstrap<T> : MonoBehaviour where T: struct, IComponentData {
        [SerializeField]
        private int initialCount = 1000;

        [SerializeField]
        private string subName;
        
        [SerializeField]
        private Text header;

        [SerializeField]
        private Text entityCountLabel;

        private EntityManager entityManager;
        
        private readonly List<Entity> entities = new List<Entity>();

        private void Awake() {
            Assertion.AssertNotEmpty(this.subName);
            Assertion.AssertNotNull(this.header);
            Assertion.AssertNotNull(this.entityCountLabel);

            this.header.text = $"Entity Count ({this.subName})";
            
            this.entityManager = World.Active.EntityManager;

            Spawn(this.initialCount);
        }
        
        private const int SPAWN_PER_CLICK = 1000;

        private void Update() {
            if (Input.GetMouseButtonDown(0)) {
                // Left click
                Spawn(SPAWN_PER_CLICK);
            }

            if (Input.GetMouseButtonDown(1)) {
                // Right click
                Destroy(SPAWN_PER_CLICK);
            }
        }

        private void Spawn(int spawnCount) {
            for (int i = 0; i < spawnCount; ++i) {
                Entity entity = this.entityManager.CreateEntity();
                this.entityManager.AddComponentData(entity, new SimpleSquareRootComponent());
                this.entityManager.AddComponentData(entity, new T());
                
                this.entities.Add(entity);
            }

            UpdateLabel();
        }
        
        private void Destroy(int count) {
            for (int i = 0; i < count; ++i) {
                if (this.entities.Count == 0) {
                    // Nothing more to destroy
                    return;
                }
                
                // Destroy from the end
                int lastIndex = this.entities.Count - 1;
                this.entityManager.DestroyEntity(this.entities[lastIndex]);
                this.entities.RemoveAt(lastIndex);
            }

            UpdateLabel();
        }
        
        private void UpdateLabel() {
            this.entityCountLabel.text = TextUtils.AsCommaSeparated(this.entities.Count);
        }
    }
}