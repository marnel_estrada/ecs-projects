using Unity.Collections;
using Unity.Entities;
using Unity.Mathematics;

using Random = Unity.Mathematics.Random;

namespace Game {
    public class SimpleSquareRootComponentSystem : ComponentSystem {
        private EntityQuery query;
        private ArchetypeChunkComponentType<SimpleSquareRootComponent> squareRootType;
        
        private Random random = new Random(123456);

        protected override void OnCreateManager() {
            this.query = GetEntityQuery(typeof(SimpleSquareRootComponent), typeof(UpdateInComponentSystem));
        }

        protected override void OnUpdate() {
            this.squareRootType = GetArchetypeChunkComponentType<SimpleSquareRootComponent>();
            
            NativeArray<ArchetypeChunk> chunks = this.query.CreateArchetypeChunkArray(Allocator.TempJob);
            for (int i = 0; i < chunks.Length; ++i) {
                Process(chunks[i]);
            }
            
            chunks.Dispose();
        }

        private void Process(in ArchetypeChunk chunk) {
            NativeArray<SimpleSquareRootComponent> components = chunk.GetNativeArray(this.squareRootType);
            for (int i = 0; i < components.Length; ++i) {
                SimpleSquareRootComponent component = components[i];
                component.value = this.random.NextFloat(0, 1000);
                component.squareRoot = math.sqrt(component.value);
                components[i] = component; // Modify the values
            }
        }
    }
}
