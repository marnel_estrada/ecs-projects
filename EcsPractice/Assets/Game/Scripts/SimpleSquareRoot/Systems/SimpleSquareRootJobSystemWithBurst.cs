using Unity.Burst;
using Unity.Entities;
using Unity.Jobs;
using Unity.Mathematics;

namespace Game {
    public class SimpleSquareRootJobSystemWithBurst : JobComponentSystem {
        private readonly Random random = new Random(123456);
        
        protected override JobHandle OnUpdate(JobHandle inputDeps) {
            Job job = new Job() {
                random = this.random
            };
            
            return job.Schedule(this, inputDeps);
        }

        [BurstCompile]
        private struct Job : IJobForEach<SimpleSquareRootComponent, UpdateInBurst> {
            public Random random;
            
            public void Execute(ref SimpleSquareRootComponent simple, ref UpdateInBurst tag) {
                simple.value = this.random.NextFloat(0, 1000);
                simple.squareRoot = math.sqrt(simple.value);
            }
        }
    }
}