using Unity.Collections;
using Unity.Entities;
using Unity.Jobs;
using Unity.Mathematics;

namespace Game {
    public class SimpleSquareRootIJobParallelForChunkIteration : JobComponentSystem {
        private EntityQuery query;
        private ArchetypeChunkComponentType<SimpleSquareRootComponent> squareRootType;
        
        private readonly Random random = new Random(123456);
        
        protected override void OnCreateManager() {
            base.OnCreateManager();
            this.query = GetEntityQuery(typeof(SimpleSquareRootComponent), typeof(UpdateInJobSystem));
            
            // We disabled it as it's only a sample
            this.Enabled = false; 
        }

        protected override JobHandle OnUpdate(JobHandle inputDeps) {
            this.squareRootType = GetArchetypeChunkComponentType<SimpleSquareRootComponent>();
            
            // Prepare the job
            NativeArray<ArchetypeChunk> chunks = this.query.CreateArchetypeChunkArray(Allocator.TempJob);
            Job job = new Job() {
                chunks = chunks,
                squareRootType = this.squareRootType,
                random = this.random
            };

            return job.Schedule(chunks.Length, 64, inputDeps);
        }

        private struct Job : IJobParallelFor {
            [DeallocateOnJobCompletion]
            public NativeArray<ArchetypeChunk> chunks;

            public ArchetypeChunkComponentType<SimpleSquareRootComponent> squareRootType;

            public Random random;

            public void Execute(int index) {
                ArchetypeChunk chunk = this.chunks[index];
                
                NativeArray<SimpleSquareRootComponent> components = chunk.GetNativeArray(this.squareRootType);
                for (int i = 0; i < components.Length; ++i) {
                    SimpleSquareRootComponent component = components[i];
                    component.value = this.random.NextFloat(0, 1000);
                    component.squareRoot = math.sqrt(component.value);
                    components[i] = component; // Modify the values
                }
            }
        }
    }
}