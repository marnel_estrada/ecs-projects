using Unity.Collections;
using Unity.Entities;
using Unity.Jobs;
using Unity.Mathematics;

namespace Game {
    public class SimpleSquareRootIJobChunk : JobComponentSystem {
        private EntityQuery query;
        private ArchetypeChunkComponentType<SimpleSquareRootComponent> squareRootType;
        
        private readonly Random random = new Random(123456);
        
        protected override void OnCreateManager() {
            base.OnCreateManager();
            this.query = GetEntityQuery(typeof(SimpleSquareRootComponent), typeof(UpdateInJobSystem));
            
            // We disabled it as it's only a sample
            this.Enabled = false; 
        }

        protected override JobHandle OnUpdate(JobHandle inputDeps) {
            this.squareRootType = GetArchetypeChunkComponentType<SimpleSquareRootComponent>();
            
            // Prepare the job
            Job job = new Job() {
                squareRootType = this.squareRootType,
                random = this.random
            };

            return JobChunkExtensions.Schedule(job, this.query, inputDeps);
        }

        private struct Job : IJobChunk {
            public ArchetypeChunkComponentType<SimpleSquareRootComponent> squareRootType;

            public Random random;

            public void Execute(ArchetypeChunk chunk, int chunkIndex, int firstEntityIndex) {
                NativeArray<SimpleSquareRootComponent> components = chunk.GetNativeArray(this.squareRootType);
                for (int i = 0; i < components.Length; ++i) {
                    SimpleSquareRootComponent component = components[i];
                    component.value = this.random.NextFloat(0, 1000);
                    component.squareRoot = math.sqrt(component.value);
                    components[i] = component; // Modify the values
                }
            }
        }
    }
}