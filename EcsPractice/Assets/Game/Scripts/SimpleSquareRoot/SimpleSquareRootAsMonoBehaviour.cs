using UnityEngine;

namespace Game {
    public class SimpleSquareRootAsMonoBehaviour : MonoBehaviour {
        private float value;
        private float squareRoot;

        private void Update() {
            this.value = Random.Range(0, 1000);
            this.squareRoot = Mathf.Sqrt(this.value);
        }
    }
}