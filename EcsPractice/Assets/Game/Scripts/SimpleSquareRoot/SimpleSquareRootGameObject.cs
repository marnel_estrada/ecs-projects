﻿using System.Collections.Generic;

using Common;

using UnityEngine;
using UnityEngine.UI;

namespace Game {
    public class SimpleSquareRootGameObject : MonoBehaviour {
        [SerializeField]
        private GameObject prefab;

        [SerializeField]
        private int initialCount = 1000;

        [SerializeField]
        private Text header;
        
        [SerializeField]
        private Text entityCountLabel;

        private readonly List<GameObject> entities = new List<GameObject>(1000); 

        private void Awake() {
            Assertion.AssertNotNull(this.prefab);
            Assertion.Assert(this.initialCount > 0);
            Assertion.AssertNotNull(this.header);
            Assertion.AssertNotNull(this.entityCountLabel);

            this.header.text = "Entity Count (GameObject)";
            
            Spawn(this.initialCount);
        }

        private const int SPAWN_PER_CLICK = 1000;

        private void Update() {
            if (Input.GetMouseButtonDown(0)) {
                // Left click
                Spawn(SPAWN_PER_CLICK);
            }

            if (Input.GetMouseButtonDown(1)) {
                // Right click
                Destroy(SPAWN_PER_CLICK);
            }
        }
        
        private void Spawn(int count) {
            for (int i = 0; i < count; ++i) {
                GameObject instance = Instantiate(this.prefab, this.transform);
                this.entities.Add(instance);
            }

            UpdateLabel();
        }

        private void Destroy(int count) {
            for (int i = 0; i < count; ++i) {
                if (this.entities.Count == 0) {
                    // Nothing more to destroy
                    return;
                }
                
                // Destroy from the end
                int lastIndex = this.entities.Count - 1;
                Destroy(this.entities[lastIndex]);
                this.entities.RemoveAt(lastIndex);
            }
            
            UpdateLabel();
        }
        
        private void UpdateLabel() {
            this.entityCountLabel.text = TextUtils.AsCommaSeparated(this.entities.Count);
        }
    }
}
