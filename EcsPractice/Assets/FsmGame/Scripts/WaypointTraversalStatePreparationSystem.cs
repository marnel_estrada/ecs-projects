﻿using System;

using Unity.Entities;

using Common.Ecs.Fsm;
using Common;

namespace Game {
    public class WaypointTraversalStatePreparationSystem : FsmStatePreparationJobSystem<WaypointTraversalFsm,
        WaypointTraversalStatePreparationSystem.PrepareJob> {

        public struct PrepareJob : IFsmStatePreparationAction {
            public void Prepare(ref FsmState state, ref EntityCommandBuffer.Concurrent commandBuffer, int jobIndex) {
                Entity owner = state.entityOwner;

                switch (state.stateId) {
                    case FsmGameBootstrap.HAS_MORE: {
                        Entity hasMoreEntity = this.AddAction(ref owner, ref commandBuffer, jobIndex);
                        commandBuffer.AddComponent(jobIndex, hasMoreEntity, new HasMore());
                    }

                        break;

                    case FsmGameBootstrap.MOVE:
                        Entity moveEntity = this.AddAction(ref owner, ref commandBuffer, jobIndex);
                        commandBuffer.AddComponent(jobIndex, moveEntity, new PrepareMove());

                        MoveAction move = new MoveAction();
                        move.finishEvent = FsmGameBootstrap.FINISHED;
                        commandBuffer.AddComponent(jobIndex, moveEntity, move);

                        commandBuffer.AddComponent(jobIndex, moveEntity, new DurationTimer());

                        break;

                    case FsmGameBootstrap.RESET:
                        Entity resetEntity = this.AddAction(ref owner, ref commandBuffer, jobIndex);
                        commandBuffer.AddComponent(jobIndex, resetEntity, new Reset());

                        break;
                }
            }
        }

        protected override PrepareJob StatePreparationAction {
            get {
                return new PrepareJob();
            }
        }
    }
}