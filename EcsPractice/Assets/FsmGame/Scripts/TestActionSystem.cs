﻿using Common.Ecs.Fsm;

using Unity.Collections;
using Unity.Entities;
using UnityEngine;

namespace Game {
    /// <summary>
    /// Can't use FsmSystem yet
    /// Fails when trying to access an element in either allStates or allFsms
    /// Thread here: https://forum.unity.com/threads/injecting-componentdatafromentity-in-a-parent-base-class-throws-nullreferenceexception.536642/
    /// </summary>
    [UpdateAfter(typeof(FsmActionStartSystem))]
    [UpdateBefore(typeof(FsmActionEndSystem))]
    [UpdateBefore(typeof(TimedWaitActionSystem))]
    class TestActionSystem : FsmActionSystem {
        private ArchetypeChunkComponentType<TestAction> testType;
        
        protected override EntityQuery ComposeQuery() {
            return GetEntityQuery(typeof(FsmAction), typeof(TestAction));
        }

        protected override void BeforeChunkTraversal() {
            base.BeforeChunkTraversal();
            this.testType = GetArchetypeChunkComponentType<TestAction>();
        }

        private NativeArray<TestAction> testActions;

        protected override void BeforeProcessChunk(ArchetypeChunk chunk) {
            base.BeforeProcessChunk(chunk);
            this.testActions = chunk.GetNativeArray(this.testType);
        }

        protected override void DoEnter(int index, ref FsmAction fsmAction) {
            TestAction test = this.testActions[index];
            Debug.Log("TestAction: " + TestAction.ACTION_NAMES[test.nameIndex]);

            if (test.finishEvent != Fsm.NULL_EVENT) {
                SendEvent(ref fsmAction, test.finishEvent);
            }

            // Destroy as it's no longer needed
            this.PostUpdateCommands.DestroyEntity(GetEntityAt(index));
        }
    }
}