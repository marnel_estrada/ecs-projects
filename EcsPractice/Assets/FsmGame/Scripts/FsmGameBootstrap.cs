﻿using Common.Ecs.Fsm;

using CommonEcs;

using Unity.Entities;
using Unity.Mathematics;
using Unity.Transforms;

using UnityEngine;
using UnityEngine.UI;

using Sprite = CommonEcs.Sprite;

namespace Game {
    public sealed class FsmGameBootstrap : MonoBehaviour {
        [SerializeField]
        private Material agentMaterial;

        [SerializeField]
        private int agentCount = 1;

        [SerializeField] 
        private Text agentCountLabel;

        private EntityManager entityManager;

        private static EntityArchetype PLAYER_ARCHETYPE;

        private FsmBuilder fsmBuilder;
        
        private Entity spriteLayerEntity;

        private void Awake() {
            Assertion.AssertNotNull(this.agentMaterial);
            Assertion.AssertNotNull(this.agentCountLabel);
        }

        private const int WAYPOINT_COUNT = 5;

        private void Start() {
            this.entityManager = World.Active.EntityManager;
            this.fsmBuilder = new FsmBuilder(this.entityManager);

            this.spriteLayerEntity = CreateSpriteLayer();

            PLAYER_ARCHETYPE = this.entityManager.CreateArchetype(
                // We use 3D position here so we can use Quaternion for rotation (TransformSystem uses Position and Rotation components)
                typeof(Translation),
                typeof(LocalToWorld),
                typeof(WaypointTraversal),
                ComponentType.Create<Waypoint>(),
                typeof(Sprite),
                typeof(AddToSpriteLayer),
                typeof(UseYAsSortOrder)
            );

            for (int i = 0; i < this.agentCount; ++i) {
                CreateAgent();
            }
            UpdateCountLabel();
        }
        
        private Entity CreateSpriteLayer() {
            Entity layerEntity = this.entityManager.CreateEntity();
            SpriteLayer spriteLayer = new SpriteLayer(layerEntity, this.agentMaterial, 100);
            spriteLayer.alwaysUpdateMesh = true;
            spriteLayer.useMeshRenderer = false;
            spriteLayer.layer = this.gameObject.layer;
            this.entityManager.AddSharedComponentData(layerEntity, spriteLayer);

            return layerEntity;
        }

        private void Update() {
            if(Input.GetMouseButtonDown(0)) {
                for (int i = 0; i < 1000; ++i) {
                    CreateAgent();
                }

                this.agentCount += 1000;
                UpdateCountLabel();
            }
        }

        private void UpdateCountLabel() {
            this.agentCountLabel.text = this.agentCount.ToString();
        }

        private void CreateAgent() {
            Entity entity = this.entityManager.CreateEntity(PLAYER_ARCHETYPE);
            
            // Prepare the sprite
            Sprite sprite = new Sprite();
            sprite.Init(Entity.Null, 0.1f, 0.1f, new float2(0.5f, 0.5f));
            sprite.SetUv(new float2(0, 0), new float2(1, 1));
            this.entityManager.SetComponentData(entity, sprite);
            
            this.entityManager.SetComponentData(entity, new AddToSpriteLayer(this.spriteLayerEntity));

            WaypointTraversal traversal = new WaypointTraversal();
            traversal.speed = UnityEngine.Random.Range(0.5f, 1.0f);
            PrepareFsm(ref entity, ref traversal);
            this.entityManager.SetComponentData(entity, traversal);

            PrepareWaypoints(entity);
        }

        // State IDs
        public const byte HAS_MORE = 1;
        public const byte MOVE = 2;
        public const byte RESET = 3;

        // Events
        public const uint YES = 1;
        public const uint NO = 2;
        public const uint FINISHED = 3;

        private void PrepareFsm(ref Entity traversalEntity, ref WaypointTraversal traversal) {
            Entity fsm = this.fsmBuilder.CreateFsm(ref traversalEntity);

            // States
            Entity hasMore = this.fsmBuilder.AddState<WaypointTraversalFsm>(fsm, HAS_MORE);
            Entity move = this.fsmBuilder.AddState<WaypointTraversalFsm>(fsm, MOVE);
            Entity reset = this.fsmBuilder.AddState<WaypointTraversalFsm>(fsm, RESET);

            // Actions are prepared in WaypointTraversalStatePreparationSystem

            // Transitions
            this.fsmBuilder.AddTransition(fsm, hasMore, YES, move);
            this.fsmBuilder.AddTransition(fsm, hasMore, NO, reset);

            this.fsmBuilder.AddTransition(fsm, move, FINISHED, hasMore);

            this.fsmBuilder.AddTransition(fsm, reset, FINISHED, hasMore);

            // Auto start the FSM
            this.fsmBuilder.Start(fsm, hasMore);

            traversal.fsmEntity = fsm;
        }

        private void PrepareWaypoints(Entity traversalEntity) {
            WaypointTraversal traversal = this.entityManager.GetComponentData<WaypointTraversal>(traversalEntity);
            traversal.waypointCount = WAYPOINT_COUNT;
            this.entityManager.SetComponentData(traversalEntity, traversal);
            
            DynamicBuffer<Waypoint> waypoints = this.entityManager.GetBuffer<Waypoint>(traversalEntity);

            for (int i = 0; i < WAYPOINT_COUNT; ++i) {
                Waypoint waypoint = new Waypoint();
                Vector3 posValue = UnityEngine.Random.insideUnitCircle * 1.3f;
                waypoint.position = posValue;
                waypoints.Add(waypoint);
            }

            // Set the position to the first waypoint
            Translation traversalPosition = this.entityManager.GetComponentData<Translation>(traversalEntity);
            traversalPosition.Value = waypoints[0].position;
            this.entityManager.SetComponentData(traversalEntity, traversalPosition);
        }
    }
}
