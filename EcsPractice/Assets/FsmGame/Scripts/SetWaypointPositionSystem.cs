﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Common.Ecs.Fsm;
using Unity.Collections;
using Unity.Entities;
using Unity.Transforms;
using UnityEngine;

namespace Game {
    /// <summary>
    /// System that sets the position to the move action component
    /// </summary>
    [UpdateAfter(typeof(FsmActionStartSystem))]
    [UpdateBefore(typeof(MoveActionSystem))]
    [UpdateBefore(typeof(FsmActionEndSystem))]
    class SetWaypointPositionSystem : FsmActionSystem {
        private ComponentDataFromEntity<WaypointTraversal> allTraversals;
        private ComponentDataFromEntity<Translation> allTranslations;

        private ArchetypeChunkComponentType<TraversalOwner> ownerType;
        private ArchetypeChunkComponentType<MoveAction> moveType;
                
        protected override EntityQuery ComposeQuery() {
            return GetEntityQuery(typeof(FsmAction), typeof(MoveAction), typeof(TraversalOwner));
        }

        protected override void BeforeChunkTraversal() {
            base.BeforeChunkTraversal();

            this.allTraversals = GetComponentDataFromEntity<WaypointTraversal>();
            this.allTranslations = GetComponentDataFromEntity<Translation>();

            this.ownerType = GetArchetypeChunkComponentType<TraversalOwner>();
            this.moveType = GetArchetypeChunkComponentType<MoveAction>();
        }

        private NativeArray<TraversalOwner> owners;
        private NativeArray<MoveAction> moveActions;
        
        protected override void BeforeProcessChunk(ArchetypeChunk chunk) {
            base.BeforeProcessChunk(chunk);

            this.owners = chunk.GetNativeArray(this.ownerType);
            this.moveActions = chunk.GetNativeArray(this.moveType);
        }

        protected override void DoEnter(int index, ref FsmAction fsmAction) {
            TraversalOwner owner = this.owners[index];
            WaypointTraversal traversal = this.allTraversals[owner.traversalEntity];
            DynamicBuffer<Waypoint> waypoints = this.EntityManager.GetBuffer<Waypoint>(owner.traversalEntity);
            MoveAction move = this.moveActions[index];
            Translation translation = this.allTranslations[move.target];

            // Prepare move
            move.from = translation.Value;
            move.to = waypoints[traversal.waypointIndex].position;
            move.duration = Vector3.Distance(move.from, move.to) / traversal.speed;
        }
    }
}