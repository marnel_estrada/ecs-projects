﻿using Common.Ecs.Fsm;

using Unity.Collections;
using Unity.Entities;

namespace Game {
    [UpdateAfter(typeof(PrepareMoveSystem))]
    [UpdateBefore(typeof(MoveActionSystem))]
    class RemovePrepareMoveSystem : ComponentSystem {
        private EntityQuery query;

        protected override void OnCreate() {
            base.OnCreate();
            this.query = GetEntityQuery(typeof(PrepareMove));
        }

        protected override void OnUpdate() {
            this.Entities.With(this.query).ForEach(delegate(Entity entity) {
                this.PostUpdateCommands.RemoveComponent<PrepareMove>(entity);
            });
        }
    }
}
