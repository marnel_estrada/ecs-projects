﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Common.Ecs.Fsm;

using Unity.Burst;
using Unity.Collections;
using Unity.Entities;
using Unity.Jobs;
using Unity.Mathematics;
using Unity.Transforms;

using UnityEngine;

namespace Game {
    [UpdateAfter(typeof(FsmActionStartSystem))]
    [UpdateBefore(typeof(FsmActionEndSystem))]
    [UpdateBefore(typeof(MoveActionSystem))]
    class PrepareMoveSystem : FsmJobSystem {
        private ComponentDataFromEntity<WaypointTraversal> allTraversals;
        private ComponentDataFromEntity<Translation> allTranslations;
        private BufferFromEntity<Waypoint> allWaypoints;

        protected override JobHandle OnUpdate(JobHandle inputDeps) {
            base.OnUpdate(inputDeps);
            
            this.allTraversals = GetComponentDataFromEntity<WaypointTraversal>();
            this.allTranslations = GetComponentDataFromEntity<Translation>();
            this.allWaypoints = GetBufferFromEntity<Waypoint>();
            
            Job job = new Job() {
                allTraversals = this.allTraversals,
                allTranslations = this.allTranslations,
                allWaypoints = this.allWaypoints,
                allStates = this.allStates,
                allFsms = this.allFsms
            };

            return job.Schedule(this, inputDeps);
        }

        [BurstCompile]
        private struct Job : IJobForEach<FsmAction, MoveAction, PrepareMove> {
            [ReadOnly]
            public ComponentDataFromEntity<WaypointTraversal> allTraversals;

            [ReadOnly]
            public ComponentDataFromEntity<Translation> allTranslations;

            [ReadOnly]
            public BufferFromEntity<Waypoint> allWaypoints;

            [ReadOnly]
            public ComponentDataFromEntity<FsmState> allStates;

            [ReadOnly]
            public ComponentDataFromEntity<Fsm> allFsms;

            public void Execute(ref FsmAction fsmAction, ref MoveAction move, ref PrepareMove prepare) {
                FsmState state = this.allStates[fsmAction.stateOwner];
                Fsm fsm = this.allFsms[state.fsmOwner];
                Entity fsmOwner = fsm.owner;
                move.target = fsmOwner;

                WaypointTraversal traversal = this.allTraversals[fsmOwner];
                Translation position = this.allTranslations[fsmOwner];
                DynamicBuffer<Waypoint> waypoints = this.allWaypoints[fsmOwner];

                move.from = position.Value;
                move.to = waypoints[traversal.waypointIndex].position;

                // Compute duration
                if (traversal.speed > 0) {
                    // Avoid divide by zero
                    move.duration = math.distance(move.to, move.from) / traversal.speed;
                } else {
                    move.duration = 0;
                }
            }
        }
    }
}
