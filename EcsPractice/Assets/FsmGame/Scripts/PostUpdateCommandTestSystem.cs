﻿using System;

using Unity.Collections;
using Unity.Entities;
using UnityEngine;

namespace Game {
    [UpdateBefore(typeof(DebugTagSystem))]
    class PostUpdateCommandTestSystem : ComponentSystem {
        private EntityQuery query;
        
        private int frameCounter;

        protected override void OnCreate() {
            this.query = GetEntityQuery(typeof(PostUpdateCommandTest));
        }

        protected override void OnUpdate() {
            NativeArray<Entity> entities = this.query.ToEntityArray(Allocator.TempJob);
            
            for(int i = 0; i < entities.Length; ++i) {
                // Add only on even frames
                if ((this.frameCounter & 1) == 0) {
                    Debug.Log("Adding DebugTag");
                    this.PostUpdateCommands.AddComponent(entities[i], new DebugTag());
                }
            }

            ++this.frameCounter;
            
            entities.Dispose();
        }
    }
}
