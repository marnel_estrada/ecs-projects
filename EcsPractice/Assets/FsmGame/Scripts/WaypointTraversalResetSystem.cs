﻿using Common.Ecs.Fsm;
using Unity.Collections;
using Unity.Entities;
using Unity.Transforms;

namespace Game {
    [UpdateAfter(typeof(FsmActionStartSystem))]
    [UpdateAfter(typeof(MoveActionSystem))]
    [UpdateBefore(typeof(FsmActionEndSystem))]
    class WaypointTraversalResetSystem : FsmActionSystem {
        private ComponentDataFromEntity<WaypointTraversal> allTraversals;
        private ComponentDataFromEntity<Translation> allTranslations;

        protected override EntityQuery ComposeQuery() {
            return GetEntityQuery(typeof(FsmAction), typeof(Reset));
        }

        protected override void BeforeChunkTraversal() {
            base.BeforeChunkTraversal();

            this.allTraversals = GetComponentDataFromEntity<WaypointTraversal>();
            this.allTranslations = GetComponentDataFromEntity<Translation>();
        }

        protected override void DoEnter(int index, ref FsmAction fsmAction) {
            Entity fsmOwner = GetFsmOwner(ref fsmAction);
            WaypointTraversal traversal = this.allTraversals[fsmOwner];
            traversal.waypointIndex = 0;
            this.allTraversals[fsmOwner] = traversal; // Update the data

            // Set position to the starting waypoint
            DynamicBuffer<Waypoint> waypoints = this.EntityManager.GetBuffer<Waypoint>(fsmOwner);
            Translation translation = this.allTranslations[fsmOwner];
            translation.Value = waypoints[0].position;
            this.allTranslations[fsmOwner] = translation; // Update the value

            SendEvent(ref fsmAction, FsmGameBootstrap.FINISHED);
        }

    }
}
