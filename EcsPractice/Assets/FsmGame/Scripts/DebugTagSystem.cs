﻿using System;

using Unity.Entities;
using UnityEngine;

namespace Game {
    [UpdateBefore(typeof(EndFrameSystem))]
    class DebugTagSystem : ComponentSystem {
        private EntityQuery query;

        protected override void OnCreate() {
            this.query = GetEntityQuery(typeof(DebugTag));
        }

        protected override void OnUpdate() {
            this.Entities.With(this.query).ForEach(delegate(Entity entity, ref DebugTag debug) {
                Debug.Log("DebugTag");
                this.PostUpdateCommands.RemoveComponent<DebugTag>(entity);
            });
        }
    }
}
