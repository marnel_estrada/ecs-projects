﻿using Unity.Entities;
using UnityEngine;

namespace Game {
    sealed class TestPostUpdateCommandsBootstrap : MonoBehaviour {
        private EntityManager entityManager;

        private void Awake() {
            this.entityManager = World.Active.EntityManager;

            // Create the entity
            Entity entity = this.entityManager.CreateEntity();
            this.entityManager.AddComponentData(entity, new PostUpdateCommandTest());
        }
    }
}
