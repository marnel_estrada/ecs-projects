﻿using System;

using Common.Ecs.Fsm;

using Unity.Collections;
using Unity.Entities;

namespace Game {
    [UpdateBefore(typeof(MoveActionSystem))]
    class HasMoreSystem : FsmActionSystem {
        private ComponentDataFromEntity<WaypointTraversal> allTraversals;

        protected override EntityQuery ComposeQuery() {
            return GetEntityQuery(typeof(FsmAction), typeof(HasMore));
        }

        protected override void BeforeChunkTraversal() {
            base.BeforeChunkTraversal();
            
            this.allTraversals = GetComponentDataFromEntity<WaypointTraversal>();
        }

        protected override void DoEnter(int index, ref FsmAction fsmAction) {
            Entity fsmOwer = GetFsmOwner(ref fsmAction);
            WaypointTraversal traversal = this.allTraversals[fsmOwer];

            if(traversal.waypointIndex + 1 < traversal.waypointCount) {
                // Has more waypoints to go
                traversal.waypointIndex = traversal.waypointIndex + 1;
                this.allTraversals[fsmOwer] = traversal; // Update the data
                SendEvent(ref fsmAction, FsmGameBootstrap.YES);
            } else {
                SendEvent(ref fsmAction, FsmGameBootstrap.NO);
            }

            // Destroy the action because it's already done
            this.PostUpdateCommands.DestroyEntity(GetEntityAt(index));
        }
    }
}
