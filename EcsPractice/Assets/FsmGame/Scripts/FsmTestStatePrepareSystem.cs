﻿using Common.Ecs.Fsm;
using Unity.Entities;

namespace Game {
    /// <summary>
    /// System that prepares state actions on state transition
    /// </summary>
    class FsmTestStatePrepareSystem : FsmStatePreparationSystem<FsmTestTag> {
        protected override void OnCreateManager() {
            base.OnCreateManager();

            // Populate prepare actions
            AddPrepareAction(FsmTestBootstrap.BEGIN, delegate(ref Entity stateEntity) {
                // Note that we create action entities using PostUpdateCommands
                // Test action
                {
                    this.FsmBuilder.AddAction(this.PostUpdateCommands, stateEntity);
                    TestAction testAction = new TestAction();
                    testAction.nameIndex = 1;
                    this.PostUpdateCommands.AddComponent(testAction);
                }

                // Wait action
                {
                    this.FsmBuilder.AddAction(this.PostUpdateCommands, stateEntity);
                    TimedWaitAction.AddAsAction(this.PostUpdateCommands, 1, FsmTestBootstrap.FINISHED);
                }
            });

            AddPrepareAction(FsmTestBootstrap.MIDDLE, delegate (ref Entity stateEntity) {
                // Note that we create action entities using PostUpdateCommands
                // Test action
                {
                    this.FsmBuilder.AddAction(this.PostUpdateCommands, stateEntity);
                    TestAction testAction = new TestAction();
                    testAction.nameIndex = 2;
                    this.PostUpdateCommands.AddComponent(testAction);
                }

                // Wait action
                {
                    this.FsmBuilder.AddAction(this.PostUpdateCommands, stateEntity);
                    TimedWaitAction.AddAsAction(this.PostUpdateCommands, 1, FsmTestBootstrap.FINISHED);
                }
            });

            AddPrepareAction(FsmTestBootstrap.END, delegate (ref Entity stateEntity) {
                // Note that we create action entities using PostUpdateCommands
                // Test action
                {
                    this.FsmBuilder.AddAction(this.PostUpdateCommands, stateEntity);
                    TestAction testAction = new TestAction();
                    testAction.nameIndex = 3;
                    this.PostUpdateCommands.AddComponent(testAction);
                }

                // Wait action
                {
                    this.FsmBuilder.AddAction(this.PostUpdateCommands, stateEntity);
                    TimedWaitAction.AddAsAction(this.PostUpdateCommands, 1, FsmTestBootstrap.FINISHED);
                }
            });
        }
    }
}
