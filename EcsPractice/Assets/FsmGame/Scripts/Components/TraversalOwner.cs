﻿using System;

using Unity.Entities;

namespace Game {
    struct TraversalOwner : IComponentData {
        public Entity traversalEntity;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="traversalEntity"></param>
        public TraversalOwner(Entity traversalEntity) {
            this.traversalEntity = traversalEntity;
        }
    }
}
