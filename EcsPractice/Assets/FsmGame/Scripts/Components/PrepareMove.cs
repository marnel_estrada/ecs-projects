﻿using Unity.Entities;

namespace Game {
    /// <summary>
    /// A tag component that prepares the MoveAction component before it is executed
    /// </summary>
    struct PrepareMove : IComponentData {
        private byte dummy;
    }
}
