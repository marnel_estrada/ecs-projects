﻿using System;
using Unity.Entities;

namespace Game {
    /// <summary>
    /// A tag used to identity states that belong to a certain FSM so that they can be filtered
    /// in *StatePrepareSystem.
    /// </summary>
    struct FsmTestTag : IComponentData {
    }
}
