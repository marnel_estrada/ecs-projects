﻿using System;

using Unity.Entities;

namespace Game {
    /// <summary>
    /// A tag component for the reset action
    /// </summary>
    struct Reset : IComponentData {
    }
}
