﻿using System;
using Unity.Entities;

namespace Game {
    public struct WaypointTraversal : IComponentData {

        public Entity fsmEntity; // The entity that holds the FSM component
        public int waypointCount;
        public int waypointIndex; // The current index to where the agent should go to

        public float speed;

    }
}
