﻿using System;

using Unity.Entities;
using Unity.Mathematics;

namespace Game {
    /// <summary>
    /// This is just a tag. We bundle it with a Position component instead.
    /// </summary>
    [InternalBufferCapacity(10)]
    public struct Waypoint : IBufferElementData {
        public float3 position;
    }
}
