﻿using System;
using System.Collections.Generic;
using Unity.Entities;

namespace Game {
    /// <summary>
    /// It's just a tag for now
    /// </summary>
    struct TestAction : IComponentData {

        public int nameIndex;
        public uint finishEvent;

        public static Dictionary<int, string> ACTION_NAMES = new Dictionary<int, string>() {
            { 1, "Begin" },
            { 2, "Middle" },
            { 3, "End" }
        };

    }
}
