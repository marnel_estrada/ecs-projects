﻿using System;

using Unity.Entities;

namespace Game {
    /// <summary>
    /// A component tag to filter entities that belong to WaypointTraversalFsm
    /// </summary>
    public struct WaypointTraversalFsm : IComponentData {
    }
}
