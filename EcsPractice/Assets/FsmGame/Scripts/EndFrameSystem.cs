﻿using Unity.Entities;
using UnityEngine;

namespace Game {
    class EndFrameSystem : ComponentSystem {
        private int frameCount;

        private EntityQuery query;

        protected override void OnCreate() {
            this.query = GetEntityQuery(typeof(PostUpdateCommandTest));
        }

        protected override void OnUpdate() {
            // Print only if there were entities with PostUpdateCommandTest components
            if (this.query.CalculateLength() > 0) {
                Debug.Log("Frame: " + this.frameCount);
                ++this.frameCount;
            }
        }
    }
}
