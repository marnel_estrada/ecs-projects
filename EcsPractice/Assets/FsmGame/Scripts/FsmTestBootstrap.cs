﻿using UnityEngine;
using Unity.Entities;
using Common.Ecs.Fsm;

namespace Game {
    sealed class FsmTestBootstrap : MonoBehaviour {
        private EntityManager entityManager;
        private FsmBuilder fsmBuilder;

        private void Awake() {
            this.entityManager = World.Active.EntityManager;
            this.fsmBuilder = new FsmBuilder(this.entityManager);

            // Create FSM Test entity
            Entity entity = this.entityManager.CreateEntity();

            FsmTest test = new FsmTest();
            PrepareFsm(ref test, ref entity);
            this.entityManager.AddComponentData(entity, test);
        }

        // Events
        public const uint FINISHED = 1;

        // State IDs
        public const byte BEGIN = 1;
        public const byte MIDDLE = 2;
        public const byte END = 3;

        private void PrepareFsm(ref FsmTest test, ref Entity owner) {
            Entity fsm = this.fsmBuilder.CreateFsm(ref owner);

            // States
            Entity begin = this.fsmBuilder.AddState<FsmTestTag>(fsm, BEGIN);
            Entity middle = this.fsmBuilder.AddState<FsmTestTag>(fsm, MIDDLE);
            Entity end = this.fsmBuilder.AddState<FsmTestTag>(fsm, END);

            // Actions
            // Actions are prepared in FsmTestStatePrepareSystem

            // Transitions
            this.fsmBuilder.AddTransition(fsm, begin, FINISHED, middle);

            this.fsmBuilder.AddTransition(fsm, middle, FINISHED, end);

            this.fsmBuilder.AddTransition(fsm, end, FINISHED, begin);

            // Start the FSM
            this.fsmBuilder.Start(fsm, begin);

            test.fsm = fsm;
        }
    }
}
