﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using Common.Ecs.Fsm;
using Common;

namespace Game {
    public class TestStruct : MonoBehaviour {

        private void Awake() {
            DurationTimer timer = new DurationTimer();
            timer.polledTime = 1;
            timer.durationTime = 2;
            Debug.Log("Initial: " + timer);

            timer.Reset(5);
            Debug.Log("Reset: " + timer);
        }

    }
}
