﻿using System;

using Unity.Collections;
using Unity.Jobs;

namespace Unify {
    struct SpriteSortEntry {
        public int index; // This is the index when referencing the sprite to its original list
        public float drawLayer; // The value to sort

        public SpriteSortEntry(int index, float drawLayer) {
            this.index = index;
            this.drawLayer = drawLayer;
        }
    }

    struct SpriteManagerSortJob : IJob {

        // The SpriteSortEntry array that is assigned to this job may have more elements than count
        // The extra elements may be an extra buffer
        public int count;

        public NativeArray<SpriteSortEntry> entries;

        public void Execute() {
            // This uses insertion sort because the items are already nearly sorted
            for (int i = 1; i < this.count; ++i) {
                SpriteSortEntry entry = this.entries[i];
                int j = i - 1;

                while (j >= 0 && this.entries[j].drawLayer > entry.drawLayer) {
                    this.entries[j + 1] = this.entries[j];
                    --j;
                }

                this.entries[j + 1] = entry;
            }
        }
    }

}